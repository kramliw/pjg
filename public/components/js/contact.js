(function($) {

    "use strict";

    /*-------------------------------------------------*/
    /* CONTACT FORM AJAX
    /*-------------------------------------------------*/

    var validateEmail = function(email) {
        var patt = /^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
        if (patt.test(email) === true) {
            return true;
        }
        return false;
    };
    var bootstrapAlert = function(type, text) {
        var alert = '<div class="alert alert-' + type + ' alert-dismissable">';
        alert += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
        alert += text;
        alert += '</div>';
        return alert;
    };
    var contactResponse = function(responseNode, type, response) {
        if (response !== '') {
            responseNode.html(bootstrapAlert(type, response));
        } else {
            responseNode.html(bootstrapAlert('danger', 'Oops! An error occured.'));
        }
        bootstrapAlert();
    };

    $(document).ready(function() {
        var contactForm = $("#contactForm");
        var responseNode = $('#contactResponse');
        contactForm.on("submit", function(e) {
            e.preventDefault();

            var self = $(this);
            var valid_form = true;
            var name = contactForm.find($("input[name='contactName']"));
            var email = contactForm.find($("input[name='contactEmail']"));
            var message = contactForm.find($("textarea[name='contactMessage']"));
            var formFields = [name, message];

            formFields.forEach(function(input) {
                if (input.val() === '') {
                    input.addClass('input-error');
                    valid_form = false;
                }
            });

            if (email.val() === '' || validateEmail(email.val()) !== true) {
                email.addClass('input-error');
                valid_form = false;
            }

            self.find('input, textarea, select').on('change', function() {
                $(this).removeClass('input-error');
            });

            if (valid_form === true) {
                var response = "Thank You! Your message has been sent.";
                contactResponse(responseNode, "success", response);
            }
        });
    });

})(jQuery);
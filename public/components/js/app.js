/*-------------------------------------------------*/
/* TABLE OF CONTENTS */
/* --------------------
    01. JQUERY EXTENTION
    02. BACKGROUND IMAGE
    03. BACKGROUND VIDEO
    04. NAVIGATION MENU
    05. OWL CAROUSEL SLIDER
    06. GOOGLE MAP
    07. PROGRESS BARS
    08. MAGNIFIC POPUP
    09. TYPED JS
    10. MASONRY
    11. DOCUMENT READY EVENT
    12. WINDOW LOAD EVENT
    13. WINDOW SCROLL EVENT
/*-------------------------------------------------*/


(function($) {

    "use strict";

    /*-------------------------------------------------*/
    /* JQUERY EXTENTION
    /*-------------------------------------------------*/
    $.fn.closestChild = function(filter) {
        var $found = $(),
            $currentSet = this; // Current place
        while ($currentSet.length) {
            $found = $currentSet.filter(filter);
            if ($found.length) break;  // At least one match: break loop
            // Get all children of the current set
            $currentSet = $currentSet.children();
        }
        return $found.first(); // Return first match of the collection
    }
    var windowWidth = function(){
        return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    };

    /*-------------------------------------------------*/
    /* BACKGROUND IMAGE
    /*-------------------------------------------------*/
    var bgImage = function() {
        var bgImgSelector = $("[data-bg-img]");
        if (bgImgSelector.length) {
            bgImgSelector.each(function() {
                var dataBgImg = $(this).attr('data-bg-img');
                var cssBgImg = $(this).css('background-image');
                var bgImg = 'url(' + dataBgImg + ')';
                if (cssBgImg !== undefined && cssBgImg !== 'none') {
                    bgImg = cssBgImg + ', ' + bgImg;
                }
                if (typeof dataBgImg !== typeof undefined && dataBgImg !== false && dataBgImg !== "") {
                    $(this).css('background-image', bgImg);
                }
            });
        }
    };

    /*-------------------------------------------------*/
    /* BACKGROUND VIDEO
    /*-------------------------------------------------*/
    var bgVideo = function() {
        $(".hero-video-player").YTPlayer({
            showControls: false
        });
    };

    /*-------------------------------------------------*/
    /* NAVIGATION MENU
    /*-------------------------------------------------*/
    var navigation = function() {
        var $this = $('.navigation'),
        navToggle = $this.find('.navigation-toggle');
        $('.navigation-menu li').has('ul').closestChild('a').addClass('has-dropdown');
        $('.navigation-menu li:has(ul) > a').append("<span class='submenu-button'></span>");
        navToggle.on('click', function() {
            $this.toggleClass('navigation-canvas-open');
        });
    }
    var navigationMobile = function () {
        var $this = $('.navigation'),
        mobileBreakpoint = 991;
        if (windowWidth() < mobileBreakpoint) {
            $this.addClass('navigation-mobile');
            $('.navigation-mobile .navigation-menu ul').hide();
            $('.navigation-mobile .navigation-menu li:has(ul) > a').off('click').on('click', function(e) {
                e.stopPropagation();
                e.preventDefault();
                $(this).parent().closestChild('ul').slideToggle();
                $(this).parent().siblings().children('ul').slideUp();
                $(this).find('.submenu-button').toggleClass('submenu-opened');
                $(this).parent().siblings().find('.submenu-button').removeClass('submenu-opened');
            });
            $('.navigation-menu li').off('mouseenter mouseleave').on('mouseenter mouseleave', function(e){
                e.stopPropagation();
            });
        }
        else {
            $this.removeClass('navigation-canvas-open');
            $this.removeClass('navigation-mobile');
            $('.navigation-menu li').off('mouseenter').on('mouseenter', function() {
                $this = $(this);
                $this.children('ul').stop(true, true).slideDown();
            });
            $('.navigation-menu li').off('mouseleave').on('mouseleave', function() {
                $this = $(this);
                $this.children('ul').stop(true, true).slideUp(100);
            });
        }
    }


    /*-------------------------------------------------*/
    /* OWL CAROUSEL SLIDER
    /*-------------------------------------------------*/
    var owlCarousel = function() {
        var owlSelector = $('.owl-carousel');
        if (owlSelector.length) {
            owlSelector.each(function() {
                var carousel = $(this),
                    autoplay_hover_pause = carousel.data('autoplay-hover-pause'),
                    loop = carousel.data('loop'),
                    items_general = carousel.data('items'),
                    margin = carousel.data('margin'),
                    autoplay = carousel.data('autoplay'),
                    autoplayTimeout = carousel.data('autoplay-timeout'),
                    smartSpeed = carousel.data('smart-speed'),
                    nav_general = carousel.data('nav'),
                    navSpeed = carousel.data('nav-speed'),
                    xs_items = carousel.data('xs-items'),
                    xs_nav = carousel.data('xxs-nav'),
                    sm_items = carousel.data('sm-items'),
                    sm_nav = carousel.data('xs-nav'),
                    md_items = carousel.data('md-items'),
                    md_nav = carousel.data('sm-nav'),
                    lg_items = carousel.data('lg-items'),
                    lg_nav = carousel.data('md-nav'),
                    xl_items = carousel.data('xl-items'),
                    xl_nav = carousel.data('lg-nav'),
                    center = carousel.data('center'),
                    dots_global = carousel.data('dots'),
                    xs_dots = carousel.data('xxs-dots'),
                    sm_dots = carousel.data('xs-dots'),
                    md_dots = carousel.data('sm-dots'),
                    lg_dots = carousel.data('md-dots'),
                    xl_dots = carousel.data('lg-dots');

                carousel.owlCarousel({
                    autoplayHoverPause: autoplay_hover_pause,
                    loop: (loop ? loop : false),
                    items: (items_general ? items_general : 1),
                    lazyLoad: true,
                    margin: (margin ? margin : 0),
                    autoplay: (autoplay ? autoplay : false),
                    autoplayTimeout: (autoplayTimeout ? autoplayTimeout : 1000),
                    smartSpeed: (smartSpeed ? smartSpeed : 250),
                    dots: (dots_global ? dots_global : false),
                    nav: (nav_general ? nav_general : false),
                    navText: ["<i class='zmdi zmdi-long-arrow-left' aria-hidden='true'></i>", "<i class='zmdi zmdi-long-arrow-right' aria-hidden='true'></i>"],
                    navSpeed: (navSpeed ? navSpeed : false),
                    center: (center ? center : false),
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: (xs_items ? xs_items : (items_general ? items_general : 1)),
                            nav: (xs_nav ? xs_nav : (nav_general ? nav_general : false)),
                            dots: (xs_dots ? xs_dots : (dots_global ? dots_global : false))
                        },
                        480: {
                            items: (sm_items ? sm_items : (items_general ? items_general : 1)),
                            nav: (sm_nav ? sm_nav : (nav_general ? nav_general : false)),
                            dots: (sm_dots ? sm_dots : (dots_global ? dots_global : false))
                        },
                        768: {
                            items: (md_items ? md_items : (items_general ? items_general : 1)),
                            nav: (md_nav ? md_nav : (nav_general ? nav_general : false)),
                            dots: (md_dots ? md_dots : (dots_global ? dots_global : false))
                        },
                        992: {
                            items: (lg_items ? lg_items : (items_general ? items_general : 1)),
                            nav: (lg_nav ? lg_nav : (nav_general ? nav_general : false)),
                            dots: (lg_dots ? lg_dots : (dots_global ? dots_global : false))
                        },
                        1199: {
                            items: (xl_items ? xl_items : (items_general ? items_general : 1)),
                            nav: (xl_nav ? xl_nav : (nav_general ? nav_general : false)),
                            dots: (xl_dots ? xl_dots : (dots_global ? dots_global : false))
                        }
                    }
                });
            });
        }
    };

    /*-------------------------------------------------*/
    /* GOOGLE MAP
    /*-------------------------------------------------*/
    var gmap = function() {
        if ($('.gmap').length) {
            var i = 0;
            $('.gmap').each(function() {
                i++;
                var self = $(this).attr("id", "gmap" + i),
                    mapDiv = "#gmap" + i,
                    mapLat = self.data('lat'),
                    mapLng = self.data('lng'),
                    mapZoom = self.data('zoom'),
                    map = new GMaps({
                        div: mapDiv,
                        lat: mapLat,
                        lng: mapLng,
                        zoom: mapZoom,
                        enableNewStyle: true,
                        scrollwheel: false
                    });
                map.addMarker({
                    lat: map.getCenter().lat(),
                    lng: map.getCenter().lng(),
                    title: 'Our Location',
                    infoWindow: {
                        content: '<h4>Our Location</h4><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>'
                    }
                });
            });
        }
    };

    /*-------------------------------------------------*/
    /* PROGRESS BARS
    /*-------------------------------------------------*/
    var progressBar = function() {
        var progressSelect = $("[data-progress]");
        $.each(progressSelect, function() {
            var self = $(this);
            self.on('inview', function(event, isInView) {
                if (isInView) {
                    self.css("width", self.data('progress') + "%");
                }
            });
        });
    }

    /*-------------------------------------------------*/
    /* MAGNIFIC POPUP
    /*-------------------------------------------------*/
    var magnificPopup = function() {

        $('.img-popup').magnificPopup({
            type: 'image',
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true
            }
        });

        $('.img-popup').each(function() {
            var $self = $(this),
                imgSrc = $self.find('img').attr('src');
            $self.attr({
                href: imgSrc
            });
        });

        $('.video-popup').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false,
            iframe: {
                patterns: {
                    youtube: {
                        src: 'https://www.youtube.com/embed/%id%?autoplay=1' /* URL that will be set as a source for iframe. */
                    },
                    vimeo: {
                        src: 'https://player.vimeo.com/video/%id%?autoplay=1'
                    },
                    gmaps: {
                        index: 'https://maps.google.'
                    }
                }
            }
        });
    };

    /*-------------------------------------------------*/
    /* TYPED JS
    /*-------------------------------------------------*/
    var typedJS = function() {
        if ($('.typed-text').length) {
            var typed = new Typed('.typed-text', {
                stringsElement: '.typed-strings',
                typeSpeed: 60,
                loop: true
            });
        }
    }

    /*-------------------------------------------------*/
    /* MASONRY
    /*-------------------------------------------------*/
    var masonry = function() {
        var portfolio = $(".portfolio-masnory"),
            portfolioWrap = portfolio.find('.row');
        if (portfolio.length) {
            portfolioWrap.each(function() {
                $(this).masonry({
                    itemSelector: '.grid-item',
                    columnWidth: '.grid-item'
                });
            });
        }
    };

    /*-------------------------------------------------*/
    /* DOCUMENT READY EVENT
    /*-------------------------------------------------*/
    $(document).ready(function() {
        bgImage();
        bgVideo();
        owlCarousel();
        gmap();
        magnificPopup();
        masonry();
        typedJS();
        navigation();
        navigationMobile();
    });

    /*-------------------------------------------------*/
    /* WINDOW RESIZE EVENT
    /*-------------------------------------------------*/
    $(window).on("resize", function() {
        navigationMobile();
    });

    /*-------------------------------------------------*/
    /* WINDOW SCROLL EVENT
    /*-------------------------------------------------*/
    $(window).on("scroll", function() {
        /* Do Something Here... */
    });

    /*-------------------------------------------------*/
    /* WINDOW LOAD EVENT
    /*-------------------------------------------------*/
    $(window).on("load", function() {
        progressBar();
    });


})(jQuery);
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string("first_name")->nullable();
            $table->string("last_name")->nullable();
            $table->string("middle_name")->nullable();
            $table->string("position")->nullable();
            $table->string("department")->nullable();
            $table->unsignedInteger("ward_id")->index();
            $table->unsignedInteger("schedule_id")->index()->nullable();
            $table->timestamps();
            $table->foreign('ward_id')->references("id")->on('wards');
            $table->foreign('schedule_id')->references("id")->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}

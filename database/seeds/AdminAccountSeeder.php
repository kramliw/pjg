<?php

use App\Laravel\Models\User;
use Illuminate\Database\Seeder;

class AdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $first_user = new  User;
            $input = ['username' => "master_admin", 'email' => "pjgrmc@yahoo.com",'password' => bcrypt('admin'),'name' => "Master Admin","type" => "super_user"];
            $first_user->fill($input);
            $first_user->save();
        
    }
}

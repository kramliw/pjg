<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    protected $fillable = ["ward_name"];


    public function employee()
    {
    	return $this->hasOne('App\Laravel\Models\Employee');
    }

  
}

<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeSchedule extends Model
{
    protected $fillable = ["employee_id","employee_ward_id","employee_shift_id","date"];


    public function employee()
    {
    	return $this->hasOne('App\Laravel\Models\Employee','id','employee_id');
    }
     public function schedule()
    {
    	return $this->hasMany('App\Laravel\Models\schedule','id','employee_shift_id');
    }

    public function ward()
    {
    	return $this->hasOne('App\Laravel\Models\Ward','id','employee_ward_id');
    }


    public function scopeFilter($query, $shift= "")    {

    	return $query->where('employee_shift_id',$shift);
    }
     

}

<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = ["time_start","time_end","description"];


    protected $appends = ['shift'];


    public function getShiftAttribute()
    {
    	return  $this->time_start ." - ". $this->time_end;
    }

    function employee_schedule()
    {
        return $this->belongsTo('App\Laravel\Models\EmloyeeSchedule',"employee_shift_id","id");
    }

   
}

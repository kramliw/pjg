<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ["first_name","last_name","middle_name","position", "department","ward_id","schedule_id"];



protected $appends = ['full_name'];

    public function getFullNameAttribute()
    {
    	return $this->first_name." ".$this->last_name;
    }

    public function ward()
    {
    	return $this->belongsTo('App\Laravel\Models\Ward');
    }
     public function schedule()
    {
    	return $this->belongsTo('App\Laravel\Models\Schedule','employee_id','id');
    }

     /**
     * Search users that match a keyword.
     */

     public function scopeKeyword($query, $keyword = "") {
        return $query->where('ward_id',$keyword);
    }
   
}

<!DOCTYPE html>
<html>
<head>
	<title>Activity</title>

<style>
	body{ font-family: 'Arial', sans-serif; font-size: 16px; }
	.page-break {
    page-break-after: always;
	}
	th.activity {
		border-bottom:none;
	}
	p{ margin: 3px 0px; padding: 0px; line-height: 20px; }
	h3{ font-weight: lighter;text-align: center; }
	td{ font-size: 12px; border-top: none;padding: 0px 0 10px 5px; }
	th{ font-size: 12px; font-weight: bold; }

	
</style>
</head>
<body>


<h3 style="padding-top: 80px;">ACTIVITY CLEARANCE</h3>
<table width="100%" style="padding-top: -20px;" cellspacing="0" cellpadding="3" border="1
">
	<tbody>
		<tr >
			<th colspan="15" class="activity">NAME OF APPLICANT / COMPANY</th>
			<th colspan="5"  class="activity"width="20%">DATE FILLED</th>
		</tr>
		
		<tr>
			<td colspan="15">{{ $activity->name }}</td>
			<td colspan="5">{{ Helper::date_db($activity->date_filed) }}</td>
		</tr>
		<tr >
			<th colspan="20" class="activity">BUSINESS ADDRESS</th>	
		</tr>
		<tr>
			<td colspan="20">{{ $activity->business_address }}</td>
		</tr>
		
		<tr >
			<th colspan="12" class="activity">AUTHORIZED REPRESENTATIVE</th>
			<th colspan="8"  class="activity">TEL./FAX NUMBER</th>
		</tr>
		<tr>
			<td colspan="12">{{ $activity->authorized_representative }}</td>
			<td colspan="8">{{ $activity->fax_number }}</td>
		</tr>
		<tr>
			<th colspan="5" class="activity">TYPE OF ACTIVITY</th>
			<th colspan="15"  class="activity">DETAILS OF ACTIVITY</th>
		</tr>
		<tr>
			<td colspan="5"">{{ $activity->activity_type }}</td>
			<td colspan="15"">{{ $activity->activity_details }}</td>
		</tr>

		<tr>
			<th colspan="8"  class="activity">LOCATION OF ACTIVITY</th>
			<th colspan="4" align="center">ACTIVITY DATE </th>
			<th colspan="4" align="center">NO. OF DAYS</th>
			<th colspan="4" align="center">ACTIVITY TIME</th>
		</tr>
		<tr>
			<td colspan="8">{{ $activity->activity_location }}</td>
			<td colspan="4" align="center"><i style="font-weight: normal;" >FROM-TO</i><br>{{ $activity->activity_date }}</td>
			<td colspan="4" align="center">{{ $activity->no_of_days }}</td>
			<td colspan="4" align="center">{{ $activity->activity_time }}</td>
		</tr>
		<tr >
			<th colspan="20" class="activity">COMMENTS(To be accomplish by BSA)</th>	
		</tr>
		<tr>
			<td colspan="20" style="padding-bottom: 40px;"></td>
		</tr>
		<tr>
			<th colspan="20" align="center" class="activity">TERMS AND CONDITIONS</th>

		</tr>
		<tr>
			<td colspan="20" style="border-bottom: none;">1. Required documents: <br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Route map: Motorcade, fun run, and other similar activities utilizing street within the jurisdiction of BSA<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Story board: Film/commercial shooting<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Other necessary documents related to a particular activity.<br>
				2. This Activity clearance shall be valid only for the purpose for which it was approved and during the day and time specified.<br> &nbsp;&nbsp;&nbsp;&nbsp;Activities not started therien shall not be allowed, unless renewed and applicable fee is paid<br>
				3. The use of Incendiary or explosive devise is strictly prohibited. BSA may allow the display of fireworks for certain related<br>&nbsp;&nbsp;&nbsp;&nbsp;activities,provided that specific risk management measures are complied with.<br>
				4. The company/applicant shall restore the place of activiy in good condition as it was prior to the activity and assumes full<br>&nbsp;&nbsp;&nbsp;&nbsp;responsibility for damage to persons or property brought about by the activity<br>
				5. The applicants shall comply with the existing Barangay/City Ordinances and other laws, rules & regulations imposed by <br>&nbsp;&nbsp;&nbsp;&nbsp;appropriate government regulatory bodies.<br>
				6. The applicant shall observe traffic rules and regulations within the Barangay.<br>
				7. Civil works are covered by a Barangay Memorandum of Afreement<br>
				8.Issuance of Barangay activity clearance shall be denied if the activity, after thorough review , is found to be contrary to law<br>&nbsp;&nbsp;&nbsp;&nbsp;good morals,public order and safety.<br>
				9. If, in the judgement of BSA, the applicant fails to meet the requirements set forth herein. BSA has the right to revoke or<br>&nbsp;&nbsp;&nbsp;&nbsp;annul any approved application for noncompliance with policy or procedural requirements.<br>
				10. This clearance must be kept in the activity site and shall be presented to BSA authorized personnel upon request.<br>
				<h4>&nbsp;&nbsp;&nbsp;&nbsp;I HEREBY UNDERDATEK TO COMPLY WITH ALL THE BSA's TERMS & CONDITIONS ABOVE CITED.</h4></td>
		</tr>
		<tr>
			<td colspan="10" align="center" style="border-bottom: none; border-right: none;">_________________________________________</td>
			<td colspan="10" align="center" style="border-bottom: none;border-left: none;">___________________</td>
		</tr>
		<tr>
			<td colspan="10" align="center" style="border-right: none;padding-top: -10px;">Applcant's signature over printed name</td>
			<td colspan="10" align="center" style="border-left: none;padding-top: -10px;">Date</td>
		</tr>
		<tr>
			<td colspan="10" style="border-bottom: none; padding-bottom: 30px;">REVIEWED/INSPECTED BY:</td>
			<td colspan="10" style="border-bottom: none;padding-bottom: 30px;" >APPROVED BY:</td>
		</tr>
		<tr>
			<td colspan="5" style="border-right:none;" align="center"><b>______________________<br>RIZZA M. LAMOSTE</b><br>Tresury Department</td>
			<td colspan="5" align="center" style="border-left: none;">_____________________<br>Date & Time Inspected<br>&nbsp;</td>
			<td colspan="10" style="border-left: :none;" align="center"><b>________________________________<br>THOMAS RAYMOND U. LISING</b><br>Punong Barangay</td>
		</tr>

	</tbody>
</table>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Certificate</title>
	<style>
		body{ font-family: 'Arial', sans-serif; font-size: 16px; }
		.page-break {
		    page-break-after: always;
		}
		p{ margin: 3px 0px; padding: 0px; line-height: 20px; }
	</style>
</head>
<body>
	<table  style="padding: 0px 25px; margin-top: 100;" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td colspan="2" style="text-align: center; letter-spacing: 4px; font-weight: bold; font-size: 22px;">CERTIFICATION</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center;">No. <b style="text-decoration: underline;">{{$certificate->certificate_number}}</b></td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 20px;"><p style="text-align: justify; text-indent: 50px;">This is to certify that per records of this office, the person named below is a bona fide resident of this Barangay:</p></td>
		</tr>
		<tr>
			<td style="width: 75%; vertical-align: text-top; padding-top: 30px;">
				<p>Name: <b>{{Str::upper($resident->full_name)}}</b></p>
				<p>Address: <b>{{$resident->full_address}}, <br>Barangay San Antonio, Pasig City</b></p>
				<p>Gender: <b>{{Str::title($resident->gender)}}</b></p>
				<p>Civil Status: <b>{{Str::title($resident->civil_status)}}</b></p>
				<p>Birthday: <b>{{Helper::date_format($resident->birthdate,'F d, Y')}}</b></p>
			</td>
			<td style="width: 25%; vertical-align: text-top; padding-top: 30px;">
				<img src="{{"{$certificate->resident->directory}/resized/{$certificate->resident->filename}"}}" style="width: 150px; height: 150px; border: 2px solid #333;">
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 40px;"><p style="text-align: justify; text-indent: 50px;">This certification is being used upon the request of of the above named person in connection with his/her application for <b>{{Str::upper($certificate->reason)}}</b>.</p></td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 40px; padding-bottom: 60px;"><p style="text-align: justify; text-indent: 50px;">Issued this <b style="text-decoration: underline">{{Helper::nice_order($certificate->created_at->format('j'))}} day of {{$certificate->created_at->format('F Y')}}</b> at Barangay San Antonio, Pasig City and valid for thirty <b>(30)</b> days only from the date hereof.</p></td>
		</tr>
	</table>
	<table width="100%" style="padding: 0px 25px;" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td style="width:30%; padding-top: 20px;">
				<p style="text-align: center;">______________________</p>
				<p style="text-align: center;">Signature of Applicant</p>
			</td>
			<td style="width: 20%;"></td>

			<td align="right"><p style="padding-top: -30px;padding-right: 10px;"><b>HON. THOMAS RAYMOND U. LISING</b><br>Punong Barangay &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
		</tr>
		<tr>
			<td style="padding-top: 20px;">
				<div style="margin:0px auto;height: 80px; width: 120px; border: 2px solid #333;"></div>
				<p style="font-size: 12px; font-style: italic;"><b>Not Valid Without Barangay Seal.</b></p>
			</td>
			<td></td>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td style="text-align: right; width: 60%;">Official Receipt No:</td>
						<td style="width: 40%;"> <b style="text-decoration: underline; margin-left: 10px;word-break: break-all;">{{$certificate->receipt_number}}</b></td>
					</tr>
					<tr>
						<td style="text-align: right;">Date:</td>
						<td> <b style="text-decoration: underline;margin-left: 10px;">{{$resident->created_at->format("m-d-Y")}}</b></td>
					</tr>
					<tr>
						<td style="text-align: right;">Amount:</td>
						<td> <b style="text-decoration: underline;margin-left: 10px;">P {{Helper::amount($certificate->amount)}}</b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
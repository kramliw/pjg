<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<p align="center" style="padding-top: -45px;" >Republic of the Philippines</p>
<h3 align="center" style="padding-top: -25px;text-transform: uppercase;">Barangay San Antonio</h3>
<p align="center" style="padding-top: -15px;">PasigCity, Metro Manila</p><br>

<h3 align="center">OFFICE OF THE LUPONG TAGAPAMAYAPA</h3><br>

<table width="100%" cellpadding="1" cellspacing="0" border="0">
	<tbody>
		<tr>
			<th colspan="10" style="font-weight: normal;" width="40%">{{$summons->complaint->complainant}}</th>
			<th colspan="10" style="font-weight: normal;" align="center" >&nbsp;&nbsp;&nbsp;&nbsp;Barangay Case No.{{$summons->case_no}}&nbsp;&nbsp;&nbsp;&nbsp;</th>
		</tr>
	

		<tr>
			<th colspan="10" style="font-weight: normal;">{{$summons->complaint->complainant_address}}</th>
			<th colspan="10" style="font-weight: normal;" align="center">For: {{$summons->complaint->description}}&nbsp;&nbsp;&nbsp;&nbsp;</th>
		</tr>

		<tr>
			<th colspan="10" style="font-weight: normal;">{{$summons->complaint->complainant_city}}</th>
			<th colspan="10" style="font-weight: normal;"></th>
		</tr>
		<tr>
			<th colspan="10" style="font-weight: normal;padding-top: -20px;"><br><p style="padding-left: 70px;padding-top: -15px;"><b>Complainant/s</b></p><p style="padding-left: 80px;">-against-</p></th>

			<th colspan="10" style="font-weight: normal;" rowspan="5"></th>
		</tr>

		<tr>
			<th colspan="10" style="font-weight: normal;padding-left: 10px ">{{$summons->complaint->respondent}}</th>

		</tr>
			<tr>
			<th colspan="10" style="font-weight: normal;padding-left: 10px;">{{$summons->complaint->respondent_address}}</th>
			
		</tr>
			<tr>
			<th colspan="10" style="font-weight: normal;padding-left: 10px ">{{$summons->complaint->respondent_city}}</th>
			
		</tr>

		<tr>
			<th colspan="10" style="font-weight: normal;padding-top: -15px;"><br><p style="padding-left: 70px;padding-top: -15px;"><b>Respondent/s</b></p></th>
		</tr>

	

	</tbody>
</table>

<h4 align="center" style="padding-top: -20px;">SUMMONS</h4>
<p style="text-align: justify;text-indent: 50px;">You are hereby summoned to appear before me in person, together with your witnesses, on the <b>{{Helper::date_format($summons->from_date ,'jS')}} and {{Helper::date_format($summons->to_date ,'jS')}} day of {{Helper::date_format($summons->to_date ,'F Y')}} at {{Helper::date_format($summons->time ,'h:i')}} o'clock in the {{Helper::date_format($summons->time ,'A') == 'AM' ? 'morning' : 'afternoon'}}</b> then and ther to answer to a complaint made before me, copy of which is attached hereto for mediation of your dispute with the complainant</p>
<p style="text-align: justify;text-indent: 50px;">You are hereby warned that if you refuse or wilfully fail to appear in obedience to this summmons, you may be barred from filing any counterclaim arisin from said complaint.</p>
<p style="text-align: justify;text-indent: 50px;">This {{Helper::date_format($summons->complaint->filed_date ,'jS')}} day of {{Helper::date_format($summons->complaint->filed_date ,'F Y')}}. </p><br><br>


<table width="100%" cellpadding="1" cellspacing="0" border="0">
	<tbody>
		<tr>
			<td align="right"><p style="padding-top: -30px;padding-right: 120px;"><b>HON. THOMAS RAYMOND U. LISING</b><br>Punong Barangay &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
		</tr>
		
	</tbody>
</table>

<p align="center">#7 Gen. mlvar Street, Barangay San Antonio, Pasig City<br>
Telephone Number: 570-6737 , 631-009</p>

</body>
</html>
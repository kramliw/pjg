<!DOCTYPE html>
<html>
<head>
  <title>mwss</title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
              <center>  <p>STATEMENT OF ACCOUNT</p>
                  <p>SMART</p>
                  <p>BRGY. SAN ANTONIO</p></center>

  <table class="table table-wrapper table-bordered">
           
              <tr>
                <th>Emloyee Name</th>
                <th>Mobile Number</th>
                <th>Account Number</th>
                <th>S.O.A Number</th>
                <th>Gross</th>
                {{-- <th>Amount Before Tax</th> --}}
                <th>2%</th>
                <th>5%</th>
                <th>Total</th>
             
              </tr>


              @php
                $grand_total = 0;
              @endphp
              @forelse($collection as $data)
              <tr>
                <td class="cell-detail">
                  <span>{{ $data->customer_name }}</span>
                  <span class="cell-detail-description">{{ $data->position }}</span>
                </td>
                <td>{{ $data->account_number }}</td>
                <td>{{ $data->soa }}</td>
                <td>{{ Helper::amount($data->total_gross) }}</td>
                @php
                  $two_percent = ($data->total_gross / 1.12) * .02; 
                  $five_percent = ($data->total_gross / 1.12) * .05; 
                @endphp
                <td>{{ Helper::amount($two_percent) }}</td>
                <td>{{ Helper::amount($five_percent)  }}</td>
                <td>{{ Helper::amount($data->total_gross - $two_percent - $five_percent) }}</td>
                @php
                  $grand_total += $data->total_gross;
                @endphp
                
              </tr>
              
              @empty
              <tr><td colspan="8" style="text-align: center">no data found</td></tr>
              @endforelse
              <tr>
                <td colspan="5"></td>
                <td>Grand Total</td>
                <td colspan="2">{{ Helper::amount($grand_total)}}</td>
              </tr>
            </table>
          <table class="pull-right">
  <tr>
    <td style="text-align: center;"><u>{{ strtoupper($accounting) }}</u></td>
  </tr>
  <tr>
    <td style="text-align: center;">ACCOUNTING</td>
  </tr>
</table>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>

<table width="40%" cellpadding="3" cellspacing="0" border="0"  style="padding-top: 50px;">
  <tbody>
    <tr>
      <td rowspan="3" colspan="14" style="color: white;">Logo</td>
      <td colspan="6" style="color: white;">Official Receipt <br> of the <br>Republic of the </td>
    </tr>
    <tr>
      <td  colspan="6" style="height: 50px;"> <br><p align="center" style="padding-bottom: -10;">{{$payment->receipt_number}}</p></td>
    </tr>
    <tr>
      <td  colspan="6" align="center"> {{$payment->created_at->format("m-d-Y")}}</td>
    </tr>
    <tr>
      <td  colspan="19" style="padding-left: 60px;"> Barangay San Antonio</td>
      <td  colspan="1" style="color:white;">Fund</td>
    </tr>
    <tr>
      @if($payment->reference_type == 'activity_clearance')
      <td colspan="20" style="font-weight: normal; padding-left: 60px;">{{ $payment->name_of_payor ? $payment->name_of_payor: 'N/A' }}</td>  
      @else
      <td colspan="20" style="font-weight: normal; padding-left: 60px;">{{ $payment->reference ? $payment->reference->business_name : 'N/A' }}</td>
      @endif
    </tr>
     <tr>
      <td colspan="20"></td>
    </tr>
    <tr>
      <td colspan="16" align="center" style="color:white">Nature of <br>Collection</td>
      <td colspan="2" align="center" style="color:white">Account <br> Code</td>
      <td colspan="2" align="center" style="color:white">Amount</td>
    </tr>
    @if($payment->reference_type == 'personal_certificate')
    <tr>
      <td colspan="16">{{  Str::title(str_replace('_', ' ', $payment->nature_of_collection ? $payment->nature_of_collection : 'N/A')) }}</td>
      <td colspan="2"></td>
      <td colspan="2" style="padding-left: 20px;"><b>{{ $payment->reference->amount ? Helper::amount($payment->reference->amount) : '0.00' }}</b></td>
    </tr>
     @elseif($payment->reference_type == 'activity_clearance')
     <tr>
      <td colspan="16">{{  Str::title(str_replace('_', ' ', $payment->nature_of_collection ? $payment->nature_of_collection : 'N/A')) }}</td>
      <td colspan="2"></td>
      <td colspan="2" style="padding-left: 20px;"><b>{{ $payment->amount ? Helper::amount($payment->amount) : '0.00' }}</b></td>
    </tr>
   @elseif($payment->reference_type == 'business_amendment')
    <tr>
      <td colspan="16">{{  Str::title(str_replace('_', ' ', $payment->nature_of_collection ? $payment->nature_of_collection : 'N/A')) }}</td>
      <td colspan="2"></td>
      <td colspan="2" style="padding-left: 20px;"><b>{{ $payment->reference->amount ? Helper::amount($payment->reference->amount) : '0.00' }}</b></td>
    </tr>
    <tr>
        <td colspan="16">Amendment Fee</td>
        <td colspan="2"></td>
        <td colspan="2" style="padding-left: 20px;"><b>50.00</b></td>
    </tr>
   
    @elseif($payment->reference_type == 'business_permit')
      <tr>
        <td colspan="16">{{  Str::title(str_replace('_', ' ', $payment->nature_of_collection ? $payment->nature_of_collection : 'N/A')) }}</td>
        <td colspan="2"></td>
        <td colspan="2" style="padding-left: 20px;"><b>{{ $payment->reference->amount ? Helper::amount($payment->reference->amount) : '0.00' }}</b></td>
      </tr>
      @if($payment->reference->business_plate)
      <tr>
        <td colspan="16">Business Plate</td>
        <td colspan="2"></td>
        <td colspan="2" style="padding-left: 20px;"><b>{{ $payment->reference->business_plate ? Helper::amount($payment->reference->business_plate) : '0.00' }}</b></td>
      </tr>
      @endif
      @if($payment->reference->penalty)
      <tr>
        <td colspan="16">Penalty</td>
        <td colspan="2"></td>
        <td colspan="2" style="padding-left: 20px;"><b>{{ $payment->reference->penalty ? Helper::amount($payment->reference->penalty) : '0.00' }}</b></td>
       </tr>
       @endif
   
      @if($payment->reference->amend_fee)
      <tr>
        <td colspan="16">Amendment Fee</td>
        <td colspan="2"></td>
        <td colspan="2" style="padding-left: 20px;"><b>{{ $payment->reference->amend_fee ? Helper::amount($payment->reference->amend_fee) : '0.00' }}</b></td>
      </tr>
     @endif
     
    
    @endif
    <tr>
      <td colspan="16" style="color:white">a</td>
      <td colspan="2" style="color:white">a</td>
      <td colspan="2" style="color:white">a</td>
    </tr>
    <tr>
      <td colspan="16" style="color:white">a</td>
      <td colspan="2" style="color:white">a</td>
      <td colspan="2" style="color:white">a</td>
    </tr>
    <tr>
      <td colspan="16" style="color:white">a</td>
      <td colspan="2" style="color:white">a</td>
      <td colspan="2" style="color:white">a</td>
    </tr>
   
    <tr>
      <td align="center" colspan="18" style="color:white">TOTAL</td>
      <td align="center" colspan="2">{{ $payment->amount }}</td>
    </tr>
    <tr>
      <td colspan="20"></td>
    </tr>
    <tr>
      <td colspan="20" style="color:white">Amount in Words</td>
    </tr>
    <tr>
      <td colspan="20">{{ str::title(Helper::convertNumberToWord($payment->amount))}} Peso only</td>
    </tr>
  </tbody>
</table>



</body>
</html>


<style type="text/css">
  
  h4{
  
  font-weight: normal;
  }
  
  h3{

    font-size: 20px;
  }

.code {
    text-decoration: underline;
    font-weight: bold;
  }

 .underline{
  border-bottom:1px solid #000000; 
  width:520px; 
 
}

.underline2{
  border-bottom:1px solid #000000; 
  width:200px; 
 
}

 .underline3{
  border-bottom:1px solid #000000; 
  width:550px; 
 
}

.date{
  border-bottom:1px solid #000000; 
  width:150px; 
 
}


 </style>


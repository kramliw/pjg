<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
	body{ font-family: 'Arial', sans-serif; font-size: 16px; }
	.page-break {
    page-break-after: always;
	}
	th.activity {
		border-bottom:none;
	}
	p{ margin: 3px 0px; padding: 0px; line-height: 20px; }
	h3{ font-weight: lighter;text-align: center; }
	td{ font-size: 12px; border-top: none;padding: 0px 0 10px 5px; }
	th{ font-size: 12px; font-weight: bold; }
	h4{ font-size: 19px; }

	 p.remarks{

	 	font-size: 10px;
	 }
	
</style>
</head>
<body>
<h4 align="center" style="padding-top: -15px;font-size: 13px;">Republic of the Philippines</h4>
<h4 align="center" style="padding-top: -20px;font-size: 13px;">City of Pasig</h4>
<h3 align="center" style="padding-top: -10px;font-size: 15px;">OFFICE OF THE SENIOR CITIZENS AFFAIRS (OSCA)</h3>
<p  align="center">www.pasigcity.gov.ph</p>
<h4 align="center" style="padding-top: -10px;">SENIOR CITIZENS APPLICATION FORM</h4>
<table width="100%"  cellspacing="0" cellpadding="3">
	<tbody>
		<tr>
			<th>BARANGAY: <u>&nbsp;&nbsp;&nbsp;&nbsp;{{$senior_citizen->barangay}}&nbsp;&nbsp;&nbsp;&nbsp;</u></th>
			<th align="right">Date: <u>&nbsp;&nbsp;&nbsp;&nbsp;{{date('F d, Y')}}&nbsp;&nbsp;&nbsp;&nbsp;</u></th>
		</tr>
	</tbody>
</table>
<h3 style="float: left;">NOTE: PLEASE PRINT</h3><br><br><br>

<table width="100%" style="padding-top: -30px;" cellspacing="0" cellpadding="3" border="1">
	<tbody>
		<tr>
			<th colspan="25">GIVEN NAME <br><p style="text-indent: 20px;">{{$senior_citizen->firstname}} &nbsp; {{$senior_citizen->suffix}}</p></th>
		</tr>
		<tr>
			<th colspan="25">MIDDLE NAME <br><p style="text-indent: 20px;">{{$senior_citizen->middlename}}</p></th>
		</tr>
		<tr>
			<th colspan="25">SURNAME <br><p style="text-indent: 20px;">{{$senior_citizen->lastname}}</p></th>
		</tr>
		<tr>
			<th colspan="10">COMPLETE ADDREES(Number, Street or Purok , Barangay) <br><p style="text-indent: 20px;">{{$senior_citizen->house_number}} {{$senior_citizen->address}} , Brgy. {{$senior_citizen->barangay}}</p></th>
			<th colspan="15">CONTACT NO: <br><p style="text-indent: 20px;">{{$senior_citizen->contact_no}}</p></th>
		</tr>
		<tr>
			<th colspan="10">DATE OF BIRTH: &nbsp;{{$senior_citizen->birthdate}} AGE: {{$years}}<br>&nbsp;</th>
			<th colspan="15">PLACE OF BIRTH: &nbsp;{{$senior_citizen->birth_place}}<br> &nbsp;</th>
		</tr>
		<tr>
			<th colspan="4" style="width: 15%;">RELIGION: <p>{{$senior_citizen->religion}}</p></th>
			<th colspan="4" style="width: 10%;" align="center">BLOOD TYPE:<p>{{$senior_citizen->blood_type}}</p></th>
			<th colspan="4" style="width: 10%;" align="center">CIVIL STATUS: <p>{{$senior_citizen->civil_status}}</p></th>
			<th colspan="4" style="width: 10%;" align="center">GENDER: <p>{{$senior_citizen->gender}}</p></th>
			<th colspan="5" style="width: 10%;" align="center">NO. YEARS IN PASIG: <p>{{$senior_citizen->years_in_pasig}}</p></th>
			<th colspan="4" style="width: 15%;" align="center">CITIZENSHIP: <p>{{$senior_citizen->citizenship}}</p></th>
		</tr>
		<tr>
			<th colspan="25"><p style="font-size: 15px;">REQUIREMENTS:</p></th>
		</tr>
		<tr>
			<th colspan="25" style="padding-left: 20px;">1. 2 PCS. 1 x 1 PICTURES<br>
							 2. XEROC COPY BIRTHCERTIFICATE/BAPTISMAL<br>
							 3. COMELEC CERTIFICATION<br>
							 4. BARANGAY CLEARANCE <br>
							 5. CEDULA (LATEST) XEROX COPY<br>
							 &nbsp;&nbsp;&nbsp;&nbsp;OTHER SUPPORTING DOCUMENT: ANY VALID ID<br>
							 &nbsp;&nbsp;&nbsp;&nbsp;(SSS, PRC, GSIS, TIN, PASSPORT, POSTAL ID)</th>
		</tr>
		<tr>
			<th align="center" colspan="4" style="padding-bottom: 60px;">THUMBMARK</th>
			<th colspan="21" style="padding-bottom: 60px;">SIGNATURE OVER PRINTED NAME OF APPLICANT:</th>
		</tr>
		<tr>
			<th  colspan="25" style="padding-bottom: 10px;"> CERTIFIED BY: <br><br><p style="text-indent: 100px;"> PRESIDENT - SENIOR CITIZEN ASSOCIATION</p></th>
			
		</tr>
		<tr>
			<th colspan="7">REMARKS <br><br> <p class="remarks">NEW<span style="color: white;">aaaaaaaaaa</span>___________ </p> <p class="remarks"> LOST <span style="color: white;">aaaaaaaaa</span>___________ </p> <p class="remarks"> CHANGE <span style="color: white;">aaaaaa</span>___________ </p> <p class="remarks">TRANSFER <span style="color: white;">aaaa</span>___________ </th>
			<th colspan="6" style="padding-bottom: 115px;width: 20%;" >ISSUED BY:</th>
			<th colspan="6" style="padding-bottom: 115px;width: 20%;">DATE ISSUED:</th>
			<th colspan="6" style="padding-bottom: 115px;width: 20%;">RECEIVED BY:</th>
		</tr>
	</tbody>
</table>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>

<h4 align="center" style="padding-top: -45px;">Republic of the Philippines</h4>
<h3 align="center" style="padding-top: -20px;">Barangay San Antonio</h3>
<h4 align="center" style="padding-top: -20px;">City of Pasig</h4>
<h3  align="center">APPLICATION FOR BARANGAY CLEARANCE</h3>
<p class="code" align="center" style="padding-top: -15px;">{{ $clearance->parent ? str_pad($clearance->parent_id, 8, "0", STR_PAD_LEFT) : str_pad($clearance->id, 6, "0", STR_PAD_LEFT) }}</p>


<div class="row"style="padding-top: -20px; " >
  <div class="col-6 col-md-4">
<p style="float: left;"> Date:</p>
</div>
<div class="col-6 col-md-4" align="center"> 
  <p style="float: left;" class="date">{{ $clearance->created_at->format("m-d-Y") }}</p>
</div>
<div class="col-6 col-md-4"> 
<p style="float: right;" class="underline2"> {{ Str::title($clearance->status) }} </p>
</div>

</div>
 <div class="row" style="padding-top: 20px; ">
    <div class="col" style="padding-left:50px;">
      Month/Day/Year
    </div>
  </div>

  </div>
<table  width="100%" cellpadding="3" cellspacing="0" border="1px" style="padding-top: 10px;" style="table-layout: fixed">
    <tbody>
    <tr>
      <th colspan="20" style="font-weight: normal;">Registered Name of Business Establishment</th>  
    </tr>
    <tr>
    <td colspan="20" style="height: 35px;">{{ $clearance->business_name }}</td>
    </tr>
    <tr>
      <th colspan="20"style="font-weight: normal;">Business Address</th>
    </tr>
    <tr align="center">
      <td style="height: 35px;word-wrap: break-word;"colspan="5"width="50%">{{ $clearance->unit_floor_no }}</td>
      <td style="height: 35px;"colspan="10" width="30%">{{ $clearance->building_name }}</td>
      <td style="height: 35px;"colspan="5" width="20%">{{ $clearance->street }}</td>
    </tr>
    <tr align="center">
      <td colspan="5" width="20%">Unit/Floor No.</td>
      <td colspan="10" width="30%">Name of Building</td>
      <td colspan="5" width="70%">Street</td>
    </tr>
    <tr>
      <td colspan="15"></td>
      <td colspan="5"></td>
    </tr>
    <tr align="center">
      <td colspan="15" width="50%">Taxpayer Identification Number(TIN)</td>
      <td colspan="5" width="50%"> SEC/DTI Registration Number</td>
    </tr>
    <tr align="center">
      <td style="height: 30px;" colspan="15" width="70%">{{ $clearance->tin ?: '---000' }}</td>
      <td style="height: 30px;" colspan="5" width="50%">{{ $clearance->registration_number }}</td>
    </tr>
     <tr >
      <td colspan="6" width="40%"> Telephone Number</td>
      <td colspan="6" width="40%"> Fax Number</td>
      <td colspan="6" width="40%"> Area of Establishment ( in sq .m )</td>
      <td colspan="2" width="10%"> Total Amount</td>
    </tr>
  <tr >
      <td style="height: 20px;" colspan="6" width="30%">{{ $clearance->telephone_number }}</td>
      <td style="height: 20px;" colspan="6" width="30%">{{ $clearance->fax_number }}</td>
      <td style="height: 20px;" colspan="6" width="10%">{{ Helper::amount($clearance->area) }}</td>
      <td style="height: 20px;" colspan="2" width="10%">{{ Helper::amount($clearance->total) }}</td>
    </tr>
  </tbody>
</table>

<div class="row">
  <div class="col">
<p style="float: left;"> Nature of Business Ownership : </p>
</div>
<div class="col" align="center"> 
  <p style="float: left;" class="underline"> {{ $clearance->business_ownership }}</p>
</div>
</div>

<table width="80%" cellpadding="3" cellspacing="0"  style="padding-top: 35px;">
  <tbody>
    <tr>
      <th style="visibility:hidden;"></th>
      <th style="visibility:hidden;"></th>
    </tr>
    <tr>
         <td style="float: left;">Nature of Business :</td>
         <td style="text-decoration: underline;"><b>{{ $clearance->nature_of_business}}</b></td>
    </tr>

  </tbody>
</table>

<div class="row">
  <div class="col">
<p style="float: left;"> MODE OF PAYMENT :</p>
</div>
<div class="col" align="center"> 
  <p style="float: left;" class="underline3"> CASH</p>
</div>
</div>

<table width="100%" cellpadding="3" cellspacing="0" style="padding-top: 50px;"border="1px">
   <tbody >
  
  <tr>
   <td style="padding-left: 10px;" width="40%">Is the place of business owned or leased ?</td>
   <td style="padding-left: 20px;" colspan="2">{{ Str::title($clearance->is_owned) }}</td>
  </tr>
   
  @if($clearance->is_owned === 'owned')
  <tr>
    <td align="center">Name of Owner</td>
    <td align="center" width="27%">Contact No.</td>
    <td align="center">Email</td>
  </tr>
  <tr>
     <td style="padding-left: 20px;">{{ $clearance->name_owner }}</td>
     <td style="padding-left: 20px;">{{ $clearance->address_owner }}</td>
     <td style="padding-left: 20px;">{{ $clearance->email_owner }}</td>
  </tr>
  @else
  <tr>
   <td align="center">Name of Lessor</td>
   <td align="center" width="27%">Address of Lessor</td>
   <td align="center">Email</td>
  </tr>
  <tr>
   <td style="padding-left: 20px;">{{ $clearance->name_lessor }}</td>
   <td style="padding-left: 20px;">{{ $clearance->address_lessor }}</td>
   <td style="padding-left: 20px;">{{ $clearance->email_lessor }}</td>
  </tr>
  
  @endif
</tbody>
</table>

<p style=" font-style: italic; font-weight: bold">I hereby certify that all the above information are true and correct.</p>
<div class="container">
  <div class="row" style="padding-top: -20px;">
    <div class="col">
      <p style="float: left;"> Printed Name of Applicant/Representative</p>
    </div>
    <div class="col"> 
      <p style="float: left; margin-left: 50px;" >{{ $clearance->name }}</p>
    </div>
  </div>
    <div class="col">
      <p style="float: left;"> Signature</p>
    </div>
    <div class="col" align="center"> 
      <p style="float: right;" >__________________________________________________</p>
    </div>
</div>

</div>




</body>
</html>


<style type="text/css">
  
  h4{
  
  font-weight: normal;
  }
  
  h3{

    font-size: 20px;
  }

.code {
    text-decoration: underline;
    font-weight: bold;
  }

 .underline{
  border-bottom:1px solid #000000; 
  width:520px; 
 
}

.underline2{
  border-bottom:1px solid #000000; 
  width:200px; 
 
}

 .underline3{
  border-bottom:1px solid #000000; 
  width:550px; 
 
}

.date{
  border-bottom:1px solid #000000; 
  width:150px; 
 
}


 </style>

 
<!doctype html>
<html lang="en">
<head>

  <title>Meralco</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    *{
      font-size: 10px
    }
    ul
    {
      list-style: none;
    }
    p
    {
      margin-left: 20%;
    }
    span
    {
      font-size: 11px
    }
  </style>


</head>
<body>




 <table class="table table-bordered text text-center" width="100%">

  <thead >
    <tr>
      <th  style="text-align: center;width:3%">No.</th>
      <th style="text-align: center;width: 10%">OLD SIN</th>
      <th style="text-align: center;width: 10%">NEW SIN</th>
      <th style="text-align: center;width: 15%">CUSTOMER NAME</th>
      <th style="text-align: center;width: 8%">FROM</th>
      <th style="text-align: center;width: 8%">TO</th>
      <th style="text-align: center;">TOTAL GROSS AMOUNT</th>
      <th style="text-align: center;">TOTAL BILL AMOUNT</th>
      <th style="text-align: center;">VAT SALES (C)</th>
      <th style="text-align: center;">VAT EXEMPT (D)</th>
      <th style="text-align: center;">LESS 2%</th>
      <th style="text-align: center;">LESS 5%</th>
      <th style="text-align: center;">TOTAL CURRENT NET AMOUNT</th>
    </tr>
  </thead>
  <tbody>
    @php
    $no = 1;
    @endphp
        @php
          $total_current_amount =0;
          $total_five_percent =0;
          $total_two_percent =0;
          $total_vat_exempt =0;
          $total_amount_gross =0;
          $total_bill_amount =0;
          $total_vat_sales =0;
          $total_vat_exempt =0;
        @endphp
    @foreach ($collection as $data)
     <?php $b =$data->vat_sales + $data->vat_exempt?>
                  <?php $e  = $b * .02?>
                  <?php $f  = $data->vat_sales * .05?>
    <tr>

      <td>{{ $no++ }}</td>
      <td>{{ $data->old_sin }}</td>
      <td>{{ $data->new_sin }}</td>
      <td>{{ $data->customer_name }}</td>
      <td>{{ date('M d Y', strtotime($data->from)) }}</td>
      <td>{{ date('M d Y', strtotime($data->to)) }}</td>
      <td>{{ Helper::amount($data->total_gross) }}</td>
   

      @if ($data->vat_sales == NULL && $data->vat_exempt == NULL )
      <td>0.00</td>
      @else
      <td>{{ Helper::amount($b) }}</td>
      @endif

      <td>{{ Helper::amount($data->vat_sales) }}</td>
      <td>{{ Helper::amount($data->vat_exempt) }}</td>


      @if ($data->vat_sales == NULL && $data->vat_exempt == NULL )
      <td>0.00</td>
      @else
      <td>{{ Helper::amount(($data->vat_sales + $data->vat_exempt) * .02,2) }}</td>
      @endif


      @if ($data->vat_sales == NULL && $data->vat_exempt == NULL )
      <td>0.00</td>
      @else
      <td>{{ Helper::amount(($data->vat_sales) * (.05)) }}</td>
      @endif


      <td>{{ Helper::amount($data->total_gross - $e - $f) }}</td>
    </tr>

    @php
      $total_current_amount +=  $data->total_gross - $e - $f;
      $total_five_percent +=  $data->vat_sales * .05;
      $total_two_percent +=  $data->vat_sales * .02;
      $total_vat_exempt += 1 ;
      $total_amount_gross += $data->total_gross;
      $total_bill_amount += $b;
      $total_vat_sales += $data->vat_sales;
      $total_vat_exempt += $data->vat_exempt;
    @endphp
    @endforeach
    <tr style="background:#ddd">
      <td>-</td>
      <td>TOTAL</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>{{ Helper::amount($total_amount_gross) }}</td>
      <td>{{ Helper::amount($total_bill_amount) }}</td>
      <td>{{ Helper::amount($total_vat_sales ) }}</td>
      <td>{{ Helper::amount( $total_vat_exempt) }}</td>
      <td>{{ Helper::amount($total_two_percent) }}</td>
      <td>{{ Helper::amount($total_five_percent) }}</td>
      <td>{{ Helper::amount($total_vat_exempt) }}</td>
    </tr>
  </tbody>
</table>


<br>

<table class="pull-right">
  <tr>
    <td style="text-align: center;"><u>{{ strtoupper($accounting) }}</u></td>
  </tr>
  <tr>
    <td style="text-align: center;">ACCOUNTING</td>
  </tr>
</table>



</div>
</div>

</body>
</html>
<!DOCTYPE html>
<html>
<head>
  <title>mwss</title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
              <center>  <p>STATEMENT OF ACCOUNT</p>
                  <p>PETRON</p>
                  <p>BRGY. SAN ANTONIO</p></center>

  <table class="table table-wrapper table-bordered">
          
              <tr>
                <th>Company Name</th>
                <th>P.O</th>
                <th>OR / Charge Invoice</th>
                <th>Liter(s)</th>
                <th>Date</th>
                <th>Amount</th>
                
              </tr>


              @php
                $grand_total = 0;
                $total_liter = 0;
              @endphp
              @forelse($collection as $data)
              <tr>
                <td>{{ $data->company_name }}</td>
                <td>{{ $data->po }}</td>
                <td>{{ $data->invoice }}</td>
                <td>{{ $data->liter }}</td>
                <td>{{date("M d, Y",strtotime($data->date))  }}</td>
                <td>{{ Helper::amount($data->total_amount) }}</td>
                
                @php
                  $grand_total += $data->total_amount;
                  $total_liter += $data->liter;
                @endphp
               
              </tr>
              
              @empty
              <tr><td colspan="6" style="text-align: center">no data found</td></tr>
              @endforelse
              <tr>
                <td></td>
                <td></td>
                <td style="text-align: right;">Grand Total</td>
                <td>{{ $total_liter }}</td>
                <td></td>
                <td >{{ Helper::amount($grand_total)}}</td>
              </tr>
            </table>

          <table class="pull-right">
  <tr>
    <td style="text-align: center;"><u>{{ strtoupper($accounting) }}</u></td>
  </tr>
  <tr>
    <td style="text-align: center;">ACCOUNTING</td>
  </tr>
</table>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
  <title>mwss</title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
              <center>  <p>STATEMENT OF ACCOUNT</p>
                  <p>MANILA WATER</p>
                  <p>BRGY. SAN ANTONIO</p></center>
 <table class="table table-bordered text text-center" width="100%">
            <thead>
              
              <tr>
                <th>Account Number</th>
                <th>Date Coverage</th>
                <th>Gross Amount</th>
                <th>Vat 2%</th>
                <th>EWT 5%</th>
                <th>Total Tax</th>
                <th>Net Amount</th>
              </tr>

              <tbody> 
                

                @forelse($collection as $bill)


                <?php 
                $two_percent_vat = ($bill->total_gross) / (1.12) * .02;
                $five_percent_vat = ($bill->total_gross) / (1.12) * .05;
                $total_net  = $bill->total_gross -  $five_percent_vat - $two_percent_vat;

                ?>
                <tr>
                  <td>{{ $bill->account_number }}</td>
                  <td>{{ date('M d, Y', strtotime($bill->from)) }} - {{ date('M d, Y',strtotime($bill->to))  }}</td>
                  <td>{{ number_format($bill->total_gross,2) }}</td>
                  <td>{{ number_format($two_percent_vat,2)}}</td>
                  <td>{{ number_format($five_percent_vat ,2) }}</td>
                  <td>{{ number_format($bill->total_gross ,2) }}</td>
                  <td>{{ number_format( $total_net ,2) }}</td>
                 
                  
                </tr>
                @empty
                @endforelse
              
    
              </tbody>
            </thead>
            <tbody>
          
            </tbody>
          </table>

          <table class="pull-right">
  <tr>
    <td style="text-align: center;"><u>{{ strtoupper($accounting) }}</u></td>
  </tr>
  <tr>
    <td style="text-align: center;">ACCOUNTING</td>
  </tr>
</table>
</body>
</html>
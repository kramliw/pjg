<!DOCTYPE html>
<html>
<head>
	<title></title>
	
</head>
<body>
    <table  id="myTable"width="100%" cellpadding="1" cellspacing="0" border="1">
    	<thead>
            <tr  align="center">
                <th>Case No.</th>
                <th colspan="2">Parties</th>
                <th colspan="2">Nature of Case/s Filed</th>
                <th>Others</th>
                <th colspan="4">Action Taken</th>
                <th colspan="2"></th>
            </tr>
            <tr>
             	<th></th>
             	<th><i>Complainant/s</i></th>
             	<th><i>Respondents</i></th>
             	<th><i>Civil</i></th>
             	<th><i>Criminal</i></th>
             	<th></th>
             	<th>Thru Mediation</th>
             	<th>Thru Conciliation</th>
             	<th>Dismissed</th>
             	<th>Certified Cases</th>
             	<th>Total</th>
             	<th>Remarks</th>
            </tr>     
    	</thead>
    	<tbody>
            <?php $civil = 0; $criminal = 0; $mediation = 0; $conciliation = 0;  $dismissed = 0;  $certified_cased = 0; $others = 0; $total = 0; ?>
    		@forelse($complaints as $index => $complaint)
            <?php $total = 0; ?>
    		<tr>
    			<td>{{$complaint->case_no}}</td>
    			<td>{{$complaint->complainant}}</td>
    			<td>{{$complaint->respondent}}</td>
    			
    			@if($complaint->case_nature == 'civil'){
                <?php $civil += 1; ?>
    			<td>{{$complaint->case_nature}}</td>
    			<td></td>
    			<td></td>
    			}@elseif($complaint->case_nature == 'criminal'){
                <?php  $criminal += 1; ?>
    			<td></td>
    			<td>{{$complaint->description}}</td>
    			<td></td>
    			}@else
                <?php  $others += 1; ?>
    			<td></td>
    			<td></td>
    			<td>{{$complaint->description}}</td>
    			@endif
                @if($complaint->thru_mediation)
                    <?php $mediation += 1; $total +=1; ?>
                @endif
                @if($complaint->thru_conciliation)
                    <?php $conciliation +=1; $total +=1; ?>
                @endif
                 @if($complaint->dismissed)
                    <?php $dismissed +=1; $total +=1; ?>
                @endif
                 @if($complaint->certified_cased)
                    <?php $certified_cased +=1; $total +=1; ?>
                @endif

              
    			<td>{{$complaint->thru_mediation}}</td>
    			<td>{{$complaint->thru_conciliation}}</td>
    			<td>{{$complaint->dismissed}}</td>
    			<td>{{$complaint->certified_cased}}</td>
    			<td>{{$total }}</td>
    			<td>{{$complaint->remarks}}</td>
    		</tr>
    		@empty
    		<td colspan="4"><i>No record found yet.</i></td>
    		@endforelse
           
    		<tr>
    			<td></td>
    			<td></td>
    			<td>Total</td>
    			<td>{{ $civil }}</td>
                <td>{{ $criminal }}</td>
                <td>{{ $others }}</td>
                <td>{{ $mediation }}</td>
                <td>{{ $conciliation }}</td>
                <td>{{ $dismissed }}</td>
                <td>{{ $certified_cased }}</td>
                <td></td>
                <td></td>
    		</tr>
    	</tbody>
    </table>
</body>
</html>



<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<p align="center" style="padding-top: -45px;" >Republic of the Philippines</p>
<h3 align="center" style="padding-top: -25px;text-transform: uppercase;">Barangay San Antonio</h3>
<p align="center" style="padding-top: -15px;">Pasig City, Metro Manila</p>

<h3 align="center">OFFICE OF THE LUPONG TAGAPAMAYAPA</h3>

<table width="100%" cellpadding="1" cellspacing="0" border="0">
	<tbody >
		<tr>
			<th colspan="10" style="font-weight: normal;padding-left: 10px;">{{$complaint->complainant}}</th>
			<th colspan="10" style="font-weight: normal;">Barangay Case No.<u>&nbsp;&nbsp;&nbsp;&nbsp;{{$complaint->case_no}}&nbsp;&nbsp;&nbsp;&nbsp;</u></th>
		</tr>
		
		<tr>
			<th colspan="10" style="font-weight: normal;padding-top: -20px;">______________________________</th>
			<th colspan="10" style="font-weight: normal;"></th>
		</tr>
		<tr>
			<th colspan="10" style="font-weight: normal;padding-left: 10px;">{{$complaint->complainant_address}}</th>
			<th colspan="10" style="font-weight: normal;">For:<u>&nbsp;&nbsp;&nbsp;&nbsp;{{$complaint->description}}&nbsp;&nbsp;&nbsp;&nbsp;</u></th>
		</tr>
		<tr>
			<th colspan="10" style="font-weight: normal;padding-top: -20px;">______________________________</th>
			<th colspan="10" style="font-weight: normal;"></th>
		</tr>
		<tr>
			<th colspan="10" style="font-weight: normal;padding-left: 10px;">{{$complaint->complainant_city}}</th>
			<th colspan="10" style="font-weight: normal;"></th>
		</tr>
		<tr>
			<th colspan="10" style="font-weight: normal;padding-top: -20px;">______________________________<p style="padding-left: 70px;padding-top: -15px;">Complainant/s</p><p style="padding-left: 80px;">-against-</p></th>

			<th colspan="10" style="font-weight: normal;" rowspan="7"></th>
		</tr>
		<tr>
			<th colspan="10" style="font-weight: normal;padding-left: 10px ">{{$complaint->respondent}}</th>
		</tr>
		<tr>
			<th colspan="10" style="font-weight: normal;padding-top: -20px;">______________________________</th>
			
		</tr>
		<tr>
			<th colspan="10" style="font-weight: normal;padding-left: 10px;padding-top: 20px;">{{$complaint->respondent_address}}</th>
		</tr>
		<tr>
			<th colspan="10" style="font-weight: normal;padding-top: -15px;">______________________________</th>
			
		</tr>
		<tr>
			<th colspan="10" style="font-weight: normal;padding-left: 10px;">{{$complaint->respondent_city}}</th>
		</tr>
		<tr>
			<th colspan="10" style="font-weight: normal;padding-top: -15px;">______________________________<p style="padding-left: 70px;padding-top: -15px;">Respondent/s</p></th>
		</tr>
		
	</tbody>
</table>
<h4 align="center" style="padding-top: -40px;">COMPLAINT</h4>
<p style="text-align: justify;">I/WE hereby complain against above named respondent/s for violation my/our rights and interests in the following manner</p>
<p style="text-align: justify; text-indent: 50px;" >___<u>{!! $complaint->complaint_description !!}</u>___</p>

<p style="text-align: justify;">THEREFORE, I/WE pray that the following relief/s be granted to me/us in accordance with law and/or equity</p>
<p  style="text-align: justify; text-indent: 50px;">___<u>{!! $complaint->condition_description !!}</u>___</p>


<p>Made this ____<u>{{ Helper::date_format($complaint->complaint_date, 'd') }}</u>____ day of _______<u>{{ Helper::date_format($complaint->complaint_date, 'F') }}</u>_______, 2019.</p><br>

<p align="right" style="padding-top: -30px;">_______________________________</p>
<p align="right" style="padding-right: 70px;padding-top: -15px;">Complainant/s</p><br>
<p style="padding-top: -30px;">Received and filed this ____<u>{{ Helper::date_format($complaint->filed_date, 'd') }}</u>____ day of _______<u>{{ Helper::date_format($complaint->filed_date, 'F') }}</u>_______, 2019.</p><br>


<table width="100%" cellpadding="1" cellspacing="0" border="0">
	<tbody>
		<tr>
		<p style="padding-top: -30px;padding-right: 120px;"><b>HON. THOMAS RAYMOND U. LISING</b><br>Punong Barangay &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
		</tr>
		
	</tbody>
</table>	
</body>
</html>
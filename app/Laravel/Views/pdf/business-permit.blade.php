<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Business Permit</title>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<style>
	
		@font-face {
		     font-family: 'Tahoma';
     		src: url({{storage_path('fonts/Tahoma.ttf')}})  format('truetype');
     		src: local('Tahoma'), local('Tahoma'), 
		    url({{storage_path('fonts/Tahoma.eot')}}) format('embedded-opentype'), 
		    url({{storage_path('fonts/Tahoma.woff2')}}) format('woff2'), 
		    url({{storage_path('fonts/Tahoma.woff')}}) format('woff'),  
		    url({{storage_path('fonts/Tahoma.svg')}}) format('svg');
		    font-weight: 600;
		    font-style: normal
		}
		body{ 

			font-family: 'Tahoma', sans-serif;
		}
		.page-break {
		    page-break-after: always;
		}
		p{ margin: 3px 0px; padding: 0px; line-height: 20px; }
	

  </style>
</head>
<body>
	<table width="100%" style="padding: 0px 25px; margin-top: 70px;" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td colspan="2" style="text-align: center; letter-spacing: 4px; font-weight: bold; font-size: 24px;">CLEARANCE</td>
		</tr>
		<tr>
			<td colspan="1" style="text-align: right;padding-left: 120px;">No. <b style="text-decoration: underline;">BSA-{{ $clearance->parent ? str_pad($clearance->parent_id, 8, "0", STR_PAD_LEFT) : str_pad($clearance->id, 6, "0", STR_PAD_LEFT) }}</b></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: right;padding-right: 20px;">Plate No. <b>{{ $clearance->business_plate_number }}</td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 15px;"><p style="text-align: justify; text-indent: 50px;">To whom it may concern:</p></td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 10px;"><p style="text-align: justify; text-indent: 50px;">The Barangay Government of San Antonio, Pasig City has no objection to the <b>{{ $clearance->parent_id ? 'renewal' : 'issuance' }} for CY{{ Carbon::now()->year }} {{ $clearance->parent_id ? 'of the' : 'of a new' }}</b></p></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center; letter-spacing: 2px; font-weight: bold; font-size: 18px; padding-top: 15px;">BUSINESS PERMIT</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center; letter-spacing: 4px; font-size: 16px; padding-top: 10px;">to</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center; letter-spacing: 2px; font-weight: bold; font-size: 18px; padding-top: 15px; text-transform: uppercase;">{{ $clearance->business_name }}</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center; padding-top: 20px; text-transform: uppercase;font-size: 12px;"><p>{{ $clearance->unit_floor_no }} {{ $clearance->building_name}} {{ $clearance->street }} {{ $clearance->subdivision }}<br>BARANGAY SAN ANTONIO, PASIG CITY<br>SEC/DTI Registration No: {{ $clearance->registration_number }}<br>Floor Area: {{ Helper::amount($clearance->area) }} Square Meters</p></td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 10px;"><p style="text-align: justify; text-indent: 50px;font-size: 14px;">The issuance of this Clearance is subject to the condition that the above enterprise has complied with and shall continue to comply with all existing national and local government laws, rules & regulations and city & barangay ordinances pertaining to the operation of a business establishment and that its operations shall not be an environmental and social hazard.</p></td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 10px;"><p style="text-align: justify; text-indent: 50px;font-size: 14px;">This Clearance shall be considered automatically revoked, null and void should the above enterprise be found to have violated or failed to comply with the above condition.</p></td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 10px;"><p style="text-align: justify; text-indent: 50px;font-size: 14px;">Issued this {{Helper::nice_order($clearance->created_at->format('j'))}} day of {{$clearance->created_at->format('F Y')}} at Barangay San Antonio, Pasig City and valid up to {{ Carbon::parse($clearance->date_expiry)->format('F d, Y') }}.</p></td>
		</tr> 
		{{-- <tr>
			<td colspan="2" style="padding-top: 40px;"><p style="text-align: justify; text-indent: 50px;">Issued this <b style="text-decoration: underline">{{Helper::nice_order($certificate->created_at->format('j'))}} day of {{$certificate->created_at->format('F Y')}}</b> at Barangay San Antonio, Pasig City and valid for thirty <b>(30)</b> days only from the date hereof.</p></td>
		</tr> --}}
	</table>
	<table width="100%" style="padding: 0px 25px;" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td style="width:30%; padding-top: 50px;">
				<p style="text-align: left;">Conforme:</p>
				<p style="text-align: center;">______________________</p>
				<p style="text-align: center;">(Authorized Representative)</p>
			</td>
			<td style="width: 20%;"></td>

			
			<td align="right"><img src="{{ public_path('assets/img/capt_sign.png') }}" width="200" height="100" style="padding-right: 40px;"><p style="padding-top: -10px;padding-right: 10px;"><b>HON. THOMAS RAYMOND U. LISING</b><br>Punong Barangay &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
		</tr>
		<tr>
			<td style="padding-top: 20px;">
				<!-- <div style="margin:0px auto;height: 80px; width: 120px; border: 2px solid #333;"></div> -->
				<p style="font-size: 12px; font-style: italic;"><b>Not valid without barangay seal.</b></p>
				<p style="font-size: 12px;">To be displayed in your respective offices</p>
			</td>
			<td></td>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-top: 20px;">
					<tr>
						<td style="text-align: right; width: 60%;">Official Receipt No:</td>
						<td style="width: 40%;"> <b style="text-decoration: underline; margin-left: 10px;">{{ $clearance->receipt_number }}</b></td>
					</tr>
					<tr>
						<td style="text-align: right;">Date:</td>
						<td> <b style="text-decoration: underline;margin-left: 10px;">{{$clearance->created_at->format("m-d-Y")}}</b></td>
					</tr>
					<tr>
						<td style="text-align: right;">Clearance Fee PHP:</td>
						<td> <b style="text-decoration: underline;margin-left: 10px;">P {{ Helper::amount($clearance->amount) }}</b></td>
					</tr>
					@if($clearance->docs)
					<tr>
						<td style="text-align: right;">Documentary Stamp:</td>
						<td> <b style="text-decoration: underline;margin-left: 10px;">P {{  Helper::amount($clearance->docs ?: 0 ) }}</b></td>
					</tr>
					@endif
					
					
					
					@if($clearance->business_plate)
					<tr>
						<td style="text-align: right;">Business Plate:</td>
						<td> <b style="text-decoration: underline;margin-left: 10px;">P {{ Helper::amount($clearance->business_plate ?: 0) }}</b></td>
					</tr>
					@endif
					@if($clearance->penalty)
					<tr>
						<td style="text-align: right;">Penalty:</td>
						<td> <b style="text-decoration: underline;margin-left: 10px;">P {{ Helper::amount($clearance->penalty ?: 0) }}</b></td>
					</tr>
					@endif

					<tr>
					@if($clearance->amend_fee)
						<td style="text-align: right;">Amend Fee:</td>
						<td> <b style="text-decoration: underline;margin-left: 10px;">P {{  Helper::amount($clearance->amend_fee?: 0 ) }}</b></td>
					@else
						
							@if(in_array('area_of_establishment', $remarks))
								<td style="text-align: right;">Amend Fee:</td>
								<td> <b style="text-decoration: underline;margin-left: 10px;">P {{  Helper::amount($amendment->amount - $clearance->amount + $clearance->amend_fee  ?: 0 ) }}</b></td>
							
						@endif
					@endif
					</tr>
					<!-- <tr>
						<td style="text-align: right;">Total:</td>
						<td> <b style="text-decoration: underline;margin-left: 10px;">P {{ Helper::amount($clearance->total ?: 0) }}</b></td>
					</tr> -->
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
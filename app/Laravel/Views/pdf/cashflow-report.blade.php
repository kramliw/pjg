<!DOCTYPE html>
<html>
<head>
	<title></title>

<style type="text/css">
	td{
		font-size: 12px;
	}
</style>
</head>
<body>
<h2>Record</h2> <h5>{{$count}} - Records</h5><br>
<table width="100%" style="padding-top: -20px;" cellspacing="0" cellpadding="3" border="1">
	<thead>
		 <tr>
                <th>Type</th>
                <th>Description</th>
                <th>Amount</th>
                <th>Payment Method</th>
                <th>Reference</th>
                <th>Cashier</th>
               
              </tr>
	</thead>
	<tbody>
      	@forelse($payments as $index => $payment)
      	<tr>
	        <td> 
		        @if($payment->reference_type == 'business_permit')
	               <span>CLEARANCE - BUSINESS</span>
		        @endif
		        @if($payment->reference_type == 'personal_certificate')
		           <span>CLEARANCE - PERSONAL</span>
		        @endif
		        @if($payment->reference_type == 'activity_clearance')
		           <span>CLEARANCE - ACTIVITY</span>
		        @endif
	         	@if($payment->reference_type == 'business_amendment')
                	<span>BUSINESS AMENDMENT</span>
             	@endif
              
	        </td>
	        <td> 
		        @if($payment->reference_type == 'business_permit' or $payment->reference_type == 'business_amendment' )
	              <span>{{$payment->reference ? $payment->reference->business_name : '-' }}</span>
	            @endif
	            @if($payment->reference_type == 'personal_certificate')
	              <span>{{$payment->reference ? $payment->reference->resident->full_name : '-' }}</span>
	            @endif
	            @if($payment->reference_type == 'activity_clearance')
	              <span>{{  Str::title($payment->name_of_payor) ?: '-' }}</span>
	            @endif
        	</td>
		    <td> 
		      <span>{{ Helper::amount($payment->amount) }}</span>
		    </td>
		    <td> 
		      <span>{{ Str::title($payment->payment_method) }}</span>
		    </td>
		    <td> 
		      <span>{{ Str::title($payment->receipt_number) }}</span>
		    </td>
		    <td> 
		      <span>{{ $payment->user ? $payment->user->name : 'N/A' }}</span>
		    </td>
      	</tr>
      @empty
      <td colspan="7" class="text-center"><i>No transactions yet.</i> </td>
      @endforelse

       <tfoot>
            <tr>
                <td colspan="2"><strong>TOTAL</strong></td>
                <td>
                    <div>Cash:<strong>{{Helper::amount($total_payment)}}</strong><br>Checque:<strong>{{Helper::amount($total_checque)}}</strong></div>
                </td>
                <td colspan="3"></td>
            </tr>                                    
        </tfoot>
    </tbody>
	
</table>
</body>
</html>
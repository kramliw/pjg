@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <form method="POST" action="" enctype="multipart/form-data">
  {!!csrf_field()!!}
  <div class="row">
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider"><strong>Export</strong> Data Header Form<span class="panel-subtitle">All reports to be export in <strong>worksheet</strong> file.</span></div>
        <div class="panel-body">
            <div class="form-group {{$errors->first('report_type') ? 'has-error' : NULL}}">
              <label>Report Type</label>
              {!!Form::select('report_type',$report_types,old('report_type'),['class' => "form-control select2"])!!}
              @if($errors->first('report_type'))
              <span class="help-block">{{$errors->first('report_type')}}</span>
              @endif
            </div>
            <div class="form-group {{$errors->first('date') ? 'has-error' : NULL}}">
              <label>Date</label>
              <input type="text" name="date" class="form-control datepicker" data-min-view="2" data-date-format="yyyy-mm-dd" value="{{old('date',Helper::date_db($date_today))}}">
              @if($errors->first('date'))
              <span class="help-block">{{$errors->first('date')}}</span>
              @endif
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row xs-pt-15">
    <div class="col-xs-6">
        <button type="submit" class="btn btn-space btn-primary">Export Record</button>
        <a href="{{route('system.dashboard')}}" class="btn btn-space btn-default">Cancel</a>
    </div>
  </div>
  </form>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
<style>
  .bg-success{ background: #34a853!important; color: #fff!important; font-weight: 600!important; }
  .bg-danger{ background: #ea4335!important;color: #fff!important; }
  
  .waybill-container .form-control{ margin-bottom: 5px; }
  .form-control.text-success{ font-weight: 600; }
  .input-wrapper{ position: relative; }
  .input-wrapper .btn-wrapper{ position: absolute; right: 12px; top: 12px; }
</style>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $.fn.check_delivery = function(){
    var ver_input = $(this).children(".form-control");
    var _parent = $(this);
    $.each($("#waybill_scheduled .input-wrapper"),function(index,element){
        var _input = $(this).children('.form-control');
        var _wrapper = $(this);
        if(_input.val() != ""){
          if(_input.val() == ver_input.val()){
            _input.addClass("bg-success")
            ver_input.addClass("bg-success");
            ver_input.removeClass("bg-danger");

          }else{
            ver_input.addClass("bg-danger")
          }
          
          // console.log(_input.val())
        }
    })

    // $(this).waybill_remitted();
  }

  $.fn.check_duplicate = function(){
    var cur_index = $(this).parents(".input-wrapper").index();
    var cur_input = $(this);
    var new_value = cur_input.val();
    var _super = $(this).parents(".waybill-container");
    var opposite_id = _super.attr("id") == "waybill_metromanila" ? "#waybill_provincial" : "waybill_metromanila";
   
    var response = false;
    $.each(_super.children('.input-wrapper'),function(index,element){
      if(index != cur_index){
        var input = $(this).children('.form-control');
        if(input.val() == new_value){
          input.addClass('bg-danger')
          cur_input.addClass('bg-danger');
          response = true;
        }else{
          if(input.hasClass('bg-danger') === false) { input.removeClass('bg-danger')}
        }
      }
    });

    if(response === false) { cur_input.removeClass('bg-danger');}
    return response;
  }

  $.fn.remove_blank = function(){
    var _super = $(this).find(".waybill-container");
    $.each(_super.children('.input-wrapper'),function(index,element){
        var _input = $(this).children('.form-control');
        if(_input.val() == "" && (index != (_input.parents('.waybill-container').children('.input-wrapper').length - 1) && index != 0 )){
          _input.parents('.input-wrapper').fadeOut(200,function(){
            $(this).remove();
          })
        }
      });
  }
  $.fn.get_total = function(){
    var _footer = $(this).siblings(".panel-footer");
    if(typeof _footer === "object"){
      var _super = $(this).find(".waybill-container");
      var num = 0;
      $.each(_super.children('.input-wrapper'),function(index,element){
        var _input = $(this).children('.form-control');
        if(_input.val() != "") num++;
      });
      _footer.find(".total").text(num)
    }
  }
  $.fn.add_row = function(){
    //enter key
    var _super = $(this).parents(".waybill-container");
    var _parent = $(this).parents(".input-wrapper");
    var cur_length = _super.children('.input-wrapper').length; //get current length
    var _next = cur_length;
    var last_input = _super.children('.input-wrapper').eq(cur_length-1).children('.form-control');
    if(last_input.val() != ""){
      var with_duplicate = last_input.check_duplicate();
      if(with_duplicate === false){
        var new_input = '<div class="input-wrapper">';
        _parent.children(".form-control").removeClass("bg-success bg-danger")
        new_input += _parent.html();
        new_input += "</div>" 
        _super.append(new_input)
        _super.children('.input-wrapper').eq(cur_length).children('.form-control').val("")
      }else{
        last_input.focus()
      }
      
    }else{
      _next-=1;
    }
    _super.children('.input-wrapper').eq(_next).children('.form-control').focus();
      
    $.each(_super.children('.input-wrapper'),function(index,element){
      if(index != 0 ){
        var _btn = $(this).children('.btn-wrapper').children("button");

        if(_btn.hasClass("btn-add") === true){
          _btn.removeClass("btn-add").addClass("btn-remove");
          var _icon = _btn.children("i");
          _icon.removeClass('text-primary mdi-plus').addClass("text-danger mdi-block")
        }
      }
    });

    if(_super.attr("id") == "waybill_delivered"){
      _parent.check_delivery();
    }

    // _parent.parents(".panel-body").get_total();
    // _parent.waybill_remitted();

  }
  $(function(){


    $('.datepicker').datetimepicker({autoclose: true})
    $('.waybill-container')
    .delegate('.input-wrapper .btn-remove','click',function(){
      var _parent = $(this).parents('.input-wrapper');
      var _prev_rec = _parent.siblings().eq(_parent.index()-1);
      var _panel_body = _parent.parents(".panel-body");
      _parent.fadeOut(500,function(){
        $(this).remove();
        _panel_body.get_total();
        _prev_rec.check_duplicate();
        _parent.check_delivery();

        // _prev_rec.check_remittance();
      });
    })
    .delegate('.input-wrapper .form-control','focusout',function(){
        var _parent = $(this).parents(".panel-body");
        _parent.remove_blank();
        _parent.get_total();
      _parent.check_delivery();

        // _parent.check_remittance();
    })
    .delegate('.input-wrapper .btn-add','click',function(){
        $(this).add_row();
    })
    .delegate('.input-wrapper .form-control','keypress',function(e){
        if(e.keyCode == 13 || e.which == 13){
          
          $(this).add_row();
          return false;
        }
    });
  });
</script>
@stop


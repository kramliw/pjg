@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default">
        <div class="panel-heading panel-heading-divider">Create Record Form</div>
        <div class="panel-body">
          <form method="POST" action="">
            {!!csrf_field()!!}
            
            <div class="form-group col-lg-4 {{$errors->first('time_start') ? 'has-error' : NULL}}">
              <label>Time start</label>
              <input type="text" placeholder="" class="form-control" name="time_start" value="{{old('time_start')}}">
              @if($errors->first('time_start'))
              <span class="help-block">{{$errors->first('time_start')}}</span>
              @endif
            </div>
             <div class="form-group col-lg-4 {{$errors->first('time_end') ? 'has-error' : NULL}}">
              <label>Time end</label>
              <input type="text" placeholder="" class="form-control" name="time_end" value="{{old('time_end')}}">
              @if($errors->first('time_end'))
              <span class="help-block">{{$errors->first('time_end')}}</span>
              @endif
            </div>
             <div class="form-group col-lg-4 {{$errors->first('description') ? 'has-error' : NULL}}">
              <label>Description</label>
              <input type="text" placeholder="" class="form-control" name="description" value="{{old('description')}}">
              @if($errors->first('description'))
              <span class="help-block">{{$errors->first('description')}}</span>
              @endif
            </div>

           
      
              <div class="col-lg-12 text-right">
                  <button type="submit" class="btn btn-space btn-lg  btn-success">Add</button>
                  <a href="{{route('system.employee.index')}}" class="btn btn-lg  btn-space btn-default">Cancel</a>
              </div>
           

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('.datepicker').datetimepicker({autoclose: true})
  });
</script>
@stop


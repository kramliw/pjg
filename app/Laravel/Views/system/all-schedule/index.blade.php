@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-table">
        <div class="row table-filters-container">
          <div class="panel-heading">Search Filters</div>

          <hr>

           <form action=""  method="get" id="search_filter" >
       

          
          <div class="form-group col-md-4">
            <label>Date From</label>
             <input type="date" name="date_from" class="form-control input-md" value="{{ old('date_from')}}" placeholder="Date From">
            </div>
            <div class="form-group col-md-4">
                <label>Date To</label>
             <input type="date" name="date_to" value="{{ old('date_to')}}" class="form-control input-md">
            </div>
               <div class="form-group col-md-4">
                <label style="color: #fff">.</label><br>
               <input type="submit" value="Filter" class="btn btn-lg btn-success">
               <a href="{{ route('system.schedule_list.index') }}" class="btn btn-lg btn-success">Clear Filter</a>
              </div>
           
        
          
          <button type="submit" style="display: none;"></button>
          </form>
          {{-- <form action="" id="search_filter" >
          <div class="col-md-3 table-filters"><span class="table-filter-title"></span>
            <div class="filter-container">
              <label class="control-label">From</label>
               
            </div>
          </div>
           <div class="col-md-3 table-filters"><span class="table-filter-title"></span>
            <div class="filter-container">
              <label class="control-label">To</label>
                <input type="date" name="date_to" class="form-control input-md"> 
                
            </div>
          </div>

          <div class="col-md-3">
                  <a href="#" class="btn btn-lg  btn-success btn-search"><i class="mdi mdi-search"></i>Filter</a>
                  <a href="{{route('system.schedule_list.index')}}" class="btn btn-lg btn-success btn-search"><i class="mdi mdi-search"></i> Clear</a>
          
          </div>
          <button type="submit" style="display: none;"></button>
          </form> --}}
        </div>
        <div class="panel-heading">Record Data
          <div class="tools dropdown">
            <a href="#" type="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
            <ul role="menu" class="dropdown-menu pull-right">
              <li><a href="{{route('system.employee.create')}}">Add new Employee</a></li>
            
            </ul>
          </div>
        </div>
        <div class="panel-body">

          <div class="col-md-12">
            
          <table class="table table-bordered table-wrapper" id="aTable" >
            <thead >
              <tr>
                <th style="text-align: center">Date</th>
                <th style="text-align: center">6:00 AM  - 3:00 PM SHIFT </th>
                <th style="text-align: center">Ward</th>
              </tr>
            </thead>
            <tbody>
            
                {{-- expr --}}
             
              @forelse ($schedules_am as $schedule)
                <tr style="text-align: center">
                  <td style="text-align: center">{{ date('M d, Y',strtotime($schedule->date)) }}</td>
                  <td style="text-align: center">{{ $schedule->employee->full_name}}</td>
                  <td style="text-align: center">{{ $schedule->ward->ward_name }}</td>
              
                </tr>
                @empty
                <tr>
                  <td colspan="4" style="text-align: center;"> No Data Found</td>
                </tr>
              @endforelse
            
            </tbody>
          </table>
          
          </div>
<div class="col-md-12" style="margin-bottom: 10px">
  <hr>

</div>


          <div class="col-md-12">
            
          <table class="table table-bordered table-wrapper" id="bTable">
            <thead class="thead-dark">
              <tr>
                <th style="text-align: center">Date</th>
                <th style="text-align: center">3:00 PM  - 11:00 PM SHIFT </th>
                <th style="text-align: center">Ward</th>
  
             
            
              </tr>
            </thead>
            <tbody>

              @forelse ($schedules_pm as $schedule)
                <tr>
                  <td style="text-align: center" id="dates">{{ date('M d, Y',strtotime($schedule->date)) }}</td>
                  <td style="text-align: center">{{ $schedule->employee->full_name}}</td>
                  <td style="text-align: center">{{ $schedule->ward->ward_name }}</td>
              
                </tr>
                @empty
                <tr>
                  <td colspan="4" style="text-align: center;"> No Data Found</td>
                </tr>
              @endforelse
             
            </tbody>
          </table>
          
          </div>
<div class="col-md-12" style="margin-bottom: 10px">
  <hr>

</div>


          <div class="col-md-12">
            
          <table class="table table-bordered table-wrapper" id="cTable">
            <thead class="thead-dark">
              <tr>
                <th style="text-align: center">Date</th>
                <th style="text-align: center">11:00 PM  - 7:00 AM SHIFT </th>
                <th style="text-align: center">Ward</th>
  
             
            
              </tr>
            </thead>
            <tbody>

              @forelse ($schedules_pms as $schedule)
                <tr>
                  <td style="text-align: center" id="dates">{{ date('M d, Y',strtotime($schedule->date)) }}</td>
                  <td style="text-align: center">{{ $schedule->employee->full_name}}</td>
                  <td style="text-align: center">{{ $schedule->ward->ward_name }}</td>
              
                </tr>
                @empty
                <tr>
                  <td colspan="4" style="text-align: center;"> No Data Found</td>
                </tr>
              @endforelse
             
            </tbody>
          </table>
          
          </div>

          <div class="col-md-12" style="margin-bottom: 10px">
  <hr>

</div>


          <div class="col-md-12">
            
          <table class="table table-bordered table-wrapper" id="dTable">
            <thead class="thead-dark">
              <tr>
                <th colspan="3" style="text-align: center;">OFF DUTIES</th>
              </tr>
              <tr>
                <th style="text-align: center">Date</th>
                <th style="text-align: center">Employee Name</th>
                <th style="text-align: center">Description</th>
              </tr>
            </thead>
            <tbody>

              @forelse ($schedules_off as $schedule)
                <tr>
                  <td style="text-align: center" id="dates">{{ date('M d, Y',strtotime($schedule->date)) }}</td>
                  <td style="text-align: center">{{ $schedule->employee->full_name}}</td>
                  <td style="text-align: center">OFF</td>
              
                </tr>
                @empty
                <tr>
                  <td colspan="4" style="text-align: center;"> No Data Found</td>
                </tr>
              @endforelse
             
            </tbody>
          </table>
          
          </div>
      
 
        </div>
        <div class="panel-footer">
          <div class="pagination-wrapper text-center">
            {{-- {!!$businesses->render()!!} --}}
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){

    SummerizeTable($('#bTable'));
    SummerizeTable($('#aTable'));
    SummerizeTable($('#cTable'));
    SummerizeTable($('#dTable'));


     $(".btn-search").on("click",function(e){
      $("#search_filter").submit();
    });

    $('.datepicker').datetimepicker({autoclose: true})
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
          $('.btn-loading').button('loading');
          $('#target').submit();
     });

  });
    function SummerizeTable(table) {
        $(table).each(function () {
          $(table).find('td').each(function () {
            var $this = $(this);
            var col = $this.index();
            var html = $this.html();
            var row = $(this).parent()[0].rowIndex;
            var span = 1;
            var cell_above = $($this.parent().prev().children()[col]);

            while (cell_above.html() === html) { 
              span += 1; 
              cell_above_old = cell_above; 
              cell_above = $(cell_above.parent().prev().children()[col]); 
            }

            if (span > 1) {
              $(cell_above_old).attr('rowspan', span);
              $this.hide();
            }

          });
        });
      }



</script>
@stop


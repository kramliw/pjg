@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-table  panel-border-color panel-border-color-primary">
        <div class="panel-heading">Record Data
          <div class="tools dropdown">
            <a href="#" type="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
            <ul role="menu" class="dropdown-menu pull-right">
              <li><a href="{{ route('system.form_builder.create') }}">Add new account</a></li>
              {{-- <li><a href="#">Import Calendar</a></li> --}}
              {{-- <li class="divider"></li> --}}
              {{-- <li><a href="#">Export calendar</a></li> --}}
            </ul>
          </div>
        </div>
        <div class="panel-body">
          <table class="table table-hover table-wrapper">
            <thead>
              <tr>
                <th>Name</th>
                <th style="width: 30%;">Description</th>
                <th style="width: 18%;">Date Added</th>
                <th style="width: 18%;">Last Modified</th>
                <th style="width: 100px;" class="actions">Action</th>
              </tr>
            </thead>
            <tbody>
              @forelse($forms as $index => $form)
              <tr>
                <td>
                  <strong><a href="{{ route('system.form_builder.edit',[$form->id]) }}">{{ $form->name }}</a></strong> <br>
                  <span class="text-muted">{{ $form->code }}</span>
                </td>
                <td>
                  {!! $form->description ? str_limit($form->description, 50) : "<i class='text-muted'>(No description provided)<i>" !!}
                </td>
                <td>{{$form->created_at->format("Y-m-d H:i a")}}</td>
                <td>{{$form->updated_at->format("Y-m-d H:i a")}}</td>
                <td class="actions">
                  <a href="{{ route('system.form_builder.edit',[$form->id]) }}" class="icon btn btn-sm" title="Edit Record"><i class="mdi mdi-edit text-primary"></i></a>
                  @if(!in_array($form->id, [1, 2]))
                  <a href="#" class="icon btn btn-sm action-delete" data-url="{{ route('system.form_builder.destroy',[$form->id]) }}" data-toggle="modal" data-target="#confirm-delete" title="Remove Record"><i class="mdi mdi-delete text-danger"></i></a>
                  @endif
                </td>
              </tr>
              @empty
              <td colspan="5" class="text-center"><i>No record found yet.</i> <a href="{{ route('system.form_builder.create') }}"><strong>Click here</strong></a> to create one.</td>
              @endforelse
            </tbody>
          </table>
          
        </div>
        <div class="panel-footer">
          <div class="pagination-wrapper text-center">
            {!!$forms->render()!!}
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script type="text/javascript">
  $(function(){
    
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
          $('.btn-loading').button('loading');
          $('#target').submit();
     });

  });
</script>
@stop


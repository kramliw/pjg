@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            @include('system._components.notifications')
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Update Form Details<span class="panel-subtitle">Modify form information.</span></div>
                <div class="panel-body">
                    <form method="POST" action="" id="form">
                        {!!csrf_field()!!}
                        <input type="hidden" id="properties" name="properties" value="">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
                                    <label>Form Name</label>
                                    <input type="text" placeholder="Form name" class="form-control" name="name" value="{{old('name', $form->name)}}">
                                    @if($errors->first('name'))
                                    <span class="help-block">{{$errors->first('name')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group {{$errors->first('code') ? 'has-error' : NULL}}">
                                    <label>Form Code</label>
                                    <input type="text" placeholder="Form code" class="form-control" name="code" value="{{old('code', $form->code)}}">
                                    @if($errors->first('code'))
                                    <span class="help-block">{{$errors->first('code')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group {{$errors->first('description') ? 'has-error' : NULL}}">
                          <label>Form Description</label>
                          <textarea class="form-control" rows="4" name="description" id="description" placeholder="Write something here ...">{{old('description', $form->description)}}</textarea>
                          @if($errors->first('description'))
                          <span class="help-block">{{$errors->first('description')}}</span>
                          @endif
                        </div>
                    </form>
                    <div class="form-group">
                        <label>Form Elements</label>
                        <div id="form-builder" class="build-wrap"></div>
                    </div>
                    <div class="row xs-pt-15">
                        <div class="col-xs-6">
                            <button id="submit" class="btn btn-space btn-primary">Update Form</button>
                            <a href="{{route('system.form_builder.index')}}" class="btn btn-space btn-default">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('page-styles')
<style type="text/css">
    .form-wrap.form-builder .frmb-control li:first-child {
        border-radius: 0px !important;
    }
    .form-wrap.form-builder .frmb-control li:last-child {
        border-radius: 0px !important;
    }
    .form-wrap.form-builder .frmb .field-label, .form-wrap.form-builder .frmb .legend {
        font-size: 13px !important;
    }
    .form-wrap.form-builder .form-control {
        border-radius: 0 !important;
    }
    .form-wrap.form-builder .stage-wrap .frmb {
        border: 3px dashed #ccc;
        padding: 10px;
    }
</style>
@stop
@section('page-scripts')
<script src="{{ asset('assets/lib/jquery-formbuilder/js/vendor.js') }}"></script>
<script src="{{ asset('assets/lib/jquery-formbuilder/js/form-builder.min.js') }}"></script>
<script src="{{ asset('assets/lib/jquery-formbuilder/js/form-render.min.js') }}"></script>
<script type="text/javascript">
    $(function() {

       var formBuilder = $('#form-builder').formBuilder({
            controlPosition: "left",
            stickyControls: { 
                enable: false 
            },
            disabledAttrs: [
                "access",
                "description"
            ],
            disabledActionButtons: [
                "data",
                "clear",
                "save",
            ],
            disableFields: [
                "autocomplete",
                "button",
                "hidden",
                "paragraph",
                "header",
                "file",
                "number",
            ],
            controlOrder: [
                "text",
                "select",
                "textarea",
                "checkbox-group",
                "radio-group",
            ],
            defaultFields: {!! json_encode($form->elements()->pluck('properties')->toArray()) !!}
        });

        $("#form").on('submit', function(e) {
            $("#properties").val(formBuilder.actions.getData('json', true));
        });

        $("#submit").on('click', function() {
            $("#form").submit();
        });

    });
</script>
@stop


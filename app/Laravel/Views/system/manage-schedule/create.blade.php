@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default">
        <div class="panel-heading panel-heading-divider">Create Record Form</div>
        <div class="panel-body">
          <form method="POST" action="">
            {!!csrf_field()!!}
            
            <div class="form-group col-lg-4 {{$errors->first('first_name') ? 'has-error' : NULL}}" hidden="" >
              <label>Id</label>
              <input type="text" placeholder="" readonly="" class="form-control" name="employee_id" value="{{old('employee_id',$employee->id)}}">
              @if($errors->first('first_name'))
              <span class="help-block">{{$errors->first('first_name')}}</span>
              @endif
            </div>
            
             <div class="form-group col-lg-4 {{$errors->first('employee_ward_id') ? 'has-error' : NULL}}">
              <label>Ward</label>
              {{-- <input type="text" placeholder="" class="form-control" name="ward" value="{{old('ward')}}"> --}}

              <select name="employee_ward_id" id=""  class="form-control">
                <option value="{{ $employee->ward_id}}">{{ strtoupper($employee->ward->ward_name) }}</option>
                @forelse($wards as $ward)
                <option value="{{ $ward->id }}">{{ strtoupper($ward->ward_name) }}</option>
                @empty
                <option value="">No data found</option>
                @endforelse
              </select>
              @if($errors->first('employee_ward_id'))
              <span class="help-block">{{$errors->first('employee_ward_id')}}</span>
              @endif
            </div>

            <div class="form-group col-lg-4 {{$errors->first('date') ? 'has-error' : NULL}}">
              <label>Date</label>
              <input type="date" name="date" id="" value="{{ old("date")}}" class="form-control">
              @if($errors->first('date'))
              <span class="help-block">{{$errors->first('date')}}</span>
              @endif
            </div>  

            <div class="form-group col-lg-4 {{$errors->first('employee_shift_id') ? 'has-error' : NULL}}">
              <label>Shift</label>
              <select name="employee_shift_id" id="" class="form-control">
                <option value="{{ old('employee_shift_id') }}">--Choose shift here--</option>
                @foreach ($schedules as $schedule)
                <option value="{{ $schedule->id }}">{{ $schedule->shift}}</option>
                @endforeach
                <option value="0">Off</option>
              </select>
              @if($errors->first('employee_shift_id'))
              <span class="help-block">{{$errors->first('employee_shift_id')}}</span>
              @endif
            </div>

      
              <div class="col-lg-12 text-right">
                  <button type="submit" class="btn btn-lg btn-space btn-success">Create </button>
                  <a href="{{route('system.manage_schedule.index')}}" class="btn btn-lg btn-space btn-default">Cancel</a>
              </div>
           

          </form>
          <br>
          <hr>

          <div class="col-lg-12">
            <table class="table table-bordered">
              <tr>
              <th style="width:20%">Employee Name</th>
              <td style="text-align:left" colspan="3" >{{ strtoupper($employee->full_name) }}</td>
            </tr> 
            <tr>
              <th>Ward</th>
              <th>Date</th>
              <th>Shift</th>
              <th class="actions"></th>
            </tr>

            
              @foreach ($emp_scheds as $emp_sched)
                <tr>
                  <td>{{ strtoupper($emp_sched->ward->ward_name)  }}</td>
                  <td>{{ strtoupper( date("M d, Y", strtotime($emp_sched->date)))  }}</td>
                  <td>
                    @if(count($emp_sched->schedule) > 0 )
                    @foreach($emp_sched->schedule as $data)
                         {{ $data->shift }}
                    @endforeach
                    @else
                    OFF
                    @endif
                   
                   
                   </td>
                   <td class="actions" style="width: 10%">
                     <div class="btn-group btn-hspace">
                      <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings"></i> Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                      <ul role="menu" class="dropdown-menu">
                        {{-- <li><a href="{{route('system.employee.edit',[$employee->id])}}">Edit Record</a></li> --}}
                        {{-- <li class="divider"></li> --}}
                        <li><a class="action-delete" href="#" data-url="{{route('system.manage_schedule.destroy',[$emp_sched->id])}}" data-toggle="modal" data-target="#confirm-delete" title="Remove Record">Remove Record</a></li>
                      </ul>
                    </div>
                   </td>
                </tr>
              @endforeach
          
            </table>
           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-modals')
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){

     $(".btn-search").on("click",function(e){
      $("#search_filter").submit();
    });
    $('.datepicker').datetimepicker({autoclose: true})
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
          $('.btn-loading').button('loading');
          $('#target').submit();
     });

  });
</script>
@stop


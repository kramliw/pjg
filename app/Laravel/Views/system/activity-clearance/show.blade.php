@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    @include('system._components.notifications')
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading panel-heading-divider">Resident Information <span class="pull-right"><a href="{{route('system.activity_clearance.print',[$activity->id])}}" class="btn btn-space btn-success">Print Record</a></span>
        <span class="pull-right"><a href="{{route('system.activity_clearance.edit',[$activity->id])}}" class="btn btn-space btn-success">Edit Record</a></span></div>
        <div class="panel-body">
            
            <div class="row">
              <div class="col-md-6">
                <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
                  <label>Name of Applicant</label>
                  <input type="text" class="form-control" disabled="disabled" name="name" value="{{old('name',$activity->name)}}">
                  @if($errors->first('name'))
                  <span class="help-block">{{$errors->first('name')}}</span>
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group {{$errors->first('business_address') ? 'has-error' : NULL}}">
                  <label>Business Address</label>
                  <input type="text" placeholder="eg. Juan" class="form-control" disabled="disabled" name="business_address" value="{{old('business_address',$activity->business_address)}}">
                  @if($errors->first('business_address'))
                  <span class="help-block">{{$errors->first('business_address')}}</span>
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group {{$errors->first('authorized_representative') ? 'has-error' : NULL}}">
                  <label>Authorized Representative</label>
                  <input type="text" class="form-control" disabled="disabled" name="authorized_representative" value="{{old('authorized_representative',$activity->authorized_representative)}}">
                  @if($errors->first('authorized_representative'))
                  <span class="help-block">{{$errors->first('authorized_representative')}}</span>
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group {{$errors->first('fax_number') ? 'has-error' : NULL}}">
                  <label>TEL./ Fax Number</label>
                  <input type="text" disabled="disabled"  class="form-control" name="fax_number" value="{{old('fax_number',$activity->fax_number)}}">
                  @if($errors->first('fax_number'))
                  <span class="help-block">{{$errors->first('fax_number')}}</span>
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group {{$errors->first('activity_type') ? 'has-error' : NULL}}">
                  <label>Type of Activity</label>
                  <input type="text" disabled="disabled"  class="form-control" name="activity_type" value="{{old('activity_type',$activity->activity_type)}}">
                  @if($errors->first('activity_type'))
                  <span class="help-block">{{$errors->first('activity_type')}}</span>
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group {{$errors->first('activity_details') ? 'has-error' : NULL}}">
                  <label>Details of Activity</label>
                  <input type="text" disabled="disabled"  class="form-control" name="activity_details" value="{{old('activity_details',$activity->activity_details)}}">
                  @if($errors->first('activity_details'))
                  <span class="help-block">{{$errors->first('activity_details')}}</span>
                  @endif
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group {{$errors->first('activity_location') ? 'has-error' : NULL}}">
                  <label>Location of Activity</label>
                  <input type="text" disabled="disabled"  class="form-control" name="activity_location" value="{{old('activity_location',$activity->activity_location)}}">
                  @if($errors->first('activity_location'))
                  <span class="help-block">{{$errors->first('activity_location')}}</span>
                  @endif
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group {{$errors->first('activity_date') ? 'has-error' : NULL}}">
                  <label>Activity Date</label>
                  <input type="text" disabled="disabled"  class="form-control" name="activity_date" value="{{old('activity_date',$activity->activity_date)}}">
                  @if($errors->first('activity_date'))
                  <span class="help-block">{{$errors->first('activity_date')}}</span>
                  @endif
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group {{$errors->first('activity_date') ? 'has-error' : NULL}}">
                  <label>No. of Days</label>
                  <input type="text" disabled="disabled"  class="form-control" name="no_of_days" value="{{old('no_of_days',$activity->no_of_days)}}">
                  @if($errors->first('no_of_days'))
                  <span class="help-block">{{$errors->first('no_of_days')}}</span>
                  @endif
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group {{$errors->first('activity_time') ? 'has-error' : NULL}}">
                  <label>Activity Time</label>
                  <input type="text" disabled="disabled"  class="form-control" name="activity_time" value="{{old('activity_time',$activity->activity_time)}}">
                  @if($errors->first('activity_time'))
                  <span class="help-block">{{$errors->first('activity_time')}}</span>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
    <!-- <div class="col-md-7">
      <div class="panel panel-default">
        <div class="panel-heading panel-heading-divider">Activity Clearance</div>
        <div class="panel-body">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th title="Certificate Number">Cert. No.</th>
                <th>Reason</th>
                <th>Date Issued</th>
                <th>Date Expiry</th>
                <th class="text-right">Amount</th>
                <th class="text-center">Status</th>
              </tr>
            </thead>
            <tbody>
              @forelse($activity->where('transaction_type','activity_clearance') as $index => $record)
              <tr>
                <td><a target="_blank" href="{{route('system.resident.certificate.print',[$record->id])}}"><b>{{$record->certificate_number}}</b></a></td>
                <td title="{{$record->reason}}">{{Str::limit($record->reason,20)}}</td>
                <td class="cell-detail">
                  <span>{{Helper::date_only($activity->created_at)}}</span>
                  <span class="cell-detail-description">{{Helper::time_only($activity->created_at)}}</span>
                </td>
                <td class="cell-detail">
                  <span>{{Helper::date_only($activity->created_at->addDays(30))}}</span>
                  <span class="cell-detail-description">{{Helper::time_only($activity->created_at->addDays(30))}}</span>
                </td>
                <td class="text-right">₱ {{Helper::amount($record->amount)}}</td>
                <td class="text-center">
                  @if($record->payment_status == "paid")
                  <span class="label label-success">Paid</span>
                  @else
                  <span class="label label-warning">For Payment</span>
                  @endif
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="6" class="text-center">No record yet.</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div> -->
    </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('input.datepick').focus(function(){
        $(this).datetimepicker({autoclose: true,locale: 'en',
              pickTime: false,todayBtn : true,format : 'mm/dd/yyyy',minview:2});
        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });
  });
</script>
@stop


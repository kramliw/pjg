@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-8">
      @include('system._components.notifications')
      <div class="panel panel-default">
        <div class="panel-heading panel-heading-divider">Create Record Form</div>
        <div class="panel-body">
          <form method="POST" action="">
            {!!csrf_field()!!}
            

            <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
              <label>Name of Applicant/Company</label>
              <input type="text" placeholder="" class="form-control" name="name" value="{{old('name')}}">
              @if($errors->first('name'))
              <span class="help-block">{{$errors->first('name')}}</span>
              @endif
            </div>
            <div class="form-group {{$errors->first('business_address') ? 'has-error' : NULL}}">
              <label>Business Address</label>
              <input type="text" placeholder="" class="form-control" name="business_address" value="{{old('business_address')}}">
              @if($errors->first('business_address'))
              <span class="help-block">{{$errors->first('business_address')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('authorized_representative') ? 'has-error' : NULL}}">
              <label>Authorized Representative</label>
              <input type="text" placeholder="" class="form-control" name="authorized_representative" value="{{old('authorized_representative')}}">
              @if($errors->first('authorized_representative'))
              <span class="help-block">{{$errors->first('authorized_representative')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('fax_number') ? 'has-error' : NULL}}">
              <label>Fax Number</label>
              <input type="text" placeholder="" class="form-control" name="fax_number" value="{{old('authorized_representative')}}">
              @if($errors->first('fax_number'))
              <span class="help-block">{{$errors->first('fax_number')}}</span>
              @endif
            </div>
            
            <div class="form-group {{$errors->first('activity_type') ? 'has-error' : NULL}}">
              <label>Type of Activity</label>
             {!!Form::select('activity_type',$activity_type,old('activity_type'),['class' => "form-control"])!!}
              @if($errors->first('activity_type'))
              <span class="help-block">{{$errors->first('activity_type')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('activity_details') ? 'has-error' : NULL}}">
              <label>Authorized Representative</label>
              <input type="text" placeholder="" class="form-control" name="activity_details" value="{{old('activity_details')}}">
              @if($errors->first('activity_details'))
              <span class="help-block">{{$errors->first('activity_details')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('activity_location') ? 'has-error' : NULL}}">
              <label>Location of Activity</label>
              <input type="text" placeholder="" class="form-control" name="activity_location" value="{{old('activity_location')}}">
              @if($errors->first('activity_location'))
              <span class="help-block">{{$errors->first('activity_location')}}</span>
              @endif
            </div>
            
            <div class="form-group {{$errors->first('activity_date') ? 'has-error' : NULL}}">
              <label>Activity Date</label>
              <input type="text" placeholder="" class="form-control" name="activity_date" value="
              {{old('activity_date')}}">
              @if($errors->first('activity_date'))
              <span class="help-block">{{$errors->first('activity_date')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('no_of_days') ? 'has-error' : NULL}}">
              <label>No. of Days</label>
              <input type="text" placeholder="" class="form-control" name="no_of_days" value="{{old('no_of_days')}}">
              @if($errors->first('no_of_days'))
              <span class="help-block">{{$errors->first('no_of_days')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('activity_time') ? 'has-error' : NULL}}">
              <label>Activity Time</label>
              <input type="text" placeholder="" class="form-control" name="activity_time" value="{{old('activity_time')}}">
              @if($errors->first('activity_time'))
              <span class="help-block">{{$errors->first('activity_time')}}</span>
              @endif
            </div>
            
            
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Create Record</button>
                  <a href="{{route('system.activity_clearance.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')


<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
  $(function(){
    $('.datepicker').datetimepicker({autoclose: true})
  });

  $(function() {
  $('input[name="activity_date"]').daterangepicker({
    opens: 'center'
  }, function(start, end, label) {
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>
@stop


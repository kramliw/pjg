<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">{{env('APP_NAME')}}</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="text-center">
                            <img src="{{asset('pjg.jfif')}}" style="margin: 0px auto;" alt="logo" width="150"  class="logo-img">
                        </li>
                       
                       
                        @if(in_array(Str::lower(Auth::user()->type), ['super_user','admin']))
                            @if(in_array(Str::lower(Auth::user()->type), ['super_user',
                            'admin','clearance_team','cashier','lupon']))
                            <li class="divider">Quick Menu</li>
                            <li><a href="{{route('system.dashboard')}}"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a> </li>
                            @endif
                            
                            @if(in_array(Str::lower(Auth::user()->type), ['super_user',
                            'admin','clearance_team']))
                            <li class="divider">Employee Management</li>
                            <li><a href="{{route('system.manage_schedule.index')}}"><i class="icon mdi mdi-face"></i><span>Employee Schedule</span></a></li>
                            <li><a href="{{route('system.schedule_list.index')}}"><i class="icon mdi mdi-face"></i><span>Schedule Report</span></a></li>
                            <li class="divider">Master File</li>

                             <li class="parent"><a href="#"><i class="icon mdi mdi-face"></i><span>Ward</span></a>
                                <ul class="sub-menu">
                                    <li><a href="{{route('system.ward.index')}}">All Wards</a> </li>
                                    <li><a href="{{route('system.ward.create')}}">Add New</a> </li>
                                </ul>
                            </li>
                             <li class="parent"><a href="#"><i class="icon mdi mdi-face"></i><span>Employee</span></a>
                                <ul class="sub-menu">
                                    <li><a href="{{route('system.employee.index')}}">All Employee</a> </li>
                                    <li><a href="{{route('system.employee.create')}}">Add New Employee</a> </li>
                                </ul>
                            </li>

                              <li class="parent"><a href="#"><i class="icon mdi mdi-face"></i><span>Schedule</span></a>
                                <ul class="sub-menu">
                                    <li><a href="{{route('system.schedule.index')}}">All Schedule</a> </li>
                                    <li><a href="{{route('system.schedule.create')}}">Add New Schedule</a> </li>
                                </ul>
                            </li>

                    
                        
                            @endif

                            @endif


                        {{-- @endif --}}


                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
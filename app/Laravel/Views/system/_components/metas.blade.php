<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="{{env("APP_NAME","BSA Backoffice")}}">
<meta name="author" content="Markwil S. Abiera">
<link rel="shortcut icon" href="{{asset('assets/img/fav-logo.png')}}">
<title>{{$page_title}}</title>
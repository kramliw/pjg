@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default panel-table">
        <div class="row table-filters-container">
       {{--    <div class="panel-heading">Search Filters</div>
          <form action="" id="search_filter" >
          <div class="col-md-6 table-filters"><span class="table-filter-title"></span>
            <div class="filter-container">
              <label class="control-label">Put any keywords</label>
                <input type="text" class="form-control input-sm" name="keyword" value="" placeholder="Search keyword such as name">
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-8 table-filters"><span class="table-filter-title"><br></span>
                <div role="group" class="btn-group btn-group-justified" style="padding-top: 20px;">
                  <a href="#" class="btn btn-success"><i class="mdi mdi-download"></i> Export </a>
                  <a href="#" class="btn btn-success btn-search"><i class="mdi mdi-search"></i> Apply Filter</a>
                  <a href="{{route('system.ward.index')}}" class="btn btn-success btn-search"><i class="mdi mdi-search"></i> Clear Filter</a>
                </div>
              </div>
            </div>
          </div>
          <button type="submit" style="display: none;"></button>
          </form> --}}
        </div>
        <div class="panel-heading">Record Data
          <div class="tools dropdown">
            <a href="#" type="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
            <ul role="menu" class="dropdown-menu pull-right">
              <li><a href="{{route('system.schedule.create')}}">Add new Schedule</a></li>
            
            </ul>
          </div>
        </div>
        <div class="panel-body">
          <table class="table table-hover table-wrapper">
            <thead>
              <tr>
                <th>Time</th>
                <th>Description</th>
                <th class="actions"></th>
              </tr>
            </thead>
            <tbody>
              @forelse($schedules as $schedule)
              <tr>
                <td class="cell-detail"> 
                  <span>{{ $schedule->time_start }} - {{$schedule->time_end}}</span>
                </td>
                <td class="cell-detail"> 
                  <span>{{ ucfirst($schedule->description) }}</span>
                </td>
            
                <td class="actions">
                    <div class="btn-group btn-hspace">
                      <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings"></i> Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                      <ul role="menu" class="dropdown-menu">
                        <li><a href="{{route('system.schedule.edit',[$schedule->id])}}">Edit Record</a></li>
                        <li class="divider"></li>
                        <li><a class="action-delete" href="#" data-url="{{route('system.schedule.destroy',[$schedule->id])}}" data-toggle="modal" data-target="#confirm-delete" title="Remove Record">Remove Record</a></li>
                      </ul>
                    </div>
                </td>
              </tr>
              @empty
              <td colspan="4" class="text-center"><i>No record found yet.</i> <a href="{{route('system.schedule.create')}}"><strong>Click here</strong></a> to create one.</td>
              @endforelse
            </tbody>
          </table>
          
        </div>
        <div class="panel-footer">
          <div class="pagination-wrapper text-center">
            {{-- {!!$businesses->render()!!} --}}
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){

     $(".btn-search").on("click",function(e){
      $("#search_filter").submit();
    });
    $('.datepicker').datetimepicker({autoclose: true})
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
          $('.btn-loading').button('loading');
          $('#target').submit();
     });

  });
</script>
@stop


@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default">
        <div class="panel-heading panel-heading-divider">Create Record Form</div>
        <div class="panel-body">
          <form method="POST" action="">
            {!!csrf_field()!!}
            
            <div class="form-group col-lg-4 {{$errors->first('first_name') ? 'has-error' : NULL}}">
              <label>First name</label>
              <input type="text" placeholder="" class="form-control" name="first_name" value="{{old('first_name')}}">
              @if($errors->first('first_name'))
              <span class="help-block">{{$errors->first('first_name')}}</span>
              @endif
            </div>
             <div class="form-group col-lg-4 {{$errors->first('last_name') ? 'has-error' : NULL}}">
              <label>Lastname</label>
              <input type="text" placeholder="" class="form-control" name="last_name" value="{{old('last_name')}}">
              @if($errors->first('last_name'))
              <span class="help-block">{{$errors->first('last_name')}}</span>
              @endif
            </div>
             <div class="form-group col-lg-4 {{$errors->first('middle_name') ? 'has-error' : NULL}}">
              <label>Middle Name</label>
              <input type="text" placeholder="" class="form-control" name="middle_name" value="{{old('middle_name')}}">
              @if($errors->first('middle_name'))
              <span class="help-block">{{$errors->first('middle_name')}}</span>
              @endif
            </div>

             <div class="form-group col-lg-4 {{$errors->first('position') ? 'has-error' : NULL}}">
              <label>Position</label>
              <input type="text" placeholder="" class="form-control" name="position" value="{{old('position')}}">
              @if($errors->first('position'))
              <span class="help-block">{{$errors->first('position')}}</span>
              @endif
            </div>
              <div class="form-group col-lg-4 {{$errors->first('department') ? 'has-error' : NULL}}">
              <label>Department</label>
              <input type="text" placeholder="" class="form-control" name="department" value="{{old('department')}}">
              @if($errors->first('department'))
              <span class="help-block">{{$errors->first('department')}}</span>
              @endif
            </div>
             <div class="form-group col-lg-4 {{$errors->first('ward') ? 'has-error' : NULL}}">
              <label>Ward</label>
              {{-- <input type="text" placeholder="" class="form-control" name="ward" value="{{old('ward')}}"> --}}

              <select name="ward_id" id="" class="form-control">
                <option value="">--Choose Ward--</option>
                @forelse($wards as $ward)
                <option value="{{ $ward->id }}">{{ strtoupper($ward->ward_name) }}</option>
                @empty
                <option value="">No data found</option>
                @endforelse
                

              </select>
              @if($errors->first('ward_id'))
              <span class="help-block">{{$errors->first('ward_id')}}</span>
              @endif
            </div>


      
              <div class="col-lg-12 text-right">
                  <button type="submit" class="btn btn-space btn-success">Add Ward</button>
                  <a href="{{route('system.employee.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
           

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('.datepicker').datetimepicker({autoclose: true})
  });
</script>
@stop


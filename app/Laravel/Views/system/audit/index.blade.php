@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-table panel-border-color">
        <div class="row table-filters-container">
          <div class="panel-heading">Search Filters</div>
          <form action="" id="search_filter">
            <div class="col-md-4 table-filters"><span class="table-filter-title"></span>
              <div class="filter-container">
                <label class="control-label">Put any keywords</label>
                  <input type="text" class="form-control input-sm" name="keyword" value="{{$keyword}}" placeholder="Search keyword such as Permit ID, Registration Number, Business Name">
              </div>
            </div>
            <div class="col-md-4 table-filters"><span class="table-filter-title"></span>
              <div class="filter-container">
                  <div class="row">
                    <div class="col-xs-6">
                      <label class="control-label">Date Since:</label>
                      <input type="text" class="form-control input-sm datepick" name="from" data-min-view="2" data-date-format="yyyy-mm-dd"  value="{{$from}}" id="date_range_from">
                    </div>
                    <div class="col-xs-6">
                      <label class="control-label">To:</label>
                      <input type="text" class="form-control input-sm datepick" name="to" data-min-view="2" data-date-format="yyyy-mm-dd"  value="{{$to}}" id="date_range_to">
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="row">      
                <div class="col-md-6 table-filters"><span class="table-filter-title"><br></span>
                  <div role="group" class="btn-group btn-group-justified" style="padding-top: 20px;">
                    {{-- <a href="#" class="btn btn-success"><i class="mdi mdi-download"></i> Export </a> --}}
                    <a  class="btn btn-success btn-search" id="search_button"><i class="mdi mdi-search"></i> Apply Filter</a>
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" style="display: none;"></button>
          </form>
        </div>
        <div class="panel-heading">Record Data</div>
        <div class="panel-body">
            <table class="table table-hover m-b-0 table-bordered record-data">
                <thead>
                <tr>                                    
                    <th width="5%">#</th>
                    <th width="5%">IP</th>
                    <th width="40%">Description</th>
                    <th width="30%">Process</th>
                    <th width="30%">PERMIT ID</th>                                        
                    <th width="30%">Date</th>                                   
                </tr>
                </thead>
                <tbody>
                    @forelse($logs as $index => $value)
                    <tr>
                        <td><blockquote class="card-blockquote"><cite style="font-size:12px;">{{++$index}}</cite></blockquote></td>
                        <td><blockquote class="card-blockquote"><cite style="font-size:12px;">{{ $value->ip }}</cite></blockquote></td>
                        <td><blockquote class="card-blockquote"><cite style="font-size:12px;">{!!$value->remarks!!}</cite></blockquote></td>
                        <td><blockquote class="card-blockquote"><cite style="font-size:12px;">{{str_replace('_', ' ', $value->process)}} 
                          @if($value->process =="BUSINESS_DELETED")
                          - {{Str::Title(str_replace('_', ' ', Helper::get_reason($value->transaction_id)))}}
                          @endif

                        </cite></blockquote></td>
                         <td><blockquote class="card-blockquote"><cite style="font-size:12px;"> {{ $value->transaction_id?$value->transaction_id:"-" }}</cite></blockquote></td>
                        <td><blockquote class="card-blockquote"><cite style="font-size:12px;">{{Helper::date_format($value->created_at)}}</cite></blockquote></td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3"><center>No Records Found</center></td>
                    </tr>
                    @endforelse
                </tbody>                               
            </table>
        </div>
        <div class="panel-footer">
          <div class="pagination-wrapper text-center">
            {!!$logs->render()!!}
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop

@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('#date_range_from, #date_range_to').daterangepicker({
            singleDatePicker: true,
        autoApply: true,
      timePicker: false,
      locale: {
        format: 'YYYY-MM-DD'
      }
    });

    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
      $('#reason').change();
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
      $('.btn-loading').button('loading');
      $('#target').submit();
     });

    $('#search_button').on('click', function(){
      console.log("akhjdhjd")
      $('#search_filter').get(0).submit();
    });

    $('#reason').on('change', function(){
      console.log($(this).val())
      var url = $(".action-delete").data('url') + "?reason=" + $(this).val()
      console.log(url)
      $("#btn-confirm-delete").attr({"href" : url});
    });
  });
</script>
@stop


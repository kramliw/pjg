<?php 

namespace App\Laravel\Controllers\System;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\Attendance;
use App\Laravel\Models\Calendar;
use App\Laravel\Models\IPAddress;
use App\Laravel\Models\User as Employee;

/*
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\AttendanceRequest;



/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB,Agent,Request,Input;

class AttendanceController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data,$auth;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());

		$this->data['num_sec'] = 0;
		$this->data['heading'] = "My Attendance";
		$this->data['employees'] = ['' => "Choose Employee"] + Employee::where('is_active','yes')->where('is_employee','yes')->lists('name','id')->toArray();
		$this->data['types']	= ['regular' => "Regular Work",'remote' => "Remote Work",'regular_ot' => "Regular OT",'weekend_ot' => "Weekend OT",'holiday_ot' => "Holiday OT",'regular_holiday' => "Regular Holiday",'special_holiday' => "Special Holiday",'sick_leave' => "Sick Leave",'regular_leave' => "Regular Leave",'emergency_leave' => "Emergency Leave","team_building" => "Company Outing"];
		$this->auth = $this->data['auth'];
	}

	public function index(){
		$this->data['page_title'] = " :: My Attendance";
		$this->data['my_attendance'] = Attendance::find($this->auth->attendance_id);
		$this->data['attendance'] = Attendance::where('user_id',$this->auth->id)->orderBy('login',"DESC")->paginate(15);
		return view('system.attendance.index',$this->data);
	}

	public function login(){
		$agent = new Agent;
		$date_today = Helper::date_db(Carbon::now());
		$is_login = Attendance::where('user_id',$this->auth->id)->whereRaw("DATE(login) = '{$date_today}'")->first();
		
		jumper:
		if($is_login){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"You are already logged in.");
			return redirect()->route('system.attendance.index');
		}else{
			$new_attendance = new Attendance;
			$new_attendance->user_id = $this->auth->id;
			$new_attendance->task = "<i>Enter your task done.</i>";
			$new_attendance->login = Carbon::now();
			$new_attendance->ip_address = Request::ip();

			$check_calendar = Calendar::whereRaw("DATE(occasion_date) = '".Helper::date_db($new_attendance->login)."'")
									->holidays()->first();

			if($check_calendar){
				$new_attendance->type = $check_calendar->type;
			}


			$validate_ip = IPAddress::where('ip',$new_attendance->ip_address)->where('is_active','yes')->first();
			if($validate_ip){
				$new_attendance->ip_location = $validate_ip->location;
			}else{
				$new_attendance->ip_location = "n/a";
			}

			$new_attendance->time_in = $this->auth->time_in;
			$new_attendance->grace_period_min = $this->auth->grace_period_min;
			if($new_attendance->save()){

				$time = explode(":", Helper::date_format($new_attendance->login,"H:i"));
				$num_sec = $time[0]*60+$time[1];

				$timein = explode(":", Helper::date_format($new_attendance->time_in,"H:i"));
				$timein_min = $timein[0]*60+$timein[1]+$new_attendance->grace_period_min;
				
				if($num_sec > $timein_min){ $new_attendance->is_late = "yes"; $new_attendance->save(); }


				session()->flash('notification-status',"success");
				session()->flash('notification-msg',"Successfully login.");
			}else{
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Server communication error. Please try again.");
			}
		}
		return redirect()->route('system.attendance.index');
	}
	public function submit(){
		$auth = $this->data['auth'];

		$attendance = Attendance::find($auth->attendance_id);

		session()->flash('notification-status',"failed");
		session()->flash('notification-msg',"Attendance record not found.");
		if($attendance){
			if(Input::has('_logout') AND Str::lower(Input::get('_logout','no')) == "no"){
				$attendance->task = Input::get("task");
				if($attendance->save()){
					session()->flash('notification-status',"success");
					session()->flash('notification-msg',"Task content succesfully updated.");
				}else{
					session()->flash('notification-status',"failed");
					session()->flash('notification-msg',"Server communication error. Please try again.");
				}
			}else{
				session()->flash('notification-msg',"Error in computation of data.");
				$attendance->task = Input::get("task");
				$attendance->logout = Carbon::now();
				$to_time = strtotime($attendance->logout);
				$from_time = strtotime($attendance->login);
				$attendance->num_min = ceil(abs($to_time - $from_time) / 60);
				$attendance->is_logout = "yes";
				// $attendance->time_in = $auth->time_in;
				// $attendance->grace_period_min = $auth->grace_period_min;
				if($attendance->save()){
					session()->flash('notification-status',"success");
					session()->flash('notification-msg',"Successfully logout.");
				}else{
					session()->flash('notification-status',"failed");
					session()->flash('notification-msg',"Server communication error. Please try again.");
				}
			}
		}
		
		return redirect()->route('system.attendance.index');
	}
}
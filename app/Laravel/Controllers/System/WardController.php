<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Ward;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\WardRequest;
use App\Http\Requests\PageRequest;
/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class WardController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];

	
	}

	public function index (PageRequest $request) {

		$this->data['page_title'] = "Wards - Record Data";

		$this->data['wards'] = Ward::orderby('updated_at','desc')->get();

		return view('system.ward.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = "Wards - Add new record";
		return view('system.ward.create',$this->data);
	}

	public function store (WardRequest $request) {
		try {

			$ward = new Ward;
			$ward->fill($request->only('ward_name'));
			if($ward->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.ward.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = "Wards - Edit record";
		$ward = Ward::find($id);

		if (!$ward) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.ward.index');
		}

		$this->data['ward'] = $ward;
		return view('system.ward.edit',$this->data);
	}

	public function update (WardRequest $request, $id = NULL) {
		try {
			$ward = Ward::find($id);

			if (!$ward) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.ward.index');
			}

			$ward->fill($request->only('ward_name'));

			if($ward->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.ward.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$ward = Ward::find($id);

			if (!$ward) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.ward.index');
			}


			if($ward->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.ward.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}
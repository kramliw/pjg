<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Location;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\LocationRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class QuotationController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['types'] = ['' => "Choose Location Type", 'metro_manila' => "Metro Manila","provincial" => "Provincial"];
		$this->data['heading'] = "Location";
	}

	public function index () {
		$this->data['page_title'] = " :: Qoutation - Record Data";
		// $this->data['locations'] = Location::orderBy('created_at',"DESC")->paginate(15);
		return view('system.qoutation.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: Qoutation - Add new record";
		return view('system.qoutation.create',$this->data);
	}

	public function store (LocationRequest $request) {
		try {
			$new_location = new Location;
			$new_location->fill($request->only('name','type'));
			if($new_location->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.qoutation.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: Qoutation - Edit record";
		$location = Location::find($id);

		if (!$location) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.qoutation.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.qoutation.index');	
		}

		$this->data['location'] = $location;
		return view('system.qoutation.edit',$this->data);
	}

	public function update (LocationRequest $request, $id = NULL) {
		try {
			$location = Location::find($id);

			if (!$location) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.qoutation.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.qoutation.index');	
			}

			$location->fill($request->only('name','type'));

			if($location->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.qoutation.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$location = Location::find($id);

			if (!$location) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.qoutation.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.qoutation.index');	
			}

			if($location->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.qoutation.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}
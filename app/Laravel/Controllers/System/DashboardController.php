<?php 

namespace App\Laravel\Controllers\System;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Resident;
use App\Laravel\Models\Business;
use App\Laravel\Models\BusinessClearance;
use App\Laravel\Models\ResidentCertificate;

/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB;

class DashboardController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		// parent::__construct();
		// array_merge($this->data, parent::get_data());
	}

	public function index(){
		$this->data['page_title'] = "Dashboard";

	
		return view('system.index',$this->data);
	}
}
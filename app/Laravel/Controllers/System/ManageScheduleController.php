<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Ward;
use App\Laravel\Models\Employee;
use App\Laravel\Models\Schedule;
use App\Laravel\Models\EmployeeSchedule;

/**
*
* Requests used for validating inputs
*/
// use App\Laravel\Requests\System\EmployeeRequest;
use App\Laravel\Requests\System\EmpRequest;

use App\Http\Requests\PageRequest;


/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,Validate;

class ManageScheduleController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];

	
	}

	public function index (PageRequest $request) {
		$this->data['page_title'] = "Employee - Record Data";
		$this->data['wards'] = Ward::get();
		 $data = Employee::keyword($request->ward)->get();
		  $this->data['employees'] = $data;

		
		return view('system.manage-schedule.index',$this->data);
	}

	public function create ($id=NULL) {
		$this->data['page_title'] = "Create Schedule - Create record";
		$this->data['wards'] = Ward::get();
		 $this->data['schedules'] = Schedule::get();
		 $this->data['emp_scheds'] = EmployeeSchedule::where("employee_id",$id)->orderBy("updated_at","desc")->get();
		$employee = Employee::find($id);
		$this->data['employee'] = $employee;
		return view('system.manage-schedule.create',$this->data);
	}

	public function store(EmpRequest $request , $id=NULL) {
		try {
			// return $request->all();

		 $check = EmployeeSchedule::where("employee_id",$request->id)
								->where("date",$request->date)->get();

			if(count($check) > 0){
				session()->flash('notification-status','failed');
				session()->flash('notification-msg',"This date is already set, Please try other date!");
				return redirect()->back();
			}

			
			$employee = new EmployeeSchedule;
			
			$employee->employee_id = $request->employee_id;
			$employee->employee_ward_id = $request->employee_ward_id;
			$employee->employee_shift_id = $request->employee_shift_id;
			$employee->date = $request->date;
	
			if($employee->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->back();
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = "Employee - Edit record";
		$this->data['wards'] = Ward::get();
		$employee = Employee::find($id);

		if (!$employee) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.employee.index');
		}

		$this->data['employee'] = $employee;
		return view('system.manage_schedule.edit',$this->data);
	}

	public function update (EmployeeRequest $request, $id = NULL) {
		try {

			// return $request->all();
			$employee = Employee::find($id);

			if (!$employee) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.employee.index');
			}

			$employee->fill($request->except('_token'));

			if($employee->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.employee.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$employee = EmployeeSchedule::find($id);

			if (!$employee) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->back();
			}

			if($employee->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->back();
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}
<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Rider;
use App\Laravel\Models\Waybill;

use App\Laravel\Models\Location;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\ExportReportRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,Excel;

class ReportingController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['report_types'] = ['' => "Choose Report",'receiving' => "Receiving",'sorting' => "Sorting",'delivery' => "Delivery"];
		$this->data['heading'] = "Reporting";
	}

	public function export () {
		$this->data['page_title'] = " :: Reporting - Export Data";
		// $this->data['riders'] = Rider::orderBy('created_at',"DESC")->paginate(15);
		return view('system.reporting.export',$this->data);
	}

	public function export_report(ExportReportRequest $request){
		$report_type = Str::lower($request->get('report_type'));
		$date = Helper::date_db($request->get('date'));

		switch($report_type){
			case 'receiving':
				Excel::create("Exported Data - Received Date {$date}", function($excel) use ($date) {
				    $excel->setTitle("Exported Data From {$date}")
				        ->setCreator("Richard Kennedy Domingo")
				        ->setCompany('Sonic Express');
				   $riders = Rider::where('location_id','<>','-1')->get();
				   

				   $excel->sheet("SUMMARY",function($sheet) use ($date) {

			   			$locations = Location::where('id','<>','-1')->orderBy('name',"ASC")->get();
			   			$sheet->setFontFamily('Calibri Light');
			   			$sheet->setFontSize(14);
			   			$sheet->mergeCells('A1:L1');
			   			$sheet->row(1,['Summary']);

			   			$sheet->mergeCells('C2:E2');
			   			$sheet->mergeCells('F2:H2');
			   			$sheet->row(2,['','',
			   				Waybill::where('receiver_rider_id','<>','0')->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->count(),'','',
			   				Waybill::where('receiver_rider_id','<>','0')->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->count(),'','',
			   				Waybill::where('receiver_rider_id','<>','0')->whereRaw("DATE(received_date) = '{$date}'")->whereIn('location_group',['metromanila','provincial'])->count(),
			   				'0',
			   				Waybill::where('receiver_rider_id','<>','0')->whereRaw("DATE(received_date) = '{$date}'")
   									->whereIn('location_group',['metromanila','provincial'])
	   								->where('with_remittance','no')->count(),
			   				Waybill::where('receiver_rider_id','<>','0')->whereRaw("DATE(received_date) = '{$date}'")
   									->whereIn('location_group',['metromanila','provincial'])
	   								->where('with_item','no')->count()]);
			   			$sheet->row(3,['Area','Rider\'s Name','MM (Small)','MM (Large)','MM (Customize)','Prov (Small)','Prov (Large)','Prov (Customize)','Total Pickup','Pending','No Remittance','Missing Parcel']);

			   			$sheet->row(4,['','',
	   						Waybill::whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->where('size','sm')->count(),
	   						Waybill::whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->where('size','lg')->count(),
	   						Waybill::whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->where('size','custom')->count(),
	   						Waybill::whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->where('size','sm')->count(),
	   						Waybill::whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->where('size','lg')->count(),
	   						Waybill::whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->where('size','custom')->count(),
			   			]);

			   			$i = 5;
			   			foreach($locations as $index => $location){
			   				$loc_name = $location->name;

			   				$riders = Rider::where('location_id',$location->id)->orderBy('name','ASC')->get();
			   			
			   				foreach($riders as $index => $rider){
			   					$sheet->row($i++,[$loc_name,$rider->name,
			   						Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->where('size','sm')->count(),
			   						Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->where('size','lg')->count(),
			   						Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->where('size','custom')->count(),
			   						Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->where('size','sm')->count(),
			   						Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->where('size','lg')->count(),
			   						Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->where('size','custom')->count(),
			   						Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->count(),
			   						0,
			   						Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")
				   								->where('with_remittance','no')->count(),
			   						Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")
				   								->where('with_item','no')->count()
			   					]);
			   				}
			   			}

			   		});


				   foreach($riders as $index => $rider){
			   			$title = Str::upper(Str::slug($rider->name));

				   		$excel->sheet("{$title}",function($sheet) use ($date,$rider) {
				   			$i = 1;
				   			$sheet->setFontFamily('Calibri Light');
				   			// $sheet->setAutoSize(true);
				   			$sheet->setFontSize(14);

				   			$waybills = Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'");
				   			$sheet->setWidth(array(
				   			    'A'     =>  10, 'B'     =>  10, 'C'		=> 10, 'D'	=> 5, 'E' => 10, 'F' => 10, 'G' => 10
				   			));
				   			$sheet->mergeCells('A1:C1');
				   			$sheet->mergeCells('E1:G1');
				   			$sheet->row(1,[Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->count(),'','','',Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->count()]);

				   			$sheet->mergeCells('A2:C2');
				   			$sheet->mergeCells('E2:G2');
				   			$sheet->row(2,['Metro Manila','','','','Provincial','','','','Not Remitted','Missing Parcel','Problematic']);

				   			$sheet->row(3,[Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->where('size','sm')->count(),Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->where('size','lg')->count(),Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->where('size','custom')->count(),'',Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->where('size','sm')->count(),Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->where('size','lg')->count(),Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->where('size','custom')->count(),
				   								'',
				   								Waybill::where('receiver_rider_id',$rider->id)
					   								->whereRaw("DATE(received_date) = '{$date}'")->where('with_remittance','no')->count(),
					   							Waybill::where('receiver_rider_id',$rider->id)
					   								->whereRaw("DATE(received_date) = '{$date}'")->where('with_item','no')->count()
				   								]);

				   			$i = 5;
				   			foreach(Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->where('size','sm')->get() as $index => $waybill){
				   				$sheet->setCellValue("A".$i++, $waybill->waybill_id);
				   			}

				   			$i = 5;
				   			foreach(Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->where('size','lg')->get() as $index => $waybill){
					   			
				   				$sheet->setCellValue("B".$i++, $waybill->waybill_id);
				   			}

				   			$i = 5;
				   			foreach(Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','metromanila')->where('size','custom')->get() as $index => $waybill){
					   			
				   				$sheet->setCellValue("C".$i++, $waybill->waybill_id);
				   			}

				   			$i = 5;
				   			foreach(Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->where('size','sm')->get() as $index => $waybill){
					   			
				   				$sheet->setCellValue("E".$i++, $waybill->waybill_id);
				   			}

				   			$i = 5;
				   			foreach(Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->where('size','lg')->get() as $index => $waybill){
					   			
				   				$sheet->setCellValue("F".$i++, $waybill->waybill_id);
				   			}

				   			$i = 5;
				   			foreach(Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('location_group','provincial')->where('size','custom')->get() as $index => $waybill){
				   				$sheet->setCellValue("G".$i++, $waybill->waybill_id);
				   			}

				   			$i = 5;
				   			foreach(Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('with_remittance','no')->get() as $index => $waybill){
				   				$sheet->setCellValue("I".$i++, $waybill->waybill_id);
				   			}

				   			$i = 5;
				   			foreach(Waybill::where('receiver_rider_id',$rider->id)
				   								->whereRaw("DATE(received_date) = '{$date}'")->where('with_item','no')->get() as $index => $waybill){
				   				$sheet->setCellValue("J".$i++, $waybill->waybill_id);
				   			}

				   		});
				   }
			        })->export('xls');
			break;
			case 'delivery':
					Excel::create("Exported Data - For Delivery Date {$date}", function($excel) use ($date) {
					    $excel->setTitle("Exported Data - For Delivery Date {$date}")
					        ->setCreator("Richard Kennedy Domingo")
					        ->setCompany('Sonic Express');
					   $riders = Rider::all();

					   foreach($riders as $index => $rider){
				   			$title = Str::upper(Str::slug($rider->name));
					   		$excel->sheet("{$title}",function($sheet) use ($date,$rider) {
					   			$i = 1;

					   			$sheet->setFontFamily('Calibri Light');
					   			$sheet->setAutoSize(true);
					   			$sheet->setFontSize(14);


					   			$waybills = Waybill::where('delivery_rider_id',$rider->id)
					   								->whereRaw("DATE(delivery_date) = '{$date}'");

   								$cod_waybills = Waybill::where('delivery_rider_id',$rider->id)
					   								->whereRaw("DATE(delivery_date) = '{$date}'")
					   								->where('is_cod','yes');
					   			$sheet->row($i++, [$waybills->count(),$cod_waybills->count()]);
					   			$sheet->row($i++, ["Out for Delivery","Cash on Delivery","Value"]);


					   			foreach($waybills->get() as $index => $waybill){
					   				// $sheet->row($i++,[$waybill->waybill_id]);
					   				$sheet->setCellValue("A".$i++, $waybill->waybill_id);
					   			}

					   			$i = 3;

					   			foreach($cod_waybills->get() as $index => $waybill){
					   				$j = $i++;
					   				$sheet->setCellValue("B".$j, $waybill->waybill_id);
					   				$sheet->setCellValue("C".$j, $waybill->cod_amount);

					   			}
					   		});
					   }
			        })->export('xls');
			break;

			case 'sorting':
				Excel::create("Exported Data - Sorting Date {$date}", function($excel) use ($date) {
				    $excel->setTitle("Exported Data - Sorting Date {$date}")
				        ->setCreator("Richard Kennedy Domingo")
				        ->setCompany('Sonic Express');
				   $locations = Location::orderBy('id','DESC')->get();

				   foreach($locations as $index => $location){
				   		$title = Str::upper(Str::slug($location->name));
				   		$excel->sheet("{$title}",function($sheet) use ($date,$location) {
				   			$i = 1;

				   			$sheet->setFontFamily('Calibri Light');
				   			$sheet->setAutoSize(true);
				   			$sheet->setFontSize(14);

				   			$waybills = Waybill::where('location_id',$location->id)
				   								->whereRaw("DATE(sorting_date) = '{$date}'")->get();
				   			$waybill_final =  Waybill::where('location_id',$location->id)
				   								->whereRaw("DATE(sorting_date) = '{$date}'")->whereRaw("final_sort_date IS NOT NULL")->get();
				   			$waybill_discrepancy  =  Waybill::where('location_id',$location->id)
				   								->whereRaw("DATE(sorting_date) = '{$date}'")->whereRaw("final_sort_date IS NULL")->get();					
				   			$sheet->row($i++, [$waybills->count(),$waybill_final->count(),$waybill_discrepancy->count()]);
				   			$sheet->row($i++, ["Initial Sorting","Final Sorting","Discrepancy"]);
				   			$start_counter = $i;
				   			foreach($waybills as $index => $waybill){
				   				$sheet->setCellValue("A".$start_counter++, $waybill->waybill_id);
				   			}
				   			$start_counter = $i;

				   			foreach($waybill_final as $index => $waybill){
				   				$sheet->setCellValue("B".$start_counter++, $waybill->waybill_id);
				   			}
				   			$start_counter = $i;	

				   			foreach($waybill_discrepancy as $index => $waybill){
				   				$sheet->setCellValue("C".$start_counter++, $waybill->waybill_id);
				   			}

				   		});
				   }
		        })->export('xls');
			break;
			default:
				session()->flash('notification-status','failed');
				session()->flash('notification-msg','Invalid request report. Unable to proceed request.');

				return redirect()->back();
		}
	}
}
<?php

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\ActivityType;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\ActivityTypeRequest;
use App\Http\Requests\PageRequest;
/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class ActivityTypeController extends Controller
{
    	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index (PageRequest $request) {
		$this->data['keyword'] = $request->get('keyword',false);
		$this->data['page_title'] = " :: Type of Activity - Record Data";
		$this->data['activity_type'] = ActivityType::keyword($this->data['keyword'])->orderBy('created_at',"DESC")->paginate(15);
		return view('system.activity-type.index',$this->data);
	}

		public function create () {
		$this->data['page_title'] = " :: Activity Type - Add new record";
		return view('system.activity-type.create',$this->data);
	}

	public function store (ActivityTypeRequest $request) {
		try {
			$new_street = new ActivityType;
			$new_street->fill($request->only('name'));
			if($new_street->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.activity_type.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: Activity Type - Edit record";
		$activity_type = ActivityType::find($id);

		if (!$activity_type) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.activity_type.index');
		}

		$this->data['activity_type'] = $activity_type;
		return view('system.activity-type.edit',$this->data);
	}

	public function update (ActivityTypeRequest $request, $id = NULL) {
		try {
			$activity_type = ActivityType::find($id);

			if (!$activity_type) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.activity_type.index');
			}

			$activity_type->fill($request->only('name'));

			if($activity_type->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.activity_type.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$street = ActivityType::find($id);

			if (!$street) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.activity_type.index');
			}

			if($street->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.activity_type.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}

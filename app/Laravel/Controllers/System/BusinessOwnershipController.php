<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\BusinessOwnership;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\BusinessOwnershipRequest;
use App\Http\Requests\PageRequest;
/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class BusinessOwnershipController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index (Pagerequest $request) {

		$this->data['keyword'] = $request->get('keyword',false);
		$this->data['page_title'] = " :: Business Ownership - Record Data";
		$this->data['ownerships'] = BusinessOwnership::keyword($this->data['keyword'])->orderBy('created_at',"DESC")->paginate(15);
		return view('system.business-ownership.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: Business Ownership - Add new record";
		return view('system.business-ownership.create',$this->data);
	}

	public function store (BusinessOwnershipRequest $request) {
		try {
			$new_ownership = new BusinessOwnership;
			$new_ownership->fill($request->only('name'));
			if($new_ownership->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.business_ownership.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: Business Ownership - Edit record";
		$ownership = BusinessOwnership::find($id);

		if (!$ownership) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.business_ownership.index');
		}

		$this->data['ownership'] = $ownership;
		return view('system.business-ownership.edit',$this->data);
	}

	public function update (BusinessOwnershipRequest $request, $id = NULL) {
		try {
			$ownership = BusinessOwnership::find($id);

			if (!$ownership) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.business_ownership.index');
			}

			$ownership->fill($request->only('name'));

			if($ownership->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.business_ownership.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$ownership = BusinessOwnership::find($id);

			if (!$ownership) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.business_ownership.index');
			}

			if($ownership->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.business_ownership.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}
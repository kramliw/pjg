<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Product;
use App\Laravel\Models\Category;


/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\ProductRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class ProductController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['types'] = ['' => "Choose Category"]+Category::pluck('name','id')->toArray();
		$this->data['uoms'] = ['' => "Choose UOM",'pack' => "Pack",'tube' => "Tube", 'piece' => "Piece",'roll' => "Roll",'bottle' => "Bottle",'box' => "Box",'bundle' => "Bundle",'set' => "Set",'pad' => "Pad",'ream' => "Ream",'book' => "Book",'pair' => "Pair",'can' => "Can",'cont' => "Cont",'unit' => "Unit",'cart' => "Cart",'bar' => "Bar",'meter' => "Meter"];
		$this->data['heading'] = "Product";
	}

	public function index () {
		$this->data['page_title'] = " :: Product - Record Data";
		$this->data['products'] = Product::orderBy('created_at',"DESC")->paginate(15);
		return view('system.product.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: Product - Add new record";
		return view('system.product.create',$this->data);
	}

	public function store (ProductRequest $request) {
		try {
			$new_product = new Product;
			$new_product->fill($request->only('category_id','name','price','uom'));
			if($new_product->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.product.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: Product - Edit record";
		$product = Product::find($id);

		if (!$product) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.product.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.product.index');	
		}

		$this->data['location'] = $product;
		return view('system.product.edit',$this->data);
	}

	public function update (ProductRequest $request, $id = NULL) {
		try {
			$product = Product::find($id);

			if (!$product) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.product.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.product.index');	
			}

			$product->fill($request->only('category_id','name','price','uom'));

			if($product->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.product.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$product = Product::find($id);

			if (!$product) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.product.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.product.index');	
			}

			if($product->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.product.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}
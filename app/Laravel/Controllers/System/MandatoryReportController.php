<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Resident;
use App\Laravel\Models\ResidentCertificate;
use App\Laravel\Models\Payment;
use App\Laravel\Models\BusinessClearance;
use App\Laravel\Models\ActivityClearance;
use App\Laravel\Models\ActivityType;
use App\Laravel\Models\Mandatory_collection;

/**
*
* Requests used for validating inputs
*/
use App\Http\Requests\PageRequest;
use Illuminate\Http\Request;

use App\Laravel\Requests\System\PaymentRequest;

use App\Laravel\Events\AuditTrailActivity;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,ImageUploader,PDF,Aut, AuditRequest, Auth, Event;

class MandatoryReportController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ 'paid' => "Paid","no_charge" => "No Charges"];
		$this->data['payment_method'] = [ 'cash' => "Cash","check" => "Check"];
		
		$this->data['nature_collection'] = ['' => "Choose Nature of Collecton"] + ActivityType::pluck('name', 'name')->toArray();
		$this->data['heading'] = "System Account";
	}


	


	public function print(Request $request,$name = NULL){
		$this->data['accounting'] = $request->name;
		$this->data['collection'] = Mandatory_collection::where('category','electric')
														->where('from','>=',$request->from)
														->where('to','<=',$request->to)
														->get();
		
		$pdf = PDF::loadView('pdf.meralco', $this->data )->setPaper('A4', 'landscape');
		return $pdf->stream('meralco.pdf');
	}

	public function print_mwss(Request $request){
		$this->data['accounting'] = $request->name;
		$this->data['collection'] = Mandatory_collection::where('category','Manila Water')
														->where('from','>=',$request->from)
														->where('to','<=',$request->to)
														->get();
		
		$pdf = PDF::loadView('pdf.mwss', $this->data )->setPaper('A4', 'landscape');
		return $pdf->stream('mwss.pdf');
	}

public function print_pldt(Request $request){
		$this->data['accounting'] = $request->name;
		$this->data['collection'] = Mandatory_collection::where('company_name','pldt')
														->where('from','>=',$request->from)
														->where('to','<=',$request->to)
														->get();
		
		$pdf = PDF::loadView('pdf.pldt', $this->data )->setPaper('A4', 'landscape');
		return $pdf->stream('pldt.pdf');
	}
	public function print_smart(Request $request){
		$this->data['accounting'] = $request->name;
		$this->data['collection'] = Mandatory_collection::where('company_name','smart')
														->where('from','>=',$request->from)
														->where('to','<=',$request->to)
														->get();
		
		$pdf = PDF::loadView('pdf.smart', $this->data )->setPaper('A4', 'landscape');
		return $pdf->stream('smart.pdf');
	}
	public function print_petron(Request $request){
		$this->data['accounting'] = $request->name;
		$this->data['collection'] = Mandatory_collection::where('company_name','petron')
														->where('from','>=',$request->from)
														->where('to','<=',$request->to)
														->get();
		
		$pdf = PDF::loadView('pdf.petron', $this->data )->setPaper('A4', 'landscape');
		return $pdf->stream('petron.pdf');
	}
	public function print_flying(Request $request){
		$this->data['accounting'] = $request->name;
		$this->data['collection'] = Mandatory_collection::where('category','flying')
														->where('from','>=',$request->from)
														->where('to','<=',$request->to)
														->get();
		
		$pdf = PDF::loadView('pdf.flying', $this->data )->setPaper('A4', 'landscape');
		return $pdf->stream('flying.pdf');
	}
	
	

	
}
<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Ward;
use App\Laravel\Models\Employee;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\EmployeeRequest;
use App\Http\Requests\PageRequest;
/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class EmployeeController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];

	
	}

	public function index (PageRequest $request) {

		$this->data['page_title'] = "Employee - Record Data";

		 $this->data['employees'] = Employee::orderBy('updated_at','desc')->paginate(5);
		 $this->data['wards'] = Ward::get();

		return view('system.employee.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = "Wards - Add new record";
		$this->data['wards'] = Ward::get();	
		return view('system.employee.create',$this->data);
	}

	public function store (EmployeeRequest $request) {
		try {


			$employee = new Employee;
			
			$employee->first_name = $request->first_name;
			$employee->last_name = $request->last_name;
			$employee->middle_name = $request->middle_name;
			$employee->position = $request->position;
			$employee->ward_id = $request->ward_id;
			$employee->department = $request->department;



			if($employee->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.employee.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = "Employee - Edit record";
		$this->data['wards'] = Ward::get();
		$employee = Employee::find($id);

		if (!$employee) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.employee.index');
		}

		$this->data['employee'] = $employee;
		return view('system.employee.edit',$this->data);
	}

	public function update (EmployeeRequest $request, $id = NULL) {
		try {

			// return $request->all();
			$employee = Employee::find($id);

			if (!$employee) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.employee.index');
			}

			$employee->fill($request->except('_token'));

			if($employee->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.employee.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$employee = Employee::find($id);

			if (!$employee) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.employee.index');
			}

			if($employee->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.employee.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}
<?php

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\ActivityClearance;
use App\Laravel\Models\ActivityType;

use App\Http\Requests\PageRequest;

use App\Laravel\Models\Payment;



/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\ActivityClearanceRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, ImageUploader, Image, File, URL, AuditRequest, Auth, Event ,PDF;


class ActivityClearanceController extends Controller
{
    
	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		$this->data['sorting'] = ['date_modified' => "Last Modified"];
		array_merge($this->data, parent::get_data());
		$this->data['activity_type'] = ['' => "Choose Activity Type"] + ActivityType::pluck('name', 'name')->toArray();
	}

	public function index (PageRequest $request) {
		$this->data['keyword'] = $request->get('keyword',false);
		$this->data['from'] = $request->get('from',false);
		$this->data['to'] = $request->get('to',false);
		$this->data['sort_by'] = $request->get('sort_by','last_modified');
		$this->data['page_title'] = " :: Activity - Record Data";

		$this->data['activities'] = ActivityClearance::keyword($this->data['keyword'])
									->dateRange($this->data['from'],$this->data['to'])->orderBy('created_at',"DESC")->paginate(15);
		return view('system.activity-clearance.index',$this->data);
	}
	public function create () {
		$this->data['page_title'] = " :: Activity - Add new record";
		return view('system.activity-clearance.create',$this->data);
	}

	public function store (ActivityClearanceRequest $request) {
		try {

			$new_activities = new ActivityClearance;
			$new_activities->fill($request->all());
			$new_activities->date_filed=Carbon::now();
			//dd($request->all());
			if($new_activities->save()) {

				$new_activities->certificate_number = "BSA-AC-".str_pad($new_activities->id, 6, "0", STR_PAD_LEFT);
				$new_activities->save();

				$new_payment = new Payment;
				$new_payment->reference_type = "activity_clearance";
				$new_payment->reference_id = $new_activities->id;
				$new_payment->amount = 0;
				$new_payment->save();
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.activity_clearance.show',[$new_activities->id]);
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}


		/*session()->flash('notification-status','success');
		session()->flash('notification-msg',"New certificate has been added.");
		return redirect()->route('system.resident.show',[$resident->id]);*/
	}


	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: Activity - Edit record";
		$activity = ActivityClearance::find($id);

		if (!$activity) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.activity_clearance.index');
		}

		$this->data['activity'] = $activity;
		return view('system.activity-clearance.edit',$this->data);
	}


	public function update (ActivityClearanceRequest $request, $id = NULL) {
		try {
			$activity = ActivityClearance::find($id);

			if (!$activity) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.activity_clearance.index');
			}

			$activity->fill($request->all());

			if($activity->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.activity_clearance.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function show ($id = NULL) {
		$this->data['page_title'] = " :: Activity - Activity Record";
		$activity = ActivityClearance::where('id',$id)->first();

		if (!$activity) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.activity_clearance.index');
		}

		$this->data['activity'] = $activity;
		return view('system.activity-clearance.show',$this->data);
	}

	public function destroy ($id = NULL) {
		try {
			$activity = ActivityClearance::find($id);

			if (!$activity) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.activity_clearance.index');
			}

			if($activity->delete()) {
				$activity->payment()->delete();
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.activity_clearance.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function print($id = NULL){
		$this->data['activity'] = ActivityClearance::find($id);

		if (!$this->data['activity']) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.activity_clearance.index');
		}

		$pdf = PDF::loadView('pdf.activity-clearance', $this->data)->setPaper('letter', 'portrait');
		return $pdf->stream('activity-clearance.pdf');
	}
}

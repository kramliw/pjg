<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\AuditTrail;

use App\Http\Requests\PageRequest;

 
/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,Auth;

class LogsController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['heading'] = "Rider";
	}

	public function index (PageRequest $request) {
		
		$this->data['keyword'] = $request->get('keyword',false);
		$this->data['from'] = $request->get('from',false);
		$this->data['to'] = $request->get('to',false);
		
		if (!$this->data['from']) {
			$from = AuditTrail::select('created_at')->orderBy('created_at','asc')->take(1)->pluck('created_at')->toArray();
		}

		if(isset($from) AND count($from)==1){
			$this->data['from'] = $from[0]->format("Y-m-d");
		}
		
		$this->data['page_title'] = " :: Logs - Record Data";
		$this->data['logs'] = AuditTrail::keyword($this->data['keyword'])
									->dateRange($this->data['from'],$this->data['to'])->where('user_id' , Auth::user()->id)->orderBy('created_at',"DESC")->paginate(15);
		
		return view('system.audit.index',$this->data);
	}
}
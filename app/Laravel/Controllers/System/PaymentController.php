<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Resident;
use App\Laravel\Models\ResidentCertificate;
use App\Laravel\Models\Payment;
use App\Laravel\Models\BusinessClearance;
use App\Laravel\Models\ActivityClearance;
use App\Laravel\Models\ActivityType;

/**
*
* Requests used for validating inputs
*/
use App\Http\Requests\PageRequest;

use App\Laravel\Requests\System\PaymentRequest;

use App\Laravel\Events\AuditTrailActivity;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,ImageUploader,PDF,Aut, AuditRequest, Auth, Event,Excel;

class PaymentController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ 'paid' => "Paid","no_charge" => "No Charges"];
		$this->data['payment_method'] = [ 'cash' => "Cash","check" => "Check"];
		
		$this->data['nature_collection'] = ['' => "Choose Nature of Collecton"] + ActivityType::pluck('name', 'name')->toArray();
		$this->data['heading'] = "System Account";
	}

	public function index (PageRequest $request) {
		$this->data['keyword'] = $request->get('keyword',false);
		$this->data['from'] = $request->get('from',false);
		$this->data['to'] = $request->get('to',false);
		$this->data['sort_by'] = $request->get('sort_by','last_modified');

		$this->data['page_title'] = " :: Payments - Paid Transactions";
		$this->data['payments'] = Payment::where('payment.payment_status','paid')->keyword($this->data['keyword'])
									->dateRange($this->data['from'],$this->data['to'])->orderBy('payment.created_at',"DESC")->paginate(15);

		return view('system.payment.index',$this->data);
	}

	public function pending (PageRequest $request) {
		$this->data['keyword'] = $request->get('keyword',false);
		$this->data['from'] = $request->get('from',false);
		$this->data['to'] = $request->get('to',false);
		$this->data['sort_by'] = $request->get('sort_by','last_modified');

		$this->data['page_title'] = " :: Payments - Pending Transactions";
		$this->data['payments'] = Payment::where('payment.payment_status', 'pending')->keyword($this->data['keyword'])
									->dateRange($this->data['from'],$this->data['to'])->orderBy('payment.created_at',"DESC")->paginate(15);
		// $activity = Payment::where('id', 6)->with('reference')->first();
		// dd($activity);
		return view('system.payment.index',$this->data);
	}

	public function manage($transaction_id = NULL){
		$this->data['page_title'] = " :: Payments - Manage Payment";
		$payment = Payment::where('id',$transaction_id)->first();

		if (!$payment) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.payment.pending');
		}

		if($payment->payment_status != "pending"){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to proceed request. Transaction already completed.");
			return redirect()->route('system.payment.pending');
		}

		$this->data['payment'] = $payment;
		$this->data['remarks'] = explode(",",$payment->amendment_remarks);

		return view('system.payment.manage',$this->data);

	}

	public function manage_payment(PaymentRequest $request,$transaction_id = NULL){
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		$current_year = Carbon::now()->format("Y");
		
		$payment = Payment::where('id',$transaction_id)->first();

		$payment->user_id = $this->data['auth']->id;

		if (!$payment) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.payment.pending');
		}

		if($payment->payment_status != "pending"){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to proceed request. Transaction already completed.");
			return redirect()->route('system.payment.pending');
		}

		$payment->receipt_number = $request->get('receipt_number');
		$payment->payment_method = $request->get('payment_method');
		$payment->bank_name = $request->get('bank_name');
		$payment->cheque_no = $request->get('cheque_no');
		$payment->amount = $request->get('amount');
		$payment->nature_of_collection = $request->get('nature_of_collection');
		$payment->payment_status = "paid";

		if($request->get('payment_status') == "no_charge"){
			$payment->amount = '0';
			$payment->receipt_number = "N-C";
		}

		if($payment->reference_type == "personal_certificate"){
			$certificate = ResidentCertificate::find($payment->reference_id);
			if($certificate){
				$certificate->cashier_user_id = Auth::user()->id;
				$certificate->amount = $payment->amount;
				$certificate->receipt_number = $payment->receipt_number;
				$certificate->payment_status = "paid";
				$certificate->save();
			}

			if($payment->save()) {

			$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "PAYMENT", 'remarks' => Auth::user()->name." has successfully received payment for personal certificate request.",'ip' => $ip,'transaction_id'=>"BSA-R-{$current_year}-".str_pad($transaction_id, 4, "0", STR_PAD_LEFT)]);
			Event::fire('log-activity', $log_data);

			$this->data['payment'] = Payment::find($transaction_id);
			$pdf = PDF::loadView('pdf.receipt', $this->data)->setPaper('letter', 'portrait');
			//return $pdf->stream('receipt.pdf');

			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Transaction completed.");
			session()->flash('id', $payment->id);
			return redirect()->route('system.payment.pending');
		}
		}

		if($payment->reference_type == "business_permit"){
			$clearance = BusinessClearance::find($payment->reference_id);
			if($clearance){
				$clearance->cashier_user_id = Auth::user()->id;
				$clearance->total = $payment->amount;
				$clearance->receipt_number = $payment->receipt_number;
				$clearance->payment_status = "paid";
				$clearance->save();
			}

			if($payment->save()) {
				
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "PAYMENT", 'remarks' => Auth::user()->name." has successfully received payment for business permit request.",'ip' => $ip,'transaction_id'=>"BSA-BP-".str_pad($payment->reference->id, 6, "0", STR_PAD_LEFT)]);
				Event::fire('log-activity', $log_data);

				$this->data['payment'] = Payment::find($transaction_id);
				$pdf = PDF::loadView('pdf.receipt', $this->data)->setPaper('letter', 'portrait');
				//return $pdf->stream('receipt.pdf');


				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Transaction completed.");
				session()->flash('id', $payment->id);
				return redirect()->route('system.payment.pending');
			}
		}

		if($payment->reference_type == "business_amendment" ){
			$clearance = BusinessClearance::find($payment->reference_id);
			if($clearance){
				$clearance->cashier_user_id = Auth::user()->id;
				$clearance->total = $payment->amount;
				$clearance->receipt_number = $payment->receipt_number;
				$clearance->payment_status = "paid";
				$clearance->save();
			}

			if($payment->save()) {
				
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "PAYMENT", 'remarks' => Auth::user()->name." has successfully received payment for business amendment request.",'ip' => $ip,'transaction_id'=>"BSA-BP-".str_pad($payment->reference->id, 6, "0", STR_PAD_LEFT)]);
				Event::fire('log-activity', $log_data);

				$this->data['payment'] = Payment::find($transaction_id);
				$pdf = PDF::loadView('pdf.receipt', $this->data)->setPaper('letter', 'portrait');
				


				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Transaction completed.");
				session()->flash('id', $payment->id);
				 return redirect()->route('system.payment.pending');
			}
		}


		

	}

	public function other_payment () {
		$this->data['page_title'] = " :: Other Payment - Add new record";
		return view('system.payment.other-payment',$this->data);
	}

	public function other_payment_store(PaymentRequest $request){
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		
		$payment = Payment::first();
		$payment->user_id = $this->data['auth']->id;
		

		try {
			$payment = new Payment;
			$payment->user_id = Auth::user()->id;
			$payment->receipt_number = $request->get('receipt_number');
			$payment->nature_of_collection = $request->get('nature_of_collection');
			$payment->name_of_payor = $request->get('name_of_payor');
			$payment->penalty = $request->get('penalty');
			$payment->payment_method = $request->get('payment_method');
			$payment->bank_name = $request->get('bank_name');
			$payment->cheque_no = $request->get('cheque_no');
			$payment->amount = $request->get('amount');
			$payment->reference_type = "activity_clearance";
			$payment->payment_status = $request->get('payment_status');
		

		if($request->get('payment_status') == "no_charge"){
			$payment->amount = '0';
			$payment->receipt_number = "N-C";
		}
			if($payment->save()) {
				$payment->user_id = $this->data['auth']->id;
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "PAYMENT", 'remarks' => Auth::user()->name." has successfully received payment for Activity Clearance request.",'ip' => $ip,'transaction_id'=>"N/A"]);
					Event::fire('log-activity', $log_data);

				$this->data['payment'] = $payment;
				$pdf = PDF::loadView('pdf.receipt', $this->data)->setPaper('letter', 'portrait');
				

				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				session()->flash('id', $payment->id);
				return redirect()->route('system.payment.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
		

		

	}

	public function show($transaction_id = NULL){
		$this->data['page_title'] = " :: Payments - Show Payment";
		$payment = Payment::where('id',$transaction_id)->first();

		if (!$payment) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect($this->getRedirectUrl());
		}

		$this->data['payment'] = $payment;

		return view('system.payment.show',$this->data);
	}

	public function print($id = NULL){
		$this->data['payment'] = Payment::find($id);
		$pdf = PDF::loadView('pdf.receipt', $this->data)->setPaper('letter', 'portrait');
		return $pdf->stream('receipt.pdf');
	}

	public function cash_flow(PageRequest $request){

		$this->data['page_title'] = "Cash Flow Report";
	
		$this->data['keyword'] = $request->get('keyword',false);
		$this->data['from'] = $request->get('from',false);
		$this->data['to'] = $request->get('to',false);
		$this->data['sort_by'] = $request->get('sort_by','last_modified');
		
		if (!$this->data['from']) {
			$from = Payment::select('created_at')->orderBy('created_at','asc')->take(1)->pluck('created_at')->toArray();
		}

		if(isset($from) AND count($from)==1){
			$this->data['from'] = $from[0]->format("Y-m-d");
		}
		
		$this->data['total_payment']= Payment::keyword($this->data['keyword'])->dateRange($this->data['from'],$this->data['to'])->where('payment_method' , 'cash')->where('user_id' , Auth::user()->id)->sum('payment.amount');

		$this->data['total_checque']= Payment::keyword($this->data['keyword'])->dateRange($this->data['from'],$this->data['to'])->where('user_id' , Auth::user()->id)->where('payment_method' , 'cheque')->where('user_id' , Auth::user()->id)->sum('payment.amount');

		$this->data['count'] = Payment::keyword($this->data['keyword'])->dateRange($this->data['from'],$this->data['to'])->where('user_id' , Auth::user()->id)->orderBy('updated_at',"DESC")->count();

		$this->data['payments'] = Payment::where('user_id',Auth::user()->id)->keyword($this->data['keyword'])
									->dateRange($this->data['from'],$this->data['to'])->orderBy('payment.created_at',"DESC")->paginate(15);
		// if($this->data['keyword']){
		// 	dd($this->data);
		// }
		
		return view('system.payment.cash-flow',$this->data);
	}

	public function print_cash_flow(PageRequest $request){
		
		$this->data['page_title'] = "Cash Flow Report";
		$this->data['keyword'] = $request->get('keyword',false);
		$this->data['from'] = $request->get('from',false);
		$this->data['to'] = $request->get('to',false);
		
		

		if (!$this->data['from']) {
			$from = Payment::select('created_at')->orderBy('created_at','asc')->take(1)->pluck('created_at')->toArray();
		}

		if(isset($from) AND count($from)==1){
			$this->data['from'] = $from[0]->format("Y-m-d");
		}


		$this->data['total_payment']= Payment::keyword($this->data['keyword'])->dateRange($this->data['from'],$this->data['to'])->where('payment_method' , 'cash')->where('user_id' , Auth::user()->id)->sum('payment.amount');



		$this->data['total_checque']= Payment::keyword($this->data['keyword'])->dateRange($this->data['from'],$this->data['to'])->where('user_id' , Auth::user()->id)->where('payment_method' , 'cheque')->sum('payment.amount');


		$this->data['count'] = Payment::keyword($this->data['keyword'])->dateRange($this->data['from'],$this->data['to'])->where('user_id' , Auth::user()->id)->orderBy('updated_at',"DESC")->count();

		$this->data['payments'] = Payment::where('user_id',Auth::user()->id)->keyword($this->data['keyword'])
									->dateRange($this->data['from'],$this->data['to'])->orderBy('payment.created_at',"DESC")->paginate(15);
		


		$pdf = PDF::loadView('pdf.cashflow-report', $this->data)->setPaper('letter', 'portrait');
		return $pdf->stream('cashflow-report.pdf');
	}

	public function export_excel(PageRequest $request){

		$this->data['page_title'] = "Cash Flow Report";
		$this->data['from'] = $request->get('from',false);
		$this->data['keyword'] = $request->get('keyword',false);
		$this->data['to'] = $request->get('to',false);
		
		//dd($this->data['from'] = $request->get('from',false));

		if (!$this->data['from']) {
			$from = Payment::select('created_at')->orderBy('created_at','asc')->take(1)->pluck('created_at')->toArray();
		}

		if(isset($from) AND count($from)==1){
			$this->data['from'] = $from[0]->format("Y-m-d");
		}
			$header = array_merge([
				"Reference Type", "Description", "Amount", "Payment Method", "Receipt Number", "Cashier","Date" 
			]);

			
			Excel::create("Exported Cashflow Data", function($excel) use($header){
				$excel->setTitle("Exported Cashflow Data")
			    		->setCreator('HSI')
			          	->setCompany('Highly Suceed Inc.')
			          	->setDescription('Exported Cashflow data.');
			     
			    $excel->sheet('Cashflow', function($sheet) use($header){
			    	$sheet->freezeFirstRow();

			    	$sheet->setOrientation('landscape');

			    	  
			       	$sheet->row(1, $header);

			       	$counter = 2;

					$this->data['total_payment']= Payment::keyword($this->data['keyword'])->dateRange($this->data['from'],$this->data['to'])->where('payment_method' , 'cash')->where('payment.payment_status' , 'paid')->where('user_id' , Auth::user()->id);



					$this->data['total_checque']= Payment::keyword($this->data['keyword'])->dateRange($this->data['from'],$this->data['to'])->where('user_id' , Auth::user()->id)->where('payment_method' , 'cheque')->where('payment.payment_status' , 'paid');


			       	$cashflow = Payment::where('user_id',Auth::user()->id)->keyword($this->data['keyword'])
									->dateRange($this->data['from'],$this->data['to'])->where('payment.payment_status' , 'paid')->orderBy('payment.created_at',"DESC")
						->chunk(15000, function($chunk) use($sheet, $counter) {
							foreach ($chunk as $cashflow) {

								if($cashflow->reference_type == "business_permit"){
									$type = "CLEARANCE - BUSINESS";
									$name = $cashflow->reference->business_name;
								}
								if($cashflow->reference_type == "personal_certificate"){
									$type = "CLEARANCE - PERSONAL";
									$name =$cashflow->reference->resident->full_name;
								}
								if($cashflow->reference_type == "activity_clearance"){
									$type = "CLEARANCE - ACTIVITY";
									$name = Str::title($cashflow->name_of_payor);
								}
								if($cashflow->reference_type == "business_amendment"){
									$type = "BUSINESS AMENDMENT";
									$name = $cashflow->reference->business_name;
								}


								

								$data = [$type,$name,$cashflow->amount,str::title($cashflow->payment_method),$cashflow->receipt_number,$cashflow->user->name,Helper::date_only($cashflow->created_at)
									];

								$sheet->row($counter++, $data);
							}

							  $footer_row = $counter + ($this->data['total_payment']->count());
               				  $sheet->setHeight($footer_row,25);

    			
							$sheet->setBorder("A{$footer_row}:H{$footer_row}", 'thin');
              				$sheet->row($footer_row,['','', '' . Helper::amount($this->data['total_payment']->sum('payment.amount')), 'PHP - CASH']);

              				
              				$sheet->row($footer_row+1,['',' ', '' . Helper::amount($this->data['total_checque']->sum('payment.amount')), 'PHP - CHEQUE']);



	
						});
			    });
			})->export('xls');
		}

}
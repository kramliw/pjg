<?php 

namespace App\Laravel\Controllers\System;

use App\Laravel\Models\FormResponse;
use Helper, Carbon, Session, Str;

class FormResponseController extends Controller
{
	protected $data;

	public function __construct () 
	{
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['heading'] = "Form Response";
	}

	public function index () 
	{
		$this->data['page_title'] = " :: Form Response - Record Data";
		$this->data['responses'] = FormResponse::orderBy('created_at',"DESC")->paginate(15);
		return view('system.form-response.index',$this->data);
	}

	public function show ($id = NULL) 
	{
		$this->data['page_title'] = " :: Form Response - Details";
		$response = FormResponse::with(['form', 'details'])->find($id);

		if (!$response) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.form_response.index');
		}

		$this->data['response'] = $response;
		return view('system.form-response.show',$this->data);
	}

	public function destroy ($id = NULL) 
	{
		$response = FormResponse::find($id);

		if (!$response) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.form_response.index');
		}

		$response->delete();

		session()->flash('notification-status','success');
		session()->flash('notification-msg',"Record has been deleted.");
		return redirect()->route('system.form_response.index');
	}

}
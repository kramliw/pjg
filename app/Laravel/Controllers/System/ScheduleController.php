<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Ward;
use App\Laravel\Models\Employee;
use App\Laravel\Models\Schedule;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\EmployeeRequest;
use App\Laravel\Requests\System\ScheduleRequest;
use App\Http\Requests\PageRequest;
/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class ScheduleController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];


	}

	public function index (PageRequest $request) {

		$this->data['page_title'] = "Schedule - Record Data";

		 $this->data['schedules'] = Schedule::orderBy('updated_at','desc')->get();
		 $this->data['wards'] = Ward::get();

		return view('system.schedule.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = "Wards - Add new record";
		$this->data['wards'] = Ward::get();
		return view('system.schedule.create',$this->data);
	}

	public function store (ScheduleRequest $request) {
		try {

			// return $request->all();
			$schedule = new Schedule;
			
			$schedule->time_start = $request->time_start;
			$schedule->time_end = $request->time_end;
			$schedule->description = $request->description;

	

			if($schedule->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.schedule.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = "Employee - Edit record";
		$this->data['wards'] = Ward::get();
		$schedule = Schedule::find($id);

		if (!$schedule) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.schedule.index');
		}

		$this->data['schedule'] = $schedule;
		return view('system.schedule.edit',$this->data);
	}

	public function update (ScheduleRequest $request, $id = NULL) {
		try {

			// return $request->all();
			$schedule = Schedule::find($id);

			if (!$schedule) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.schedule.index');
			}

			$schedule->fill($request->except('_token'));

			if($schedule->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.schedule.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$schedule = Schedule::find($id);

			if (!$schedule) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.schedule.index');
			}

			if($schedule->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.schedule.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}
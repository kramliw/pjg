<?php 

namespace App\Laravel\Controllers\System;

use App\Laravel\Models\Form;
use App\Laravel\Requests\System\FormRequest;
use Helper, Carbon, Session, Str;

class FormController extends Controller
{
	protected $data;

	public function __construct () 
	{
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['heading'] = "Form Builder";
	}

	public function index () 
	{
		$this->data['page_title'] = " :: Form Builder - Record Data";
		$this->data['forms'] = Form::orderBy('created_at',"DESC")->paginate(15);
		return view('system.form-builder.index',$this->data);
	}

	public function create () 
	{
		$this->data['page_title'] = " :: Form Builder - Add new account";
		return view('system.form-builder.create',$this->data);
	}

	public function store (FormRequest $request) 
	{
		$form = new Form;
		$form->fill($request->all());
		$form->save();

		session()->flash('notification-status','success');
		session()->flash('notification-msg',"New form has been added.");
		return redirect()->route('system.form_builder.edit', [$form->id]);
	}

	public function edit ($id = NULL) 
	{
		$this->data['page_title'] = " :: Form Builder - Edit record";
		$form = Form::with('elements')->find($id);

		if (!$form) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.form_builder.index');
		}

		$this->data['form'] = $form;
		return view('system.form-builder.edit',$this->data);
	}

	public function update (FormRequest $request, $id = NULL) 
	{
		$form = Form::find($id);

		if (!$form) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.form_builder.index');
		}

		$form->fill($request->all());
		$form->save();

		$elementsToRetain = [];
		$properties = json_decode($request->get('properties'), true);

		foreach ($properties as $index => $property) {
			$name = array_get($property, "name");
			$element = $form->elements()->firstOrNew(['key' => $name]);
			$element->fill([
				'key' => array_get($property, "name"),
				'label' => array_get($property, "label"),
				'file_type' => array_get($property, "type"),
				'value' => array_key_exists("values", $property)
						? implode(",", array_pluck(array_get($property, "values"), "value"))
						: array_get($property, "value"),
				'display_value' => array_key_exists("values", $property)
						? implode(",", array_pluck(array_get($property, "values"), "label"))
						: null,
				'is_required' => (boolean) array_get($property, "required", false),
				'maxlength' => (int) array_get($property, "maxlength"),
				'sorting' => $index,
				'properties' => $property,
			]);
			$element->save();
			array_push($elementsToRetain, $element->id);
		}

		$form->elements()->whereNotIn('id', $elementsToRetain)->delete();

		session()->flash('notification-status', "success");
		session()->flash('notification-msg', "Form has been modified successfully.");

		return redirect()->route('system.form_builder.index');
	}

	public function destroy ($id = NULL) 
	{
		$form = Form::find($id);

		if (!$form) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.form_builder.index');
		}

		$form->delete();

		session()->flash('notification-status','success');
		session()->flash('notification-msg',"Record has been deleted.");
		return redirect()->route('system.form_builder.index');
	}

}
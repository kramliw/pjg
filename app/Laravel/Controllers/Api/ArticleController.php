<?php 

namespace App\Laravel\Controllers\Api;

use Helper, Str, DB,ImageUploader;
use App\Laravel\Models\Article;
use Illuminate\Http\Request;
use App\Laravel\Requests\Api\ArticleRequest;
use App\Laravel\Transformers\ArticleTransformer;
use App\Laravel\Transformers\TransformerManager;

class ArticleController extends Controller{

	protected $response = array();

	public function __construct(){
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

	public function index(Request $request, $format = '') {

		$per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        $user = $request->user();
        $keyword = $request->get('keyword');
        $category_ids = $request->get('category_id');

        $sort_by = Str::lower($request->get('sort_by','date_posted'));
        $sort_order = Str::lower($request->get('sort_order','desc'));
    
        $this->response['msg'] = "List of Articles";
        $this->response['status_code'] = "ARTICLE_LIST";

        switch($sort_order){ 
            case 'desc'  : $sort_order = 'desc'; break;
            default: $sort_order = 'asc';
        }

        switch($sort_by){ 
            case 'category'  : $sort_by = 'category_id'; break;
            default: $sort_by = 'created_at';
        }


        $articles = Article::keyword($keyword)
                ->categories($category_ids)
                ->where(function($query) use($request,$user){
                    if($request->has('type') AND Str::lower($request->get('type')) == "own"){
                        $this->response['msg'] = "List of My Articles";
                        return $query->where('user_id',$user->id);
                    }
                })
                ->orderBy($sort_by,$sort_order)->paginate($per_page);

        if($request->has('keyword')){
            $this->response['msg'] = "Search result: '{$keyword}'";
            $this->response['status_code'] = "SEARCH_RESULT";
        }

        $this->response['status'] = TRUE;
        $this->response['has_morepages'] = $articles->hasMorePages();
        $this->response['data'] = $this->transformer->transform($articles, new ArticleTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function store(ArticleRequest $request, $format = '') {

        $user = $request->user();

        $article = new Article;
        $article->user_id = $user->id;
        $article->fill($request->only('title','video_url','content','category_id'));

        if($request->hasFile('file')) {
            $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/articles");
            $article->path = $image['path'];
            $article->directory = $image['directory'];
            $article->filename = $image['filename'];
        }

        $article->save();

        $this->response['msg'] = "New article has been posted.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "ARTICLE_CREATED";
        $this->response['data'] = $this->transformer->transform($article, new ArticleTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function show(Request $request, $format = '') {

        $article = $request->get('article_data');

        $this->response['msg'] = "Article detail.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "ARTICLE_DETAIL";
        $this->response['data'] = $this->transformer->transform($article, new ArticleTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update(ArticleRequest $request, $format = '') {

        $article = $request->get('article_data');
        $user = $request->user();

        $article->fill($request->only('title','video_url','content','category_id'));

        if($request->hasFile('file')) {
            $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/articles");
            $article->path = $image['path'];
            $article->directory = $image['directory'];
            $article->filename = $image['filename'];
        }

        $article->save();

        $this->response['msg'] = "Article has been updated.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "ARTICLE_MODIFIED";
        $this->response['data'] = $this->transformer->transform($article, new ArticleTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function destroy(Request $request, $format = '') {

        $article = $request->get('article_data');

        $article->delete();

        $this->response['msg'] = "Article has been removed.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "ARTICLE_DELETED";
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }
}
<?php


/**
 *
 * ------------------------------------
 * Api Routes
 * ------------------------------------
 *
 */

Route::group(

	array(
		'as' => "api.",
		'namespace' => "Api"
	),

	function() {

		// $this->group(['as' => "setting.",'prefix' => "setting"],function(){
		// 	$this->post('get-form1.{format?}',['as' => "get_form1",'uses' => "SettingController@get_form1"]);
		// 	$this->post('validate-form1.{format?}',['as' => "validate_form1",'uses' => "SettingController@validate_form1"]);
		// 	$this->post('get-form2.{format?}',['as' => "get_form2",'uses' => "SettingController@get_form2"]);
		// 	$this->post('validate-form2.{format?}',['as' => "validate_form2",'uses' => "SettingController@validate_form2"]);
		// });

		$this->group(['prefix' => "setting"],function(){
			$this->post('version.{format}',['as' => "version" , 'uses' => "AppSettingController@version"]);
			$this->post('categories.{format}',['as' => "category" , 'uses' => "AppSettingController@category"]);
			$this->post('specialties.{format}',['as' => "specialty" , 'uses' => "AppSettingController@specialty"]);
		});

		$this->group(['as' => "form.",'prefix' => "form"],function(){
			$this->any('all.{format?}',['as' => "index",'uses' => "FormController@index"]);
			$this->any('show.{format?}',['as' => "show",'uses' => "FormController@show"]);
		});

		$this->group(['as' => "form_element.",'prefix' => "form-element"],function(){
			$this->any('all.{format?}',['as' => "index",'uses' => "FormElementController@index"]);
			$this->any('show.{format?}',['as' => "show",'uses' => "FormElementController@show"]);
		});

		$this->group(['as' => "form_response.",'prefix' => "form-response"],function(){
			$this->post('create.{format?}',['as' => "store",'uses' => "FormResponseController@store"]);
		});

		$this->group(['as' => "auth.", 'prefix' => "auth", 'namespace' => "Auth"], function () {
			$this->post('login.{format?}', ['as' => "login", 'uses' => "LoginController@authenticate"]);
			$this->post('register.{format?}', ['as' => "register", 'uses' => "RegisterController@store"]);
			$this->post('forgot-password.{format?}', ['as' => "forgot_password", 'uses' => "ForgotPasswordController@forgot_password"]);
			$this->post('reset-password.{format?}', ['as' => "reset_password", 'uses' => "ResetPasswordController@reset_password", 'middleware' => "api.verify-reset-token"]);
			$this->post('logout.{format?}', ['as' => "logout", 'uses' => "LoginController@logout", 'middleware' => "jwt.auth"]);
			$this->post('refresh-token.{format?}', ['as' => "refresh_token", 'uses' => "RefreshTokenController@refresh", 'middleware' => "jwt.refresh"]);
		});

		//All routes protected by authentication
		$this->group(['middleware' => "jwt.auth"], function() {


			// This is Scheduling route

				$this->group(['as' => "employee.", 'prefix' => "employee"], function() { 
				$this->post('create.{format?}', ['as' => "store", 'uses' => "EmployeeController@store"]);
				$this->post('all.{format?}', ['as' => "all", 'uses' => "EmployeeController@show"]);
				$this->post('edit.{format?}', ['as' => "edit", 'uses' => "EmployeeController@edit"]);
				$this->post('update.{format?}', ['as' => "update", 'uses' => "EmployeeController@update"]);
				$this->post('delete.{format?}', ['as' => "delete", 'uses' => "EmployeeController@destroy"]);
				});

				$this->group(['as' => "ward.", 'prefix' => "ward"], function() { 
				$this->post('create.{format?}', ['as' => "store", 'uses' => "WardController@store"]);
				$this->post('all.{format?}', ['as' => "all", 'uses' => "WardController@index"]);
				$this->post('edit.{format?}', ['as' => "edit", 'uses' => "WardController@edit"]);
				$this->post('update.{format?}', ['as' => "update", 'uses' => "WardController@update"]);
				});




			//End of Scheduing Route
			$this->group(['as' => "profile.", 'prefix' => "profile"], function() { 
				$this->post('info.{format?}', ['as' => "show", 'uses' => "ProfileController@show"]);
				$this->post('edit.{format?}', ['as' => "update_profile", 'uses' => "ProfileController@update_profile"]);
				$this->post('edit-description.{format?}', ['as' => "update_description", 'uses' => "ProfileController@update_description"]);

				$this->post('change-password.{format?}', ['as' => "update_password", 'uses' => "ProfileController@update_password"]);
				$this->post('change-currency.{format?}', ['as' => "update_currency", 'uses' => "ProfileController@update_currency"]);

				$this->post('change-avatar.{format?}', ['as' => "update_avatar", 'uses' => "ProfileController@update_avatar"]);
				$this->post('change-fcm-token.{format?}', ['as' => "update_device", 'uses' => "ProfileController@update_device"]);
				$this->post('fb-connect.{format?}', ['as' => "fb_connect", 'uses' => "ProfileController@fb_connect"]);

				$this->post('resend-verification.{format?}', ['as' => "resend_verification", 'uses' => "ProfileController@resend_verification"]);
				$this->post('notifications.{format?}', ['as' => "notifications", 'uses' => "ProfileController@notifications"]);


			});

			$this->group(['as' => "mentorship.",'prefix' => "mentorship"],function(){
				$this->post('search.{format?}',['as' => "search",'uses' => "MentorshipController@search",'middleware' => ['api.exists:mentee','api.exists:ongoing_mentorship'] ]); 
				$this->post('show.{format?}',['as' => "show",'uses' => "MentorshipController@show",'middleware' => "api.exists:mentorship"]);
				$this->post('all.{format?}',['as' => "index",'uses' => "MentorshipController@index"]);
				$this->post('ongoing.{format?}',['as' => "ongoing",'uses' => "MentorshipController@ongoing"]);

				
				//complete
				$this->post('complete.{format?}',['as' => "complete",'uses' => "MentorshipController@complete",'middleware' => ["api.exists:mentorship"]]);
				$this->post('send-review.{format?}',['as' => "send_review",'uses' => "MentorshipController@send_review",'middleware' => ["api.exists:mentorship"]]);


				$this->group(['as' => "message.",'prefix' => "message",'middleware' => "api.exists:mentorship"],function(){
					$this->post('all.{format?}',['as' => "index",'uses' => "MentorshipConversationController@index"]);
					$this->post('create.{format?}',['as' => "create",'uses' => "MentorshipConversationController@store"]);
					$this->post('upload.{format?}',['as' => "upload",'uses' => "MentorshipConversationController@upload"]);
					$this->post('delete.{format?}',['as' => "delete",'uses' => "MentorshipConversationController@destroy",'middleware' => "api.exists:mtrsp_message"]);

				});

				$this->group(['as' => "participant.",'prefix' => "participant"],function(){
					$this->post('all.{format?}',['as' => "index",'uses' => "MentorshipParticipantController@index",'middleware' => "api.exists:mentorship"]);

					$this->post('moderator.{format?}',['as' => "moderator",'uses' => "MentorshipParticipantController@moderator",'middleware' => "api.exists:mentorship"]);
					$this->post('member.{format?}',['as' => "member",'uses' => "MentorshipParticipantController@member",'middleware' => "api.exists:mentorship"]);
					$this->post('create.{format?}',['as' => "create",'uses' => "MentorshipParticipantController@store",'middleware' => ["api.exists:mentorship","api.exists:user"]]);
					$this->post('promote.{format?}',['as' => "promote",'uses' => "MentorshipParticipantController@promote",'middleware' => ["api.exists:user","api.exists:mentorship","api.exists:mentorship_participant"]]);
					$this->post('demote.{format?}',['as' => "demote",'uses' => "MentorshipParticipantController@demote",'middleware' => ["api.exists:user","api.exists:mentorship","api.exists:mentorship_participant"]]);

					// $this->post('delete.{format?}',['as' => "delete",'uses' => "MentorshipParticipantController@destroy"]);
				});

				// $this->post('edit.{format?}',['as' => "edit",'uses' => "ChatController@update",'middleware' => "api.exists:own_comment"]);
				// $this->post('delete.{format?}',['as' => "delete",'uses' => "ChatController@destroy",'middleware' => "api.exists:own_comment"]);

			});

			$this->group(['as' => "chat.",'prefix' => "chat"],function(){
				$this->post('all.{format?}',['as' => "index",'uses' => "ChatController@index"]);
				$this->post('create.{format?}',['as' => "create",'uses' => "ChatController@store"]); 
				$this->post('show.{format?}',['as' => "show",'uses' => "ChatController@show",'middleware' => "api.exists:chat"]);
				$this->post('name.{format?}',['as' => "name",'uses' => "ChatController@update_name",'middleware' => "api.exists:own_chat"]);
				$this->post('icon.{format?}',['as' => "icon",'uses' => "ChatController@update_icon",'middleware' => "api.exists:own_chat"]);

				$this->post('leave.{format?}',['as' => "leave",'uses' => "ChatParticipantController@leave",'middleware' => "api.exists:chat"]);

				$this->group(['as' => "message.",'prefix' => "message",'middleware' => "api.exists:chat"],function(){
					$this->post('all.{format?}',['as' => "index",'uses' => "ChatConversationController@index"]);
					$this->post('create.{format?}',['as' => "create",'uses' => "ChatConversationController@store"]);
					$this->post('upload.{format?}',['as' => "upload",'uses' => "ChatConversationController@upload"]);
					
					$this->post('delete.{format?}',['as' => "delete",'uses' => "ChatConversationController@destroy",'middleware' => "api.exists:gc_message"]);
				});

				$this->group(['as' => "participant.",'prefix' => "participant"],function(){
					$this->post('all.{format?}',['as' => "index",'uses' => "ChatParticipantController@index",'middleware' => "api.exists:chat"]);
					$this->post('moderator.{format?}',['as' => "moderator",'uses' => "ChatParticipantController@moderator",'middleware' => "api.exists:chat"]);
					$this->post('member.{format?}',['as' => "member",'uses' => "ChatParticipantController@member",'middleware' => "api.exists:chat"]);
					$this->post('create.{format?}',['as' => "create",'uses' => "ChatParticipantController@store",'middleware' => ["api.exists:chat","api.exists:user"]]);
					$this->post('promote.{format?}',['as' => "promote",'uses' => "ChatParticipantController@promote",'middleware' => ["api.exists:user","api.exists:chat","api.exists:chat_participant"]]);
					$this->post('demote.{format?}',['as' => "demote",'uses' => "ChatParticipantController@demote",'middleware' => ["api.exists:user","api.exists:chat","api.exists:chat_participant"]]);
					$this->post('delete.{format?}',['as' => "delete",'uses' => "ChatParticipantController@destroy",'middleware' => ["api.exists:user","api.exists:chat","api.exists:chat_participant"]]);

					// $this->post('delete.{format?}',['as' => "delete",'uses' => "ChatParticipantController@destroy"]);
				});

				// $this->post('edit.{format?}',['as' => "edit",'uses' => "ChatController@update",'middleware' => "api.exists:own_comment"]);
				// $this->post('delete.{format?}',['as' => "delete",'uses' => "ChatController@destroy",'middleware' => "api.exists:own_comment"]);
				
			});

			$this->group(['as' => "article.",'prefix' => "article"],function(){
				$this->post('all.{format?}',['as' => "index",'uses' => "ArticleController@index"]);
				$this->post('owned.{format?}',['as' => "owned",'uses' => "ArticleController@owned"]);

				$this->post('show.{format?}',['as' => "show",'uses' => "ArticleController@show",'middleware' => "api.exists:article"]);
				$this->group(['middleware' => "api.mentor"],function(){
					$this->post('create.{format?}',['as' => "create",'uses' => "ArticleController@store"]);
					$this->post('edit.{format?}',['as' => "edit",'uses' => "ArticleController@update",'middleware' => "api.exists:own_article"]);
					$this->post('delete.{format?}',['as' => "delete",'uses' => "ArticleController@destroy",'middleware' => "api.exists:own_article"]);

				});

				$this->group(['as' => "reaction.",'prefix' => "reaction",'middleware' => "api.exists:article"],function(){
					$this->post('all.{format?}',['as' => "index",'uses' => "ArticleReactionController@index"]);
					$this->post('react.{format?}',['as' => "react",'uses' => "ArticleReactionController@react"]);
					
				});

				$this->group(['as' => "comment.",'prefix' => "comment",'middleware' => "api.exists:article"],function(){
					$this->post('all.{format?}',['as' => "index",'uses' => "ArticleCommentController@index"]);
					$this->post('create.{format?}',['as' => "create",'uses' => "ArticleCommentController@store"]);
					$this->post('show.{format?}',['as' => "show",'uses' => "ArticleCommentController@show",'middleware' => "api.exists:comment"]);
					$this->post('edit.{format?}',['as' => "edit",'uses' => "ArticleCommentController@update",'middleware' => "api.exists:own_comment"]);
					$this->post('delete.{format?}',['as' => "delete",'uses' => "ArticleCommentController@destroy",'middleware' => "api.exists:own_comment"]);
				});

			});

			$this->group(['as' => "user.",'prefix' => "user"],function(){
				$this->post('all.{format?}',['as' => "index",'uses' => "UserController@index"]);
				$this->post('search.{format?}',['as' => "search",'uses' => "UserController@search"]);
				$this->post('mentor.{format?}',['as' => "mentor",'uses' => "UserController@mentor"]);
				$this->post('mentee.{format?}',['as' => "mentee",'uses' => "UserController@mentee"]);
				$this->post('show.{format?}',['as' => "show",'uses' => "UserController@show",'middleware' => "api.exists:user"]);

			});
		});
		
	}
);
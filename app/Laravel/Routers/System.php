<?php

$this->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "System", 
	'as' => "system.", 
	'prefix'	=> "",
	// 'middleware' => "", 

], function(){

	$this->group(['middleware' => ["web","system.guest"]], function(){
		$this->get('register/{_token?}',['as' => "register",'uses' => "AuthController@register"]);
		$this->post('register/{_token?}',['uses' => "AuthController@store"]);
		$this->get('login/{redirect_uri?}',['as' => "login",'uses' => "AuthController@login"]);
		$this->post('login/{redirect_uri?}',['uses' => "AuthController@authenticate"]);

		$this->get('sample', ['uses' => "AuthController@sample"]);
		$this->post('sample', ['uses' => "AuthController@sample_store"]);
	});

	$this->group(['middleware' => ["web","system.auth"]], function(){
		
		$this->get('lock',['as' => "lock", 'uses' => "AuthController@lock"]);
		$this->post('lock',['uses' => "AuthController@unlock"]);
		$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);

		$this->group(['as' => "account."],function(){
			$this->get('p/{username?}',['as' => "profile",'uses' => "AccountController@profile"]);
			$this->group(['prefix' => "setting"],function(){
				$this->get('info',['as' => "edit-info",'uses' => "AccountController@edit_info"]);
				$this->post('info',['uses' => "AccountController@update_info"]);
				$this->get('password',['as' => "edit-password",'uses' => "AccountController@edit_password"]);
				$this->post('password',['uses' => "AccountController@update_password"]);
			});
		});



		$this->group(['middleware' => ["system.update_profile_first"]], function() {
			$this->get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);

			$this->group(['prefix' => "subscriber", 'as' => "subscriber."], function () {
				$this->get('/',['as' => "index", 'uses' => "NewsletterSubscriptionController@index"]);
			});



			$this->group(['prefix' => "system-account", 'as' => "user."], function () {
				$this->get('/',['as' => "index", 'uses' => "UserController@index"]);
				$this->get('create',['as' => "create", 'uses' => "UserController@create"]);
				$this->post('create',['uses' => "UserController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "UserController@edit"]);
				$this->post('edit/{id?}',['uses' => "UserController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "UserController@destroy"]);
			});
			///////////////////////////////////////////////////////////////////////////////


				// Ward Route 
				$this->group(['prefix' => "ward", 'as' => "ward."], function () {
				$this->get('/',['as' => "index", 'uses' => "WardController@index"]);
				$this->get('create',['as' => "create", 'uses' => "WardController@create"]);
				$this->post('create',['uses' => "WardController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "WardController@edit"]);
				$this->post('edit/{id?}',['uses' => "WardController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "WardController@destroy"]);
			});

			//Emloyee Route

				$this->group(['prefix' => "employee", 'as' => "employee."], function () {
				$this->get('/',['as' => "index", 'uses' => "EmployeeController@index"]);
				$this->get('create',['as' => "create", 'uses' => "EmployeeController@create"]);
				$this->post('create',['uses' => "EmployeeController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "EmployeeController@edit"]);
				$this->post('edit/{id?}',['uses' => "EmployeeController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "EmployeeController@destroy"]);
			});

			// Schedule Route

				$this->group(['prefix' => "schedule", 'as' => "schedule."], function () {
					$this->get('/',['as' => "index", 'uses' => "ScheduleController@index"]);
					$this->get('create',['as' => "create", 'uses' => "ScheduleController@create"]);
					$this->post('create',['uses' => "ScheduleController@store"]);
					$this->get('edit/{id?}',['as' => "edit", 'uses' => "ScheduleController@edit"]);
					$this->post('edit/{id?}',['uses' => "ScheduleController@update"]);
					$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ScheduleController@destroy"]);
			});

			// Manage Schedule

				$this->group(['prefix' => "manage-schedule", 'as' => "manage_schedule."], function () {
					$this->get('/',['as' => "index", 'uses' => "ManageScheduleController@index"]);
					$this->get('create/{id?}',['as' => "create", 'uses' => "ManageScheduleController@create"]);
					$this->post('create/{id?}',['uses' => "ManageScheduleController@store"]);
					$this->get('edit/{id?}',['as' => "edit", 'uses' => "ManageScheduleController@edit"]);
					$this->post('edit/{id?}',['uses' => "ManageScheduleController@update"]);
					$this->get('delete/{id?}',['as' => "destroy", 'uses' => "ManageScheduleController@destroy"]);
			});

		   // All Schedule Route

				$this->group(['prefix' => "schedule-list", 'as' => "schedule_list."], function () {
					$this->get('/',['as' => "index", 'uses' => "AllScheduleController@index"]);
					$this->get('create/{id?}',['as' => "create", 'uses' => "AllScheduleController@create"]);
					$this->post('create/{id?}',['uses' => "AllScheduleController@store"]);
					$this->get('edit/{id?}',['as' => "edit", 'uses' => "AllScheduleController@edit"]);
					$this->post('edit/{id?}',['uses' => "AllScheduleController@update"]);
					$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AllScheduleController@destroy"]);
			});



/////////////////////////////////////////////////////////////////////////////////////////////////
			

	
		});
	});
});
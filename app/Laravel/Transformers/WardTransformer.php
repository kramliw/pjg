<?php 

namespace App\Laravel\Transformers;

use Input;
use JWTAuth, Carbon, Helper;
use App\Laravel\Models\Employee;
use App\Laravel\Models\Ward;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;

use Str;

class WardTransformer extends TransformerAbstract{

	protected $user,$auth;

	

    public function __construct() {
    	$this->auth = Auth::user();
    }

	public function transform(Ward $ward) {
		$this->user = $ward;
	    return [
	     	'id' => $ward->id ?:0,
	     
	     	'ward_name' => $ward->ward_name,
	     	

	     ];
	}

	
}
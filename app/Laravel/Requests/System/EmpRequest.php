<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class EmpRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			// 'first_name'		=> "required|unique:wards,ward_name,{$id}",
			'employee_id'		=> "required",
			'employee_ward_id'		=> "required",
			'employee_shift_id'		=> "required",
			'date'		=> "required"
			// 'date'		=> "required|unique:employee_schedules,date,{$id}"
			];

		return $rules;
	}

	public function messages(){
		return [
			'date.unique'	=> "This date is already set, Please try other date!",
			'required'	=> "Field is required.",
		];
	}
}
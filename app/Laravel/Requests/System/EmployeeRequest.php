<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class EmployeeRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			// 'first_name'		=> "required|unique:wards,ward_name,{$id}",
			'first_name'		=> "required",
			'last_name'		=> "required",
			'middle_name'		=> "required",
			'position'		=> "required",
			'ward_id'		=> "required",
			'department'=> "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'name.unique'	=> "Nature of Business already used. Please double check your input.",
			'required'	=> "Field is required.",
		];
	}
}
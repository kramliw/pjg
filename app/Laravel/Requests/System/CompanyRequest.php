<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class CompanyRequest extends RequestManager{

	public function rules(){

		// $id = $this->route('resident_id')?:0;
		// $type = $this->route('transaction_type')?:"";

		// $id = Auth::user()->id;

		$rules = [
			'company_name'		=> "required",
			'category'		=> "required",
		
		];

	
		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'amount.numeric' => "Invalid data",
		];
	}
}
<?php namespace App\Laravel\Requests\System;

use App\Laravel\Requests\RequestManager;

class FormRequest extends RequestManager
{
	public function rules()
	{
		$id = $this->route('id') ?: 0;

		$rules = [
			'name'		=> "required|unique:form,name,{$id}",
			'code'		=> "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'name.unique'	=> "Form name is already used.",
		];
	}
}
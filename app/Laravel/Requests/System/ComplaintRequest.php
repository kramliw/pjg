<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ComplaintRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			
			
			'complainant_address'=> "required",
			'complainant_city'=> "required",
			'respondent'=> "required",
			'respondent_address' => "required",
			'respondent_city'=> "required",
			'complaint_description' => "required",
			'condition_description' => "required",
			'complaint_date' => "required",
			'filed_date' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'case_no.unique'	=> "Case Number already used. Please double check your input.",
			'required'	=> "Field is required."
		];
	}
}
<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ScheduleRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			// 'first_name'		=> "required|unique:wards,ward_name,{$id}",
			'time_start'		=> "required",
			'time_end'		=> "required",
			'description'		=> "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'name.unique'	=> "Nature of Business already used. Please double check your input.",
			'required'	=> "Field is required.",
		];
	}
}
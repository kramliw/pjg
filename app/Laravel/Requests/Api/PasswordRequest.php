<?php

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
// use JWTAuth;

class PasswordRequest extends ApiRequestManager
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();

        $rules = [
            'password' => "required|password_format|confirmed",
            'current_password' => "required|old_password:" . $user->id,
        ];

        return $rules;
    }

    public function messages() {
        return [
            'password.password_format' => "New password must be 2-25 characters long only and without space.",
            'current_password.old_password' => "Please put your current password.",
        ];
    }
}

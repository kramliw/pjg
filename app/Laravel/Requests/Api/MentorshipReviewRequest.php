<?php

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
// use JWTAuth;

class MentorshipReviewRequest extends ApiRequestManager
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();

        $rules = [
            'rating' => "required|min:1|max:5",
        ];

        return $rules;
    }

    public function messages() {

        return [
            'max' => ":max is the max user rating.",
            'min' => "Minimum user rating is :min",
            'required' => "Please indicate your user rating from 1 - 5.",
        ];
    }
}

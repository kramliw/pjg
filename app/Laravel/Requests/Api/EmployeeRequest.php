<?php

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
// use JWTAuth;

class EmployeeRequest extends ApiRequestManager
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'first_name' => "required",
            'last_name' => "required",
            'middle_name' => "required",
            'position' => "required",
            'department' => "required",
            'ward_id' => "required",

        ];


          

        // if($this->has('birthdate')) {
        //     $rules['birthdate'] = "date";
        // }

        // if($this->has('contact_number')) {
        //     $rules['contact_number'] = "phone:PH,mobile";
        //     // $rules['country'] = "required_with:contact_number";
        // }
        return $rules;
    }

    public function messages() {

        return [
            'email.email'           => "Email address format is invalid.",
            'email.unique_email'         => "Email address already taken. Please use another.",
            'username.username_format'   => "Display name is invalid. Please try another.",
            'username.unique_username'   => "Display name no longer available. Please try another.",
            'max'                   => "Input value is too long maxlenght is :max",
            'contact_number.phone' => "This mobile number is in invalid format.",
            'password.password_format' => "Your password must be 2-25 characters long only and without space.",
            'type.valid_account'    => "Type of account is invalid.",
            'type.required' => "Please select type of account.",
        ];
    }
}
